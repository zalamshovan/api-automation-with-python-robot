import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from AmplifiersController import AmplifiersController as amp_con
from rendr_util import get_logged_in_user_facility_id


class AmplifiersTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""

    amp = amp_con()

    def __init__(self):
        self._expression = ''

    def set_token_of_amplifiers_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.amp.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_amplifiers_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.amp.set_super_admin_token(token)

    def create_amplifier(self, code):
        """
        @desc - method to check create amplifier endpoint
        :param - code: Expected status code
        """
        self.amp.post_create_amplifier(amp_name="TEST_API", amp_sn="1", facility_id=self.facility_id,
                                       expected_code=code)

    def get_by_sn(self, code):
        """
        @desc - Get amp by serial number.
        :param - code: expected status code
        """
        self.amp.get_get_by_sn(sn_amp=None, expected_code=code)

    def get_amplifier_types(self, code):
        """
        @desc - Get types of all amps
        :param - code: expected status code
        """
        self.amp.get_get_amplifier_types(expected_code=code)

    def get_modes_for_amplifier(self, code):
        """
        @desc - Get modes of Amp with ID.
        :param - code: expected status code
        """
        self.amp.get_get_modes_for_amplifier(amplifier_type_id=None, expected_code=code)

    def get_sample_rates_for_amplifier(self, code):
        """
        @desc - Get sample rates of Amp with ID.
        :param - code: expected status code
        """
        self.amp.get_get_sample_rates_for_amplifier(amplifier_type_id=None, expected_code=code)

    def get_amplifiers(self, code):
        """
        @desc - Get list of all amps
        :param - code: expected status code
        """
        self.amp.get_get_amplifiers(expected_code=code)

    def get_amplifier_id(self, code):
        """
        @desc - Get amp with id
        :param - code: expected status code
        """
        self.amp.get_get_amplifier_id(amp_id=None, expected_code=code)

    def post_amplifiers(self, code):
        """
        @desc - Post amp endpoint validation
        :param - code: expected status code
        """
        self.amp.post_amplifiers(expected_code=code)

    def patch_amplifiers(self, code):
        """
        @desc - Patch amp endpoint validation
        :param - code: expected status code
        """
        self.amp.patch_amplifiers_id(expected_code=code)

    def put_amplifiers(self, code):
        """
        @desc - Put amp endpoint validation
        :param - code: expected status code
        """
        self.amp.put_amplifiers(expected_code=code)

    def delete_amplifier(self, code):
        """
        @desc - Delete amp with ID.
        :param - code: expected status code
        """
        self.amp.delete_delete_amplifier(amp_id=None, expected_code=code)

    class AmplifiersTCs(Exception):
        pass
