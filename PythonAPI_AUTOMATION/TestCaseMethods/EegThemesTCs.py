import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'rendrapi'))
sys.path.insert(0, lib_path1)
from EegThemesController import EegThemesController as eeg_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class EegThemesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""

    eeg_theme = eeg_con()

    def __int__(self):
        self._expression = ''

    def set_token_of_eeg_themes_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.eeg_theme.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_eeg_themes_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin's token
        :return:
        """
        self.eeg_theme.set_super_admin_token(token)

    def post_eeg_themes(self, code):
        """
        @desc - method to test post endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.post_eeg_themes(eeg_theme_name="TEST_API", background_color="black", label_color="yellow",
                                       facility_id=self.facility_id, expected_code=code)

    def get_eeg_themes(self, code):
        """
        @desc - validation of get eeg themes endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.get_eeg_themes(expected_code=code)

    def get_eeg_themes_id(self, code):
        """
        @desc - validation of get eeg themes by id endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.get_eeg_themes_id(eeg_themes_id=None, expected_code=code)

    def patch_eeg_themes(self, code):
        """
        @desc - validation of patch endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.patch_eeg_themes_id(eeg_themes_id=None, eeg_theme_name="API_TEST_patch",
                                           background_color="black", label_color="green", facility_id=self.facility_id,
                                           expected_code=code)

    def put_eeg_themes(self, code):
        """
        @desc - validation of put endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.put_eeg_themes_id(eeg_themes_id=None, eeg_themes_name="API_TEST_put", background_color="black",
                                         label_color="blue", facility_id=self.facility_id, expected_code=code)

    def delete_eeg_themes(self, code):
        """
        @desc - validation of delete endpoint
        :param code: expected status code
        :return:
        """
        self.eeg_theme.delete_eeg_themes_id(eeg_themes_id=None, expected_code=code)

    class EegThemesTCs(Exception):
        pass
