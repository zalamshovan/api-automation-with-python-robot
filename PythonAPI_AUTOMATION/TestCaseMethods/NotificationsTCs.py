import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from StudyEventsController import StudyEventsController
from StudiesController import StudiesController
from NotificationsController import NotificationsController
from rendr_util import login_to_rendr, get_logged_in_user_facility_id, get_logged_in_user_id


class NotificationsTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    token_super_admin = ""
    user_token = ""

    facility_id = ""
    study_id = ""
    event_id = ""

    sender_user_id = ""
    receiver_user_id = ""
    receiver_user_email = ""

    recording_index = ""
    packet_count = ""

    study_events = StudyEventsController()
    studies = StudiesController()
    notification = NotificationsController()

    def __init__(self):
        self._expression = ''

    def set_token_of_notifications_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.notification.set_token(token)
        self.user_token = token
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_notifications_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.token_super_admin = token
        self.notification.set_super_admin_token(token)

    def set_sender_user_id(self, token):
        """
        @desc - This method sets the value of Sender's User ID
        :param token:
        :return:
        """
        self.sender_user_id = get_logged_in_user_id(token)

    def set_receiver_user_email(self, email):
        """
        @desc - This method sets the value of Receiver's User Email
        :param email:
        :return:
        """
        self.receiver_user_email = email
        print("Receiver email is set to :" + self.receiver_user_email)
        return self.receiver_user_email

    def set_receiver_user_id(self, token):
        """
        @desc - This method sets the value of Receiver's User ID
        :param token:
        :return:
        """
        self.receiver_user_id = get_logged_in_user_id(token)
        print("Receiver user ID is set to :" + self.receiver_user_id)
        return self.receiver_user_id

    def set_super_admin_token_for_study_events_and_studies(self, token):
        """
        @desc - method to set token of the super admin user of Studies Events and Studies class
        :param - token: user's token
        """
        self.studies.set_super_admin_token(token)
        self.study_events.set_super_admin_token(token)

    def get_study_id(self):
        """
        This method sets the value of Study ID, Recording ID and Packet Count
        :return:
        """
        if self.token_super_admin is None:
            raise AssertionError("Please set the Token of SuperAdmin")
        else:
            self.set_super_admin_token_for_study_events_and_studies(token=self.token_super_admin)
        response = self.studies.get_studies(is_super_admin=True, expected_code="200")
        json = response.json()
        is_found = 0

        for each in range(len(json)):
            if is_found == 1:
                break
            if json[each]['SyncInfo']['SyncState'] == 1 and len(json[each]['SyncInfo']['StudyRecordings']) > 0:
                for data in range(len(json[each]['SyncInfo']['StudyRecordings'])):
                    if (json[each]['SyncInfo']['StudyRecordings'][data]['SyncState'] == 1) and (
                            10000 < json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount'] < 20000):
                        print("STUDY ID -> " + json[each]['SyncInfo']['StudyRecordings'][data]['StudyId'])
                        self.study_id = json[each]['SyncInfo']['StudyRecordings'][data]['StudyId']
                        is_found = 1
                        break
        if is_found == 1:
            return self.study_id

    def get_event_id(self):
        """
        This method sets the value of Event ID, based on the found Study ID
        :return:
        """
        if self.token_super_admin is None or self.study_id is None:
            raise AssertionError("Please set the Token of SuperAdmin and value of Study ID")
        else:
            self.set_super_admin_token_for_study_events_and_studies(token=self.token_super_admin)

        response = self.study_events.get_get_study_events_by_study_id(is_super_admin=True, study_id=self.study_id,
                                                                      expected_code="200")

        json = response.json()
        self.event_id = json[0]['ID']
        print("Event ID has been set to :" + self.event_id)
        return self.event_id

    def share_study(self, study_id, user_id, code):
        """
        @desc - Share study Test case
        :param study_id: ID of the study to share
        :param user_id: ID of the user to share with
        :param code:
        :return:
        """
        if study_id is None:
            study_id = self.get_study_id()
            if self.study_id is None:
                raise AssertionError("Study ID is set to None")

        if user_id is None:
            user_id = self.receiver_user_id
            if self.receiver_user_id is None:
                raise AssertionError("Receiver User ID is set to None")

        self.notification.post_share_study(study_id=study_id, user_id=user_id, expected_code=code)

    def share_study_event(self, study_id, event_id, user_id, code):
        """
        @desc - Share study events with test user
        :param study_id: ID of the study
        :param event_id: Event ID of the set Study ID
        :param user_id: Receiver user ID
        :param code:
        :return:
        """
        if study_id is None:
            study_id = self.get_study_id()
            if study_id is None:
                raise AssertionError("Study ID is set to None")

        if event_id is None:
            event_id = self.get_event_id()
            if event_id is None:
                raise AssertionError("Event ID is set to None")

        if user_id is None:
            user_id = self.receiver_user_id
            if user_id is None:
                raise AssertionError("Receiver User ID is set to None")

        self.notification.post_share_study_event(study_id=study_id, study_event_id=event_id,
                                                 user_id=user_id, expected_code=code)

    def notify_study_submitted(self, study_id, user_id, user_mail, code):
        """
        @desc - Notify study submitted to Test user
        :param study_id: ID of the Study
        :param user_id: ID of Receiver
        :param user_mail: Email of the Receiver
        :param code:
        :return:
        """
        if study_id is None:
            study_id = self.get_study_id()
            if study_id is None:
                raise AssertionError("Study ID is set to None")

        if user_mail is None:
            user_mail = self.receiver_user_email
            if user_mail is None:
                raise AssertionError("User email is set to None")

        if user_id is None:
            user_id = self.receiver_user_id
            if user_id is None:
                raise AssertionError("Receiver User ID is set to None")

        self.notification.post_notify_study_submitted(study_id=study_id, user_id=user_id,
                                                      user_email=user_mail, expected_code=code)

    def get_shareable_users_and_groups(self, code):
        """
        @desc - Test case to get shareable users list
        :param code:
        :return:
        """
        self.notification.get_get_shareable_users_and_groups(facility_id=self.facility_id, expected_code=code)

    def get_submittable_users_and_groups(self, code):
        """
        @desc - Test case to get submittable users list
        :param code:
        :return:
        """
        self.notification.get_get_submittable_users_and_groups(facility_id=self.facility_id, expected_code=code)

    class NotificationsTCs(Exception):
        pass
