import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'rendrapi'))
sys.path.insert(0, lib_path1)
from ControlSoftwareLogsController import ControlSoftwareLogsController as control_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class ControlSoftwareLogsTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""

    control = control_con()

    def __int__(self):
        self._expression = ''

    def set_token_of_control_software_logs_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.control.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_control_software_logs_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin user's token
        :return:
        """
        self.control.set_super_admin_token(token)

    def post_control_software_logs(self, device_id, code):
        """
        @desc - method to validate POST endpoint
        :param device_id: ID of the device
        :param code: expected status code
        :return:
        """
        self.control.post_control_software_logs(device_id=device_id, user_name="TEST_API", facility_id=self.facility_id,
                                                expected_code=code)

    def get_control_software_logs(self, code):
        """
        @desc - method to validate GET endpoint
        :param code: expected status code
        :return:
        """
        self.control.get_control_software_logs(expected_code=code)

    def get_control_software_logs_id(self, code):
        """
        @desc - method to validate GET by id endpoint
        :param code: expected status code
        :return:
        """
        self.control.get_control_software_logs_id(control_software_log_id=None, expected_code=code)

    def put_control_software_logs(self, device_id, code):
        """
        @desc - method to validate PUT endpoint
        :param device_id: ID of the device
        :param code: expected status code
        :return:
        """
        self.control.put_control_software_logs(control_software_log_id=None, device_id=device_id, user_name='TES_API',
                                               facility_id=self.facility_id, expected_code=code)

    def patch_control_software_logs(self, device_id, code):
        """
        @desc - method to validate PATCH endpoint
        :param device_id: ID of the device
        :param code: expected status code
        :return:
        """
        self.control.patch_control_software_logs(control_software_log_id=None, device_id=device_id, user_name='TES_API',
                                                 facility_id=self.facility_id, expected_code=code)

    def get_device_logs(self, device_id, code):
        """
        @desc - method to validate Get Device Logs endpoint
        :param device_id: ID of the device
        :param code: expected status code
        :return:
        """
        self.control.get_get_device_logs(device_id=device_id, max_search_results='700', expected_code=code)

    def get_facility_logs(self, code):
        """
        @desc - method to validate Get Facility logs endpoint
        :param code: expected status code
        :return:
        """
        self.control.get_get_facility_logs(facility_id=self.facility_id, expected_code=code)

    def post_synchronize_log_entry(self, device_id, code):
        """
        @desc - method to validate POST synchronize log entry endpoint
        :param device_id: ID of the device
        :param code: expected status code
        :return:
        """
        self.control.post_synchronize_log_entry(device_id=device_id, user_name='TEST_API', facility_id=self.facility_id,
                                                expected_code=code)

    def delete_control_software_log(self, code):
        """
        @desc - method to validate DELETE endpoint
        :param code: expected status code
        :return:
        """
        self.control.delete_control_software_log(control_software_log_id=None, expected_code=code)

    class ControlSoftwareLogsTCs(Exception):
        pass
