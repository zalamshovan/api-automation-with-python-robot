import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from StudiesController import StudiesController as study_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class StudiesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    study = study_con()

    study_id = ""

    def __int__(self):
        self._expression = ''

    def set_token_of_studies_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.study.set_token(token)

    def set_token_of_studies_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin user's token
        :return:
        """
        self.study.set_super_admin_token(token)

    def get_studies(self, code):
        """
        @desc - method to validate GET STUDIES endpoint
        :param code: expected status code
        :return:
        """
        self.study.get_studies(is_super_admin=False, expected_code=code)

    def get_studies_id(self, study_id, code):
        """
        @desc - method to validate GET STUDIES by ID endpoint
        :param study_id: ID of the study
        :param code: expected status code
        :return:
        """
        self.study.get_studies_id(study_id=study_id, expected_code=code)

    def study_upload_setup(self, patient_id, code):
        """
        @desc - method to validate StudyUploadSetup endpoint
        :param patient_id: ID of the patient
        :param code: expected status code
        :return: STUDY ID
        """
        self.study_id = self.study.post_study_upload_study_setup(patient_id=patient_id, study_sync_state="0", scale="0",
                                                                 use_video="false", sample_rate="256", harness_id="2",
                                                                 expected_code=code)
        return self.study_id

    def aws_bucket_for_study(self, study_id, code):
        """
        @desc - method to validate AwsBucketForStudy
        :param study_id: ID of the study
        :param code: expected status code
        :return:
        """
        self.study.post_aws_bucket_for_study(study_id=study_id, expected_code=code)

    def delete_study(self, study_id, code):
        """
        @desc - method to validate DELETE STUDY endpoint
        :param study_id: ID of the study to be deleted
        :param code: expected status code
        :return:
        """
        self.study.delete_study_id(study_id=study_id, expected_code=code)
