import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from FacilitiesController import FacilitiesController as fac_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class FacilitiesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""
    created_fac_id = ""

    test_facility_id = "506b92a3-fff7-429e-b3fc-cb3df8c6da69"

    fac = fac_con()

    invitation_id = ""

    def __init__(self):
        self._expression = ''

    def set_test_values_for_facilities_class(self, ):
        self.fac.set_test_values(fac_name="Tempo", fac_dom="Tempo.com")

    def set_token_of_facilities_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.fac.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_facilities_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.fac.set_super_admin_token(token)

    def rotate_token(self, code):
        """
        @desc - method to test rotate token
        :param code:
        :return:
        """
        self.fac.post_rotate_token(fac_id=None, expected_code=code)

    def get_facility_metrics(self, code):
        """
        @desc - method to test get facility metrics. ID of the facility is set to None
        :param code:
        :return:
        """
        self.fac.get_metrics(fac_id=None, expected_code=code)

    def get_facilities(self, code):
        """
        @desc - method to test get all facilities
        :param code:
        :return:
        """
        self.fac.get_get_facilities(expected_code=code)

    def create_facility(self, code):
        """
        @desc - method to test create facility
        :param code:
        :return:
        """
        self.created_fac_id = self.fac.post_create_facility(fac_name="API_TEST_", fac_domain="API_DOMAIN",
                                                            expected_code=code)
        return self.created_fac_id

    def create_facility_named_enosis(self, code):
        """
        @desc - method to create a facility named Enosis
        :param code: expected status code
        :return:
        """
        self.created_fac_id = self.fac.post_create_facility(fac_name="Enosis", fac_domain="API_DOMAIN",
                                                            expected_code=code)
        return self.created_fac_id

    def send_invitation(self, code):
        """
        @desc - method to test send invitation. Email and RoleID is set to none. Therefore, will be set from the
        Facilities controller class
        :param code:
        :return:
        """
        self.fac.get_send_invitation(email=None, role_id=None, expected_code=code)

    def send_invitation_using_email_role(self, email, role_id, code):
        """
        @desc - method to test send invitation using email and role id
        :param email:
        :param role_id:
        :param code:
        :return:
        """
        self.fac.get_send_invitation(email=email, role_id=role_id, expected_code=code)

    def accept_invitation(self, code):
        """
        @desc - Method to test accept invitation
        :param code:
        :return:
        """
        return self.fac.post_accept_invitation(invitation_id=None, expected_code=code)

    def get_invitation(self, code):
        """
        @desc - Method to test get invitation. ID is set to none.
        :param code:
        :return:
        """
        self.fac.get_get_invitation(invitation_id=None, expected_code=code)

    def get_invitation_id(self, invitation_id, code):
        """
        @desc - method to get invitation by id
        :param invitation_id: ID of the invitation
        :param code:
        :return:
        """
        self.fac.get_get_invitation(invitation_id=invitation_id, expected_code=code)

    def get_invitations(self, is_super_admin, email_address, code):
        """
        @desc - Method to test get invitations. Active only is set to True
        :param is_super_admin: IF SuperAdmin or not
        :param email_address: Invited email address
        :param code:
        :return:
        """
        if is_super_admin == "None":
            is_super_admin = None
        if email_address == "None":
            email_address = None
        self.invitation_id = self.fac.get_get_invitations(active_only=True, is_super_admin=is_super_admin,
                                                          email_address=email_address, expected_code=code)
        return self.invitation_id

    def get_active_invitations(self, code):
        """
        @desc - Method to test get active invitations.
        :param code:
        :return:
        """
        self.fac.get_get_invitations(active_only=True, expected_code=code)

    def get_inactive_invitations(self, code):
        """
        @desc - Method to test get inactive invitations.
        :param code:
        :return:
        """
        self.fac.get_get_invitations(active_only=False, expected_code=code)

    def does_invited_user_exist(self, code):
        """
        @desc - Method to test whether an invited user exists or not. ID is set to none
        :param code:
        :return:
        """
        self.fac.get_does_invited_user_exist(inv_id=None, expected_code=code)

    def is_invitation_expired(self, code):
        """
        @desc - Method to test if an invitation is expired. ID is set to none
        :param code:
        :return:
        """
        self.fac.get_is_invitation_expired(invitation_id=None, expected_code=code)

    def delete_facility_id(self, fac_id, code):
        """
        @desc - Method to test deletion of a facility with ID
        :param fac_id:
        :param code:
        :return:
        """
        self.fac.delete_facility(fac_id=fac_id, expected_code=code)

    def delete_facility(self, code):
        """
        @desc - Method to test deletion of a facility.
        :param code:
        :return:
        """
        # self.fac.delete_facility(fac_id=None, expected_code=code)
        self.fac.delete_facility(fac_id=self.test_facility_id, expected_code=code)

    def get_facility_by_id(self, fac_id, code):
        """
        @desc - Method to get facility info by ID.
        :param fac_id:
        :param code:
        :return:
        """
        self.fac.get_facility_id(fac_id=fac_id, expected_code=code)

    def get_facility(self, code):
        """
        @desc - Method to get facility info by ID.
        :param code:
        :return:
        """
        # self.fac.get_facility_id(fac_id=None, expected_code=code)
        self.fac.get_facility_id(fac_id=self.test_facility_id, expected_code=code)

    def patch_facility(self, code):
        """
        @desc - Method to update facility info by PATCH.
        :param code:
        :return:
        """
        # self.fac.patch_facility_id(fac_id=None, expected_code=code)
        self.fac.patch_facility_id(fac_id=self.test_facility_id, expected_code=code)

    def put_facility_by_id(self, fac_id, code):
        """
        @desc - Method to update facility info by PUT.
        :param code:
        :return:
        """
        self.fac.put_facility_id(fac_id=fac_id, expected_code=code)

    def put_facility(self, code):
        """
        @desc - Method to update facility info by PUT.
        :param code:
        :return:
        """
        # self.fac.put_facility_id(fac_id=None, expected_code=code)
        self.fac.put_facility_id(fac_id=self.test_facility_id, expected_code=code)

    class FacilitiesTCs(Exception):
        pass
