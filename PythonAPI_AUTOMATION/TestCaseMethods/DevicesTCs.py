import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from DevicesController import DevicesController as device_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class DevicesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""
    created_device_id = ""

    device = device_con()

    temporary_facility_id = "95aba95c-e070-400e-b342-33741a96d20a"

    def __int__(self):
        self._expression = ''

    def set_token_of_devices_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """

        self.device.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_devices_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: user's token
        :return:
        """
        self.device.set_super_admin_token(token)

    def create_device(self, code):
        """
        @desc - method to check create device endpoint
        :param code: expected status code
        :return:
        """
        self.created_device_id = self.device.post_create_device(device_name="TEST_API", device_sn="1",
                                                                device_config="TEST_API", device_partnumber="TEST_API",
                                                                facility_id=self.facility_id, expected_code=code)
        return self.created_device_id

    def get_by_sn(self, code):
        """
        @desc - method to get device by serial number
        :param code: expected status code
        :return:
        """
        self.device.get_get_by_sn(sn_device=None, expected_code=code)

    def get_devices(self, code):
        """
        @desc - method to get a list of devices
        :param code: expected status code
        :return:
        """
        self.device.get_get_devices(expected_code=code)

    def get_device_id(self, code):
        """
        @desc - method to get a Device by id
        :param code: expected status code
        :return:
        """
        self.device.get_get_devices_id(id_devices=None, expected_code=code)

    def post_devices(self, code):
        """
        @desc - Post device endpoint validation
        :param code: expected status code
        :return:
        """
        self.device.post_devices(expected_code=code)

    def patch_devices(self, code):
        """
        @desc - Patch device endpoint validation
        :param code: expected status code
        :return:
        """
        self.device.patch_devices_id(expected_code=code)

    def put_devices(self, code):
        """
        @desc - Put device endpoint validation
        :param code: expected status code
        :return:
        """
        self.device.put_devices_id(expected_code=code)

    def change_device_facility(self, id_facility, code):
        """
        @desc - ChangeDeviceFacility endpoint validation
        :param code: expected status code
        :param id_facility: facility ID to be changed to
        :return:
        """
#        self.device.post_change_device_facility(device_id=None, facility_id=id_facility, expected_code=code)
        self.device.post_change_device_facility(device_id=None, facility_id=id_facility,
                                                expected_code=code)

    def delete_device_by_id(self, dev_id, code):
        """
        @desc - This method deletes the device with specific ID.
        :param dev_id:
        :param code:
        :return:
        """
        self.device.delete_delete_devices(id_device=dev_id, expected_code=code)

    def delete_devices(self, code):
        """
        @desc - Delete device with ID
        :param code: expected status code
        :return:
        """
        self.device.delete_delete_devices(id_device=None, expected_code=code)

    class DevicesTCs(Exception):
        pass
