import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from FacilityUsersController import FacilityUsersController as fac_usr
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class FacilityUsersTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""

    fac = fac_usr()

    def __init__(self):
        self._expression = ''

    def set_token_of_facility_users_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.fac.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_facility_users_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.fac.set_super_admin_token(token)

    def create_facility_user_with_mail(self, email, code):
        """
        @desc - method to create facility user with email
        :param email:
        :param code:
        :return:
        """
        self.fac.post_create_facility_user(user_email=email, expected_code=code)

    def get_user(self, code):
        """
        @desc - get facility user info based of the created user's id
        :param code:
        :return:
        """
        self.fac.get_get_user(fac_user_id=None, expected_code=code)

    def get_users(self, code):
        """
        @desc - get all facility users info
        :param code:
        :return:
        """
        self.fac.get_get_users(expected_code=code)

    def post_facility_user(self, user_id, code):
        """
        @desc - method to check post facility user
        :param user_id:
        :param code:
        :return:
        """
        self.fac.post_facility_user(fac_id=None, user_id=user_id, expected_code=code)

    def delete_facility_user(self, code):
        """
        @desc - method to delete the created facility user
        :param code:
        :return:
        """
        self.fac.delete_facility_user(fac_user_id=None, expected_code=code)

    def get_facility_user(self, code):
        """
        @desc - get facility user info based of the created facility id
        :param code:
        :return:
        """
        self.fac.get_facility_user(fac_user_id=None, expected_code=code)

    def get_facility_users(self, code):
        """
        @desc - get all facility users' info
        :param code:
        :return:
        """
        self.fac.get_facility_users(expected_code=code)

    def put_facility_user(self, user_id, code):
        """
        @desc - method to check put facility user
        :param user_id:
        :param code:
        :return:
        """
        self.fac.put_facility_user(fac_id=None, user_id=user_id, expected_code=code)

    def patch_facility_user(self, user_id, code):
        """
        @desc - method to check patch facility user
        :param user_id:
        :param code:
        :return:
        """
        self.fac.patch_facility_user(fac_id=None, user_id=user_id, expected_code=code)

    class FacilityUsersTCs(Exception):
        pass
