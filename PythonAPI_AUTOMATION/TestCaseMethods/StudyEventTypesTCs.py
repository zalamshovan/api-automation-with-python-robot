import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'rendrapi'))
sys.path.insert(0, lib_path1)
from StudyEventTypesController import StudyEventTypesController as study_con
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class StudyEventTypesTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""

    study_event_type = study_con()

    def __int__(self):
        self._expression = ''

    def set_token_of_study_event_types_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.study_event_type.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_study_event_types_class_for_super_admin(self, token):
        """
        @desc - method to set the token of super admin user
        :param token: super admin's token
        :return:
        """
        self.study_event_type.set_super_admin_token(token)

    def post_study_event_types(self, code):
        """
        @desc - method to validate POST endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.post_study_event_types(study_event_type_name="TEST_API", description="TEST_API",
                                                     expected_code=code)

    def get_study_event_types(self, code):
        """
        @desc - method to validate GET endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.get_study_event_types(expected_code=code)

    def get_study_event_types_id(self, code):
        """
        @desc - method to validate GET by id endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.get_study_event_types_id(study_event_type_id=None, expected_code=code)

    def patch_study_event_types(self, code):
        """
        @desc - method to validate PATCH endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.patch_study_event_types(type_id=None, name="TEST_API_PATCH", description="TEST_API_PATCH",
                                                      expected_code=code)

    def put_study_event_types(self, code):
        """
        @desc - method to validate PUT endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.put_study_event_types(type_id=None, name="TEST_API_PUT", description="TEST_API_PUT",
                                                    expected_code=code)

    def delete_study_event_types(self, code):
        """
        @desc - method to validate DELETE endpoint
        :param code: expected status code
        :return:
        """
        self.study_event_type.delete_study_event_types(type_id=None, expected_code=code)

    class StudyEventTypesTCs(Exception):
        pass
