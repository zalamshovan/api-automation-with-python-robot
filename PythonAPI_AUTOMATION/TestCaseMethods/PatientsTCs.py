import os
import sys
from collections import Counter

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from PatientsController import PatientsController as pat_con
from StudiesController import StudiesController
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class PatientsTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    super_admin_token = ""

    facility_id = ""
    user_defined_patient_id = ""
    patient_id = ""

    pat = pat_con()
    studies = StudiesController()

    most_commonly_used_patient_name = ""

    def __init__(self):
        self._expression = ''

    def set_token_of_patients_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.pat.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_patients_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.pat.set_super_admin_token(token)
        self.super_admin_token = token

    def get_patient_from_studies_list(self):
        """
        @desc - This method find's the study id associated with the most used patient.
        :return:
        """
        if self.super_admin_token == "":
            raise AssertionError("Please initialize the super_admin_token of PatientsTCs class")
        else:
            self.studies.set_super_admin_token(token=self.super_admin_token)

        response = self.studies.get_studies(is_super_admin=True, expected_code="200")

        json = response.json()

        name_list = []

        for each in range(len(json)):
            if json[each]['FirstName'] == "*****" or json[each]['LastName'] == "*****":
                continue
            full_name = json[each]['FirstName'] + " " + json[each]['LastName']
            study_id = json[each]['ID']
            name_list.append(full_name)
            name_list.append(study_id)

        most_common, num_most_common = Counter(name_list).most_common(1)[0]

        print(most_common)

        found = 0

        for each in range(len(name_list)):
            if name_list[each] == most_common:
                id_of_most_common_patients_study = name_list[each + 1]
                found = 1
                break

        if found == 1:
            print(id_of_most_common_patients_study)
            self.most_commonly_used_patient_name = most_common
            return id_of_most_common_patients_study
        else:
            raise AssertionError("Did not find a study ID.")

    def create_patient(self, code):
        """
        @desc - method to create patient
        :param code:
        :return:
        """
        self.patient_id = self.pat.post_patient(f_name="Z_API_F", l_name="Z_API_L", fac_id=self.facility_id,
                                                expected_code=code)
        return self.patient_id

    def search_patient_by_name(self, pat_name, code):
        if pat_name is None:
            pat_name = self.most_commonly_used_patient_name
        return self.pat.get_search(super_admin=False, search_string=pat_name, expected_code=code)

    def search_patient(self, code):
        """
        @desc - method to search patient
        :param code:
        :return:
        """
        self.pat.get_search(super_admin=False, search_string="Z_API_F", expected_code=code)

    def get_patient_for_editing(self, code):
        """
        @desc - method to get patient info for editing. Patient ID is set to None.
        :param code:
        :return:
        """
        self.pat.get_get_patient_for_editing(pat_id=None, expected_code=code)

    def delete_patient(self, code):
        """
        @desc -  method to delete the created patient.
        :param code:
        :return:
        """
        self.pat.delete_patients(pat_id=None, expected_code=code)

    def delete_patient_id(self, patient_id, code):
        """
        @desc -  method to delete the created patient.
        :param patient_id: ID of the patient to be deleted
        :param code:
        :return:
        """
        self.pat.delete_patients(pat_id=patient_id, expected_code=code)

    def get_patient_by_id(self, pat_id, code):
        """
        @desc - method to get patient info by patient id
        :param pat_id:
        :param code:
        :return:
        """
        self.pat.get_patient_id(pat_id=pat_id, expected_code=code)

    def get_patient(self, code):
        """
        @desc - method to get patient info with the created patient's id
        :param code:
        :return:
        """
        self.pat.get_patient_id(pat_id=None, expected_code=code)

    def get_patients(self, code):
        """
        @desc - method to get all patients info
        :param code:
        :return:
        """
        self.pat.get_patients(expected_code=code)

    def put_patients(self, code):
        """
        @desc - method to edit patient info by Put method
        :param code:
        :return:
        """
        self.pat.put_patient(expected_code=code)

    def patch_patients(self, code):
        """
        @desc - method to edit patient info by Patch method
        :param code:
        :return:
        """
        self.pat.patch_patients(expected_code=code)

    class PatientsTCs(Exception):
        pass
