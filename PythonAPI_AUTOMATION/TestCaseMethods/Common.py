import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from UsersController import UsersController as user_con
from rendr_util import get_api_version, login_to_rendr, get_logged_in_user_facility_id, get_logged_in_user_id
from TestDataProvider import TestDataProvider

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf


class Common(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')
    data = TestDataProvider()

    # variables for newly created test users
    id_super_admin = ""
    id_support = ""
    id_production = ""
    id_facility_admin = ""
    id_review_doctor = ""
    id_lead_tech = ""
    id_field_tech = ""
    id_office_personnel = ""

    token_super_admin = ""
    token_support = ""
    token_production = ""
    token_facility_admin = ""
    token_review_doctor = ""
    token_lead_tech = ""
    token_field_tech = ""
    token_office_personal = ""

    facility_id = ""
    theme_id = ""

    super_admin_email = ""
    support_email = ""
    production_email = ""
    facility_admin_email = ""
    review_doctor_email = ""
    lead_tech_email = ""
    field_tech_email = ""
    office_personnel_email = ""
    password = ""
    test_super_admin_email = ""
    test_support_email = ""
    test_production_email = ""
    test_facility_admin_email = ""
    test_review_doctor_email = ""
    test_lead_tech_email = ""
    test_field_tech_email = ""
    test_office_personnel_email = ""

    user = user_con()

    def __init__(self):
        self._expression = ''
        self.super_admin_email = self.data.get_rendr_user_email("Super Admin")
        self.support_email = self.data.get_rendr_user_email("Support")
        self.production_email = self.data.get_rendr_user_email("Production")
        self.facility_admin_email = self.data.get_rendr_user_email("Facility Admin")
        self.review_doctor_email = self.data.get_rendr_user_email("Review Doctor")
        self.lead_tech_email = self.data.get_rendr_user_email("Lead Tech")
        self.field_tech_email = self.data.get_rendr_user_email("Field Tech")
        self.office_personnel_email = self.data.get_rendr_user_email("Office Personnel")

        self.password = self.data.get_common_password()

        self.test_super_admin_email = self.data.get_test_user_email("Super Admin")
        self.test_support_email = self.data.get_test_user_email("Support")
        self.test_production_email = self.data.get_test_user_email("Production")
        self.test_facility_admin_email = self.data.get_test_user_email("Facility Admin")
        self.test_review_doctor_email = self.data.get_test_user_email("Review Doctor")
        self.test_lead_tech_email = self.data.get_test_user_email("Lead Tech")
        self.test_field_tech_email = self.data.get_test_user_email("Field Tech")
        self.test_office_personnel_email = self.data.get_test_user_email("Office Personnel")

    @staticmethod
    def get_version_number(token):
        """
        @desc - To get the API version number
        :param token: user token
        :return: API version
        """
        return get_api_version(token)

    @staticmethod
    def get_logged_in_user_id(token):
        """
        @desc - This method returns ID of the logged in user
        :param token:
        :return:
        """
        return get_logged_in_user_id(token)

    @staticmethod
    def login_to_ras_with_email_password(email, password):
        """
        @desc - To login into the system
        :param - email: user's email
        :param - password: user's password
        :rtype : token of the logged in user
        """
        token = login_to_rendr(email, password)
        return token

    def login_as_super_admin(self):
        """
        @desc - to login to the system with super admin credentials set in the conf.py file
        :return: token_super_admin
        """
        token_super_admin = login_to_rendr(self.super_admin_email, self.password)
        facility_id = get_logged_in_user_facility_id(token_super_admin)
        self.user.set_token(token_super_admin)
        print("Super Admin (" + self.super_admin_email + ") of Facility ID:" + facility_id + " logged in")
        return token_super_admin

    def login_to_ras(self, role):
        """
        @desc - To login into the system
        :param - role: user's role
        :rtype : token of the logged in user
        """
        if role.lower() == "super admin":
            self.token_super_admin = login_to_rendr(self.test_super_admin_email, self.password)
            return self.token_super_admin
        elif role.lower() == "support":
            self.token_support = login_to_rendr(self.test_support_email, self.password)
            return self.token_support
        elif role.lower() == "production":
            self.token_production = login_to_rendr(self.test_production_email, self.password)
            return self.token_production
        elif role.lower() == "facility admin":
            self.token_facility_admin = login_to_rendr(self.test_facility_admin_email, self.password)
            return self.token_facility_admin
        elif role.lower() == "review doctor":
            self.token_review_doctor = login_to_rendr(self.test_review_doctor_email, self.password)
            return self.token_review_doctor
        elif role.lower() == "lead tech":
            self.token_lead_tech = login_to_rendr(self.test_lead_tech_email, self.password)
            return self.token_lead_tech
        elif role.lower() == "field tech":
            self.token_field_tech = login_to_rendr(self.test_field_tech_email, self.password)
            return self.token_field_tech
        elif role.lower() == "office personnel":
            self.token_office_personal = login_to_rendr(self.test_office_personnel_email, self.password)
            return self.token_office_personal
        else:
            raise AssertionError("Please select the correct user role to log in properly.")

    def change_selected_facility(self, facility_name):
        if facility_name.lower() == "enosis":
            self.user.post_set_selected_facility(facility_id=conf.enosis_facility_id, expected_code="200")
        elif facility_name.lower() == "mobilemedtek":
            self.user.post_set_selected_facility(facility_id=conf.mobile_med_tek_facility_id, expected_code="200")

    def change_selected_facility_to_enosis(self, facility_id):
        """
        @desc - method to change the selected facility to enosis
        :param facility_id: ID of enosis facility
        :return:
        """
        self.user.post_set_selected_facility(facility_id=facility_id, expected_code="200")

    def create_test_users_at_enosis_facility(self, role, code):
        """
        @desc - Create uses at enosis facility to carry on the automation
        :param role: type of role the user will have
        :param code:
        :return:
        """
        token = self.login_as_super_admin()

        self.user.set_super_admin_token(token)
        # changing super admin's selected facility to Enosis facility
        self.user.post_set_selected_facility(facility_id=conf.enosis_facility_id, expected_code="200")

        if role.lower() == "super admin":
            self.id_super_admin = self.user.post_create_user(first_name="API", last_name="SuperAdmin",
                                                             email=self.test_super_admin_email,
                                                             password=self.password,
                                                             role=conf.super_admin_role,
                                                             delete_if_conflicts=True,
                                                             expected_code=code)
            return self.id_super_admin
        elif role.lower() == "support":
            self.id_support = self.user.post_create_user(first_name="API", last_name="Support",
                                                         email=self.test_support_email,
                                                         password=self.password,
                                                         role=conf.support_role,
                                                         delete_if_conflicts=True,
                                                         expected_code=code)
            return self.id_support
        elif role.lower() == "production":
            self.id_production = self.user.post_create_user(first_name="API", last_name="Production",
                                                            email=self.test_production_email,
                                                            password=self.password,
                                                            role=conf.production_role,
                                                            delete_if_conflicts=True,
                                                            expected_code=code)
            return self.id_production
        elif role.lower() == "facility admin":
            self.id_facility_admin = self.user.post_create_user(first_name="API", last_name="FacilityAdmin",
                                                                email=self.test_facility_admin_email,
                                                                password=self.password,
                                                                role=conf.facility_admin_role,
                                                                delete_if_conflicts=True,
                                                                expected_code=code)
            return self.id_facility_admin
        elif role.lower() == "review doctor":
            self.id_review_doctor = self.user.post_create_user(first_name="API", last_name="Review",
                                                               email=self.test_review_doctor_email,
                                                               password=self.password,
                                                               role=conf.review_doctor_role,
                                                               delete_if_conflicts=True,
                                                               expected_code=code)
            return self.id_review_doctor
        elif role.lower() == "lead tech":
            self.id_lead_tech = self.user.post_create_user(first_name="API", last_name="Lead",
                                                           email=self.test_lead_tech_email,
                                                           password=self.password,
                                                           role=conf.lead_tech_role,
                                                           delete_if_conflicts=True,
                                                           expected_code=code)
            return self.id_lead_tech
        elif role.lower() == "field tech":
            self.id_field_tech = self.user.post_create_user(first_name="API", last_name="Field",
                                                            email=self.test_field_tech_email,
                                                            password=self.password,
                                                            role=conf.field_tech_role,
                                                            delete_if_conflicts=True,
                                                            expected_code=code)
            return self.id_field_tech
        elif role.lower() == "office personnel":
            self.id_office_personnel = self.user.post_create_user(first_name="API", last_name="Office",
                                                                  email=self.test_office_personnel_email,
                                                                  password=self.password,
                                                                  role=conf.office_personnel_role,
                                                                  delete_if_conflicts=True,
                                                                  expected_code=code)
            return self.id_office_personnel
        else:
            raise AssertionError("Wrong role name selected.")

    def create_test_users(self, role, code):
        """
        @desc - Create uses to carry on the automation
        :param role: type of role the user will have
        :param code:
        :return:
        """
        print("1st")
        token = self.login_as_super_admin()
        print("2nd")

        self.user.set_super_admin_token(token)
        # changing super admin's selected facility to MobileMedTek facility
        self.user.post_set_selected_facility(facility_id=conf.mobile_med_tek_facility_id, expected_code="200")

        if role.lower() == "super admin":
            self.id_super_admin = self.user.post_create_user(first_name="API", last_name="SuperAdmin",
                                                             email=self.test_super_admin_email,
                                                             password=conf.password,
                                                             role=conf.super_admin_role,
                                                             delete_if_conflicts=True,
                                                             expected_code=code)
            return self.id_super_admin
        elif role.lower() == "support":
            self.id_support = self.user.post_create_user(first_name="API", last_name="Support",
                                                         email=self.test_support_email,
                                                         password=conf.password,
                                                         role=conf.support_role,
                                                         delete_if_conflicts=True,
                                                         expected_code=code)
            return self.id_support
        elif role.lower() == "production":
            self.id_production = self.user.post_create_user(first_name="API", last_name="Production",
                                                            email=self.test_production_email,
                                                            password=conf.password,
                                                            role=conf.production_role,
                                                            delete_if_conflicts=True,
                                                            expected_code=code)
            return self.id_production
        elif role.lower() == "facility admin":
            self.id_facility_admin = self.user.post_create_user(first_name="API", last_name="FacilityAdmin",
                                                                email=self.test_facility_admin_email,
                                                                password=conf.password,
                                                                role=conf.facility_admin_role,
                                                                delete_if_conflicts=True,
                                                                expected_code=code)
            return self.id_facility_admin
        elif role.lower() == "review doctor":
            self.id_review_doctor = self.user.post_create_user(first_name="API", last_name="Review",
                                                               email=self.test_review_doctor_email,
                                                               password=conf.password,
                                                               role=conf.review_doctor_role,
                                                               delete_if_conflicts=True,
                                                               expected_code=code)
            return self.id_review_doctor
        elif role.lower() == "lead tech":
            self.id_lead_tech = self.user.post_create_user(first_name="API", last_name="Lead", email=self.test_lead_tech_email,
                                                           password=conf.password,
                                                           role=conf.lead_tech_role,
                                                           delete_if_conflicts=True,
                                                           expected_code=code)
            return self.id_lead_tech
        elif role.lower() == "field tech":
            self.id_field_tech = self.user.post_create_user(first_name="API", last_name="Field", email=self.test_field_tech_email,
                                                            password=conf.password,
                                                            role=conf.field_tech_role,
                                                            delete_if_conflicts=True,
                                                            expected_code=code)
            return self.id_field_tech
        elif role.lower() == "office personnel":
            self.id_office_personnel = self.user.post_create_user(first_name="API", last_name="Office",
                                                                  email=self.test_office_personnel_email,
                                                                  password=conf.password,
                                                                  role=conf.office_personnel_role,
                                                                  delete_if_conflicts=True,
                                                                  expected_code=code)
            return self.id_office_personnel
        else:
            raise AssertionError("Wrong role name selected.")

    def delete_user_with_id(self, user_id, code):
        """
        @desc - Delete a user with ID. Super Admin logs in to delete the user.
        :param user_id:
        :param code:
        :return:
        """
        self.login_as_super_admin()
        print("Found user id to delete: " + user_id)
        self.user.delete_delete_user_id(user_id, expected_code=code)

    def delete_created_users(self, role, code):
        """
        @desc - to delete a user created at the beginning of the test
        :param role: Role of the user who will get deleted
        :param code:
        :return:
        """
        if role.lower() == "super admin":
            self.user.delete_delete_user_id(user_id=self.id_super_admin, expected_code=code)
        elif role.lower() == "support":
            self.user.delete_delete_user_id(user_id=self.id_support, expected_code=code)
        elif role.lower() == "production":
            self.user.delete_delete_user_id(user_id=self.id_production, expected_code=code)
        elif role.lower() == "facility admin":
            self.user.delete_delete_user_id(user_id=self.id_facility_admin, expected_code=code)
        elif role.lower() == "review doctor":
            self.user.delete_delete_user_id(user_id=self.id_review_doctor, expected_code=code)
        elif role.lower() == "field tech":
            self.user.delete_delete_user_id(user_id=self.id_field_tech, expected_code=code)
        elif role.lower() == "lead tech":
            self.user.delete_delete_user_id(user_id=self.id_lead_tech, expected_code=code)
        elif role.lower() == "office personnel":
            self.user.delete_delete_user_id(user_id=self.id_office_personnel, expected_code=code)
        else:
            raise AssertionError("Wrong role name selected.")

    def get_user_email(self, role):
        """
        @desc - returns test user's email address from the conf file based on role
        :param role: Role of the user who's email address is needed
        :return:
        """
        if role.lower() == "super admin":
            return self.test_super_admin_email
        elif role.lower() == "support":
            return self.test_support_email
        elif role.lower() == "production":
            return self.test_production_email
        elif role.lower() == "facility admin":
            return self.test_facility_admin_email
        elif role.lower() == "review doctor":
            return self.test_review_doctor_email
        elif role.lower() == "field tech":
            return self.test_lead_tech_email
        elif role.lower() == "lead tech":
            return self.test_field_tech_email
        elif role.lower() == "office personnel":
            return self.test_office_personnel_email
        else:
            raise AssertionError("Wrong role name selected.")

    def get_user_token(self, role):
        """
        @desc - To get the token of logged in user
        :param - role: user's role
        :rtype : token of the logged in user
        """
        print(role)
        if role.lower() == "super admin":
            return self.login_to_ras(role=role)
        elif role.lower() == "support":
            return self.login_to_ras(role=role)
        elif role.lower() == "production":
            return self.login_to_ras(role=role)
        elif role.lower() == "facility admin":
            return self.login_to_ras(role=role)
        elif role.lower() == "review doctor":
            return self.login_to_ras(role=role)
        elif role.lower() == "lead tech":
            return self.login_to_ras(role=role)
        elif role.lower() == "field tech":
            return self.login_to_ras(role=role)
        elif role.lower() == "office personnel":
            return self.login_to_ras(role=role)
        else:
            raise AssertionError("Please select the correct user role to get token.")

    def work_with_json_data(self, code):
        """
        @desc - THIS METHOD WAS CREATED FOR TEST PURPOSE
        :param code:
        :return:
        """
        json_response = self.user.get_get_my_theme(code)

        print("THEME ID :" + json_response['ID'])
        print("THEME GROUPS ID :" + json_response['ThemeGroups'][0]['ID'])
        print("ThemeChannelTypes ID 0:" + json_response['ThemeChannelTypes'][0]['ID'])
        print("ThemeChannelTypes ID 1:" + json_response['ThemeChannelTypes'][1]['ID'])

        for each in json_response['ThemeChannelTypes']:
            print("ThemeChannelTypes ID :" + each['ID'])
            print("ThemeChannelTypes Color :" + each['Color'])

    class Common(Exception):
        pass
