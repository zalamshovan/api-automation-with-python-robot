import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'rendrapi'))
sys.path.insert(0, lib_path1)
from StudiesController import StudiesController as study_con
from StudyRecordingsController import StudyRecordingsController as study_recording


class StudyRecordingsTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    study = study_con()
    study_rec = study_recording()

    recording_index = ""
    camera_index = ""
    video_index = ""

    study_id = ""

    def __int__(self):
        self._expression = ''

    def set_token_of_study_recordings_class(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.study_rec.set_token(token)

    def set_token_of_studies_class(self, token):
        """
        @desc - method to set token of the user of studies class
        :param token: user's token
        :return:
        """
        self.study.set_token(token)

    def get_studies_id(self, code):
        """
        @desc - method to get studyID, recordingIndex, cameraIndex & videoIndex
        :param code: expected status code
        :return:
        """
        response = self.study.get_studies(is_super_admin=None, expected_code=code)
        json_response = response.json()
        length = len(json_response)
        flag = 0
        flag_for_outer_loop = 0
        for i in range(length):
            if str(json_response[i]['UseVideo']) == 'True' and str(json_response[i]['SyncInfo']['SyncState']) == '1'\
                    and str(json_response[i]['SyncInfo']['UseVideo']) == 'True' \
                    and str(json_response[i]['RecordingCount']) == '1':
                self.study_id = str(json_response[i]['ID'])
                response_by_id = self.study.get_studies_id(study_id=self.study_id, expected_code=code)
                json_response_by_id = response_by_id.json()
                for each in range(len(json_response_by_id['StudyRecordings'])):
                    if json_response_by_id['StudyRecordings'][each]['Index']:
                        for data in range(len(json_response_by_id['StudyRecordings'][each]['Videos'])):
                            if json_response_by_id['StudyRecordings'][each]['Videos'][data]['VideoIndex'] and \
                                    json_response_by_id['StudyRecordings'][each]['Videos'][data]['CameraIndex']:
                                print(json_response_by_id)
                                self.recording_index = str(json_response_by_id['StudyRecordings'][each]['Index'])
                                print(self.recording_index)
                                self.video_index = \
                                    str(json_response_by_id['StudyRecordings'][each]['Videos'][data]['VideoIndex'])
                                print(self.video_index)
                                self.camera_index = \
                                    str(json_response_by_id['StudyRecordings'][each]['Videos'][data]['CameraIndex'])
                                print(self.camera_index)
                                flag = 1
                                break
                        if flag == 1:
                            flag_for_outer_loop = 1
                            break
            if flag_for_outer_loop == 1:
                break
        print("ID of the study selected: {}".format(self.study_id))
        if flag == 0:
            print("Could not find a study with video, camera index & video index! Something went wrong!")
        return self.study_id

    def get_study_recording(self, code):
        """
        @desc - method to validate GetStudyRecording endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.get_get_study_recording(study_id=self.study_id, recording_index=self.recording_index,
                                               expected_code=code)

    def update_study_recording_notes(self, code):
        """
        @desc - method to validate UpdateStudyRecordingNotes endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.post_update_study_recording_notes(study_id=self.study_id, index=self.recording_index,
                                                         notes="TEST_API", expected_code=code)

    def get_last_sync_packet_index(self, code):
        """
        @desc - method to validate GetLastSyncPacketIndex endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.get_get_last_sync_packet_index(study_id=self.study_id, index=self.recording_index,
                                                      expected_code=code)

    def calculate_server_checksum(self, code):
        """
        @desc - method to validate CalculateServerChecksum endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.get_calculate_server_checksum(study_id=self.study_id, index=self.recording_index,
                                                     expected_code=code)

    def calculate_server_video_checksum(self, code):
        """
        @desc - method to validate CalculateServerVideoChecksum endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.get_calculate_server_video_checksum(study_id=self.study_id, recording_index=self.recording_index,
                                                           camera_index=self.camera_index, video_index=self.video_index,
                                                           expected_code=code)

    def get_server_video_size(self, code):
        """
        @desc - methdo to validate GetServerVideoSize endpoint
        :param code: expected status code
        :return:
        """
        self.study_rec.get_get_server_video_size(study_id=self.study_id, recording_index=self.recording_index,
                                                 video_index=self.video_index, camera_index=self.camera_index,
                                                 expected_code=code)

    class StudyRecordingsTCs(Exception):
        pass
