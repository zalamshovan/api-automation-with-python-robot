import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from StudyEventsController import StudyEventsController
from StudiesController import StudiesController
from rendr_util import login_to_rendr, get_logged_in_user_facility_id


class StudyEventsTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    token_super_admin = ""

    facility_id = ""
    study_id = ""
    recording_index = ""
    packet_count = ""

    study_events = StudyEventsController()
    studies = StudiesController()

    def __init__(self):
        self._expression = ''

    def set_token_of_study_events_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.study_events.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)

    def set_token_of_study_events_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.token_super_admin = token
        self.study_events.set_super_admin_token(token)

    def set_token_of_studies_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.studies.set_super_admin_token(token)

    def set_study_id_and_recording_index(self):
        """
        This method sets the value of Study ID, Recording ID and Packet Count
        :return:
        """
        if self.token_super_admin == "":
            raise AssertionError("Please set the Token of SuperAdmin!!")
        else:
            self.set_token_of_studies_class_for_super_admin(token=self.token_super_admin)
        response = self.studies.get_studies(is_super_admin=True, expected_code="200")

        print("RESPONSE : " + response.text)

        json = response.json()
        is_found = 0

        for each in range(len(json)):
            if is_found == 1:
                break
            if json[each]['SyncInfo']['SyncState'] == 1 and len(json[each]['SyncInfo']['StudyRecordings']) > 0:
                for data in range(len(json[each]['SyncInfo']['StudyRecordings'])):
                    if (json[each]['SyncInfo']['StudyRecordings'][data]['SyncState'] == 1) and (
                            10000 < json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount'] < 20000):
                        print("INDEX -> " + str(json[each]['SyncInfo']['StudyRecordings'][data]['Index']))
                        self.recording_index = str(json[each]['SyncInfo']['StudyRecordings'][data]['Index'])
                        print("STUDY ID -> " + json[each]['SyncInfo']['StudyRecordings'][data]['StudyId'])
                        self.study_id = json[each]['SyncInfo']['StudyRecordings'][data]['StudyId']
                        print("PACKET COUNT -> " + str(
                            json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount']))
                        self.packet_count = str(
                            json[each]['SyncInfo']['StudyRecordings'][data]['PacketCount'])
                        is_found = 1
                        break
        if is_found == 1:
            return self.study_id

    def add_study_events(self, code):
        """
        @desc - Method to add study events
        :param code:
        :return:
        """
        self.set_study_id_and_recording_index()
        self.study_events.post_add_study_events(study_id=self.study_id, recording_index=self.recording_index,
                                                start_packet_index=str(int(self.packet_count)+100),
                                                end_packet_index=str(int(self.packet_count)+200), comment="API_TEST",
                                                expected_code=code)

    def get_study_events_by_study_id(self, code):
        """
        @desc - Method to get study events by study id
        :param code:
        :return:
        """
        self.study_events.get_get_study_events_by_study_id(is_super_admin=None, study_id=self.study_id, expected_code=code)

    def get_study_events_by_event_id(self, code):
        """
        @desc - Method to get study events by event ID
        :param code:
        :return:
        """
        self.study_events.get_study_events_id(event_id=None, expected_code=code)

    def patch_study_event(self, code):
        """
        @desc - Method to call patch request. 204 is returned for successful call
        :param code:
        :return:
        """
        self.study_events.patch_study_event(event_id=None, expected_code=code)

    def put_study_event(self, code):
        """
        @desc - Method to call put request
        :param code:
        :return:
        """
        self.study_events.put_study_events(event_id=None, expected_code=code)

    def get_study_events(self, code):
        """
        @desc - Method to get all study events
        :param code:
        :return:
        """
        self.study_events.get_study_events(expected_code=code)

    def post_study_event(self, code):
        """
        @desc - Method to call post study event. 201 is returned for successful call
        :param code:
        :return:
        """
        self.study_events.post_study_event(expected_code=code)

    def delete_study_event(self, code):
        """
        @desc - Method to delete Study event by event id.
        :param code:
        :return:
        """
        self.study_events.delete_study_event(event_id=None, expected_code=code)

    class StudyEventsTCs(Exception):
        pass
