import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)
from logger_robot import logger as logger_extended

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from AccountController import AccountController
from rendr_util import get_logged_in_user_facility_id, get_logged_in_user_id
from TestDataProvider import TestDataProvider


class AccountTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    log = logger_extended().get_logger('API_Automation')

    facility_id = ""
    logged_in_user_id = ""

    authorized_access = ""
    reset_user_password_access = ""
    forgot_password_access = ""
    account_recovery_access = ""
    user_info_access = ""
    is_authorized_for_device_use_access = ""
    logout_access = ""
    get_manage_info_access = ""
    change_password_access = ""
    set_password_access = ""
    add_external_login_access = ""
    remove_login_access = ""
    get_external_login_access = ""
    get_external_logins_access = ""
    register_access = ""
    login_access = ""

    account = AccountController()

    def initialize_access_for_user(self, user_role):
        if user_role in ('Super Admin',
                         'Support',
                         'Production',
                         'Facility Admin',
                         'Review Doctor',
                         'Lead Tech',
                         'Field Tech',
                         'Office Personnel'):
            data = TestDataProvider()
            self.authorized_access = data.get_access_status(controller_name="AccountController",
                                                            endpoint_name="Authorized", user_role=user_role)
            self.reset_user_password_access = data.get_access_status(controller_name="AccountController",
                                                                     endpoint_name="ResetUserPassword",
                                                                     user_role=user_role)
            self.forgot_password_access = data.get_access_status(controller_name="AccountController",
                                                                 endpoint_name="ForgotPassword", user_role=user_role)
            self.account_recovery_access = data.get_access_status(controller_name="AccountController",
                                                                  endpoint_name="AccountRecovery", user_role=user_role)
            self.user_info_access = data.get_access_status(controller_name="AccountController",
                                                           endpoint_name="UserInfo", user_role=user_role)
            self.is_authorized_for_device_use_access = data.get_access_status(controller_name="AccountController",
                                                                              endpoint_name="IsAuthorizedForDeviceUse",
                                                                              user_role=user_role)
            self.logout_access = data.get_access_status(controller_name="AccountController",
                                                        endpoint_name="Logout", user_role=user_role)
            self.get_manage_info_access = data.get_access_status(controller_name="AccountController",
                                                                 endpoint_name="GetManageInfo", user_role=user_role)
            self.change_password_access = data.get_access_status(controller_name="AccountController",
                                                                 endpoint_name="ChangePassword", user_role=user_role)
            self.set_password_access = data.get_access_status(controller_name="AccountController",
                                                              endpoint_name="SetPassword", user_role=user_role)
            self.add_external_login_access = data.get_access_status(controller_name="AccountController",
                                                                    endpoint_name="AddExternalLogin",
                                                                    user_role=user_role)
            self.remove_login_access = data.get_access_status(controller_name="AccountController",
                                                              endpoint_name="RemoveLogin", user_role=user_role)
            self.get_external_login_access = data.get_access_status(controller_name="AccountController",
                                                                    endpoint_name="GetExternalLogin",
                                                                    user_role=user_role)
            self.get_external_logins_access = data.get_access_status(controller_name="AccountController",
                                                                     endpoint_name="GetExternalLogins",
                                                                     user_role=user_role)
            self.register_access = data.get_access_status(controller_name="AccountController",
                                                          endpoint_name="Register", user_role=user_role)
            self.login_access = data.get_access_status(controller_name="AccountController",
                                                       endpoint_name="Login", user_role=user_role)

            # print(self.authorized_access)
            # print(self.reset_user_password_access)
            # print(self.forgot_password_access)
            # print(self.account_recovery_access)
            # print(self.user_info_access)
            # print(self.is_authorized_for_device_use_access)
            # print(self.logout_access)
            # print(self.get_manage_info_access)
            # print(self.change_password_access)
            # print(self.set_password_access)
            # print(self.add_external_login_access)
            # print(self.remove_login_access)
            # print(self.get_external_login_access)
            # print(self.get_external_logins_access)
            # print(self.register_access)
            # print(self.login_access)

    def set_token_of_account_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.account.set_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)
        self.logged_in_user_id = get_logged_in_user_id(token)

    def set_token_of_account_class_for_super_admin(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.account.set_super_admin_token(token)
        self.facility_id = get_logged_in_user_facility_id(token)
        self.logged_in_user_id = get_logged_in_user_id(token)

    def authorized(self, code=None):
        """
        @desc - Authorized check
        :param code:
        :return:
        """
        if code is None:
            code = self.authorized_access
        self.account.get_authorized(expected_code=code)

    def reset_user_password(self, user_id, code=None):
        """
        @desc - resetting a user password
        :param user_id:
        :param code:
        :return:
        """
        if code is None:
            code = self.reset_user_password_access
        if user_id is None:
            user_id = self.logged_in_user_id

        return self.account.post_reset_user_password(user_id=user_id, expected_code=code)

    def forget_password(self, email, code=None):
        """
        @desc - method to send password reset link
        :param email:
        :param code:
        :return:
        """
        if code is None:
            code = self.forgot_password_access
        self.account.post_forget_password(email=email, expected_code=code)

    # DO NOT CALL THIS ONE
    def account_recovery(self, user_id, code=None):
        """
        @desc - method to recover account. Not performing now as API cannot prove the CODE
        :param user_id:
        :param code:
        :return:
        """
        if code is None:
            code = self.account_recovery_access
        if user_id is None:
            user_id = self.logged_in_user_id
        self.account.post_account_recovery(user_id=user_id, password=None, code=None, expected_code=code)

    def user_info(self, code=None):
        """
        @desc - get user info
        :param code:
        :return:
        """
        if code is None:
            code = self.user_info_access
        self.account.get_user_info(expected_code=code)

    def is_authorized_for_device_use(self, device_id, code=None):
        """
        @desc - checking if device is authorized for device
        :param device_id:
        :param code:
        :return:
        """
        if code is None:
            code = self.is_authorized_for_device_use_access
        self.account.get_is_authorized_for_device_use(device_id=device_id, expected_code=code)

    # DO NOT CALL THIS ONE
    def manage_info(self, code=None):
        """

        :param code:
        :return:
        """
        if code is None:
            code = self.get_manage_info_access
        self.account.get_manage_info(return_url=None, general_state=None, expected_code=code)

    def change_password(self, code=None):
        """
        @desc - To change logged in user password
        :param code:
        :return:
        """
        if code is None:
            code = self.change_password_access
        self.account.post_change_password(old_password="Enosis123", new_password="Enosis123", expected_code=code)

    def set_password(self, code=None):
        """
        @desc - To set password
        :param code:
        :return:
        """
        if code is None:
            code = self.set_password_access
        self.account.post_set_password(new_password="Enosis123", expected_code=code)

    # DO NOT CALL THIS ONE
    def add_external_login(self, code=None):
        """

        :param code:
        :return:
        """
        if code is None:
            code = self.add_external_login_access
        self.account.post_add_external_login(token=None, expected_code=code)

    # DO NOT CALL THIS ONE
    def remove_login(self, code=None):
        """

        :param code:
        :return:
        """
        if code is None:
            code = self.remove_login_access
        self.account.post_remove_login(login_provider=None, provider_key=None, expected_code=code)

    # DO NOT CALL THIS ONE
    def external_login(self, code=None):
        """

        :param code:
        :return:
        """
        if code is None:
            code = self.add_external_login_access
        self.account.get_external_login(provider=None, error=None, expected_code=code)

    # DO NOT CALL THIS ONE
    def external_logins(self, code=None):
        """

        :param code:
        :return:
        """
        if code is None:
            code = self.get_external_logins_access
        self.account.get_external_logins(return_url=None, generate_state=None, expected_code=code)

    def log_out(self, code=None):
        """
        @desc - method to log out
        :param code:
        :return:
        """
        if code is None:
            code = self.logout_access
        self.account.post_log_out(expected_code=code)

    class AccountTCs(Exception):
        pass
