import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'Utilities'))
sys.path.insert(0, lib_path)

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path1)
from UsersController import UsersController as user_con

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf


class UsersTCs(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    facility_id = ""
    theme_id = ""
    created_user_id = ""

    user = user_con()

    def __init__(self):
        self._expression = ''

    def set_token_of_users_class(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user.set_token(token)

    def set_token_of_users_class_for_super_admin(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user.set_super_admin_token(token)

    def create_user(self, role, code):
        """
        @desc - Create user using  /api/Users/CreateUser endpoint
        :param - role: user's role
        :param - code: expected status code
        :rtype : token of the logged in user
        """
        if role.lower() == "super admin":
            user_role = conf.super_admin_role
        elif role.lower() == "support":
            user_role = conf.support_role
        elif role.lower() == "production":
            user_role = conf.production_role
        elif role.lower() == "facility admin":
            user_role = conf.facility_admin_role
        elif role.lower() == "review doctor":
            user_role = conf.review_doctor_role
        elif role.lower() == "lead tech":
            user_role = conf.lead_tech_role
        elif role.lower() == "field tech":
            user_role = conf.field_tech_role
        elif role.lower() == "office personnel":
            user_role = conf.office_personnel_role
        else:
            raise AssertionError("Wrong Role selected")

        self.created_user_id = self.user.post_create_user(first_name="API", last_name="API",
                                                          email="zubairibnealam@ymail.com", password="Enosis123",
                                                          role=user_role, delete_if_conflicts=True,
                                                          expected_code=code)

    def change_user_role(self, role, code):
        """
        @desc - Change user role /api/Users/ChangeUserRole endpoint
        :param - role: role which will be assigned
        :param - code: expected status code
        """
        if role.lower() == "super admin":
            user_role = conf.super_admin_role
        elif role.lower() == "support":
            user_role = conf.support_role
        elif role.lower() == "production":
            user_role = conf.production_role
        elif role.lower() == "facility admin":
            user_role = conf.facility_admin_role
        elif role.lower() == "review doctor":
            user_role = conf.review_doctor_role
        elif role.lower() == "lead tech":
            user_role = conf.lead_tech_role
        elif role.lower() == "field tech":
            user_role = conf.field_tech_role
        elif role.lower() == "office personnel":
            user_role = conf.office_personnel_role
        else:
            raise AssertionError("Wrong Role selected")

        self.user.post_change_user_role(user_id=self.created_user_id, role_id=user_role,
                                        expected_code=code)

    def get_my_roles(self, code):
        """
        @desc - Get user role
        :param - code: expected status code
        """
        self.user.get_get_my_roles(code)

    def get_my_theme(self, code):
        """
        @desc - Get my theme. Set the value of theme_id
        :param - code: expected status code
        """
        json_response = self.user.get_get_my_theme(code)
        if json_response is not None:
            self.theme_id = json_response['ID']
        else:
            print("No theme was selected. Theme id is set to the Dark theme id")
            self.theme_id = conf.dark_theme_id

    def set_my_theme(self, code):
        """
        @desc - Set my theme. Theme id is taken from theme_id variable.
        :param - code: expected status code
        """
        self.user.post_set_my_theme(self.theme_id, code)

    def set_selected_facility(self, code):
        """
        @desc - Set selected facility to MobileMedTek. Facility id is taken from conf file.
        :param - code: expected status code
        """
        self.user.post_set_selected_facility(facility_id=conf.mobile_med_tek_facility_id, expected_code=code)

    def is_user_in_role(self, code):
        """
        @desc - Is user in role endpoint check. Task of this endpoint is unclear.
        :param - code: expected status code
        """
        self.user.post_is_user_in_role(role_name="NEED TO ASK MATT ABOUT IT", expected_code=code)

    def get_user_info(self, code):
        """
        @desc - Check get user info with id
        :param - code: expected status code
        """
        self.user.get_get_user_info(user_id=self.created_user_id, expected_code=code)

    def get_users(self, code):
        """
        @desc - Get users endpoint check.
        :param - code: expected status code
        """
        self.user.get_get_users(code)

    def get_user_id(self, code):
        """
        @desc - Get user with id endpoint check. Id is set to the id of created user.
        :param - code: expected status code
        """
        self.user.get_get_user_id(user_id=self.created_user_id, expected_code=code)

    def patch_user(self, code):
        """
        @desc - Patch user endpoint check. The values were initialized in UsersController class when user was created.
        :param - code: expected status code
        """
        self.user.patch_user_id(code)

    def put_user(self, code):
        """
        @desc - Put user endpoint check. The values were initialized in UsersController class when user was created.
        :param - code: expected status code
        """
        self.user.put_users(code)

    def post_user(self, code):
        """
        @desc - Post user endpoint check. The values were initialized in UsersController class when user was created.
        has to ask Matt for details.
        :param - code: expected status code
        """
        self.user.post_users(code)

    def delete_facility_user(self, code):
        """
        @desc - Delete user from facility check. ID is the id of created user.
        :param - code: expected status code
        """
        self.user.delete_delete_facility_user(user_id=None, expected_code=code)

    def delete_facility_user_with_id(self, id, code):
        """
        @desc - Delete user from facility check. ID is the id of created user.
        :param - id: user's id
        :param - code: expected status code
        """
        self.user.delete_delete_facility_user(user_id=id, expected_code=code)

    def delete_user(self, code):
        """
        @desc - Delete user from RAS check. ID is the id of created user.
        :param - code: expected status code
        """
        self.user.delete_delete_user_id(user_id=self.created_user_id, expected_code=code)

    class UsersTCs(Exception):
        pass
