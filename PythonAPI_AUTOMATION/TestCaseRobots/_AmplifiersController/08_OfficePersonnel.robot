*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/AmplifiersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  office personnel    201
    ${token_for_version}=  get user token    office personnel
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   office personnel   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods


*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Office Personnel User
    ${token}=  login to ras    office personnel
    set suite variable  ${token}
    set token of amplifiers class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of amplifiers class for super admin  ${super_admin_token}

# AMPLIFIERS CONTROLLER
OfficePersonnel User cannot access Create Amplifier
    [Tags]    AmplifiersController      CreateAmplifier
    create amplifier  403

OfficePersonnel User cannot access Get By SN
    [Tags]    AmplifiersController      GetBySn
    get by sn  403

OfficePersonnel User can access Get Amplifer Types
    [Tags]    AmplifiersController      GetAmplifierTypes
    get amplifier types  200

OfficePersonnel User can access Get Amplifer Modes
    [Tags]    AmplifiersController      GetModesForAmplifier
    get modes for amplifier  200

OfficePersonnel User can access Get Sample Rates for Amplifer
    [Tags]    AmplifiersController      GetSampleRatesForAmplifier
    get sample rates for amplifier  200

OfficePersonnel User cannot access Get Amplifers
    [Tags]    AmplifiersController      Get
    get amplifiers  403

OfficePersonnel User cannot access Get Amplifer with ID
    [Tags]    AmplifiersController      Get?id=
    get amplifier id  403

OfficePersonnel User cannot access Post Amplifer with ID
    [Tags]    AmplifiersController      Post
    post amplifiers  403

OfficePersonnel User cannot access Patch Amplifer with ID
    [Tags]    AmplifiersController      Patch
    patch amplifiers  403

OfficePersonnel User cannot access Put Amplifer with ID
    [Tags]    AmplifiersController      Put
    put amplifiers  403

OfficePersonnel User cannot access Delete Amplifer with ID
    [Tags]    AmplifiersController      Delete
    delete amplifier  403