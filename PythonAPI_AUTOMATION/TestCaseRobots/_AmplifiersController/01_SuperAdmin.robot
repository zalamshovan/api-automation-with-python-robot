*** Settings ***
Documentation     Test Cases to validate all Amplifiers Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/AmplifiersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${token_for_version}=  get user token    super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   super admin   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=  login to ras    super admin
    set suite variable  ${token}
    set token of amplifiers class  ${token}
    set token of amplifiers class for super admin  ${token}

# AMPLIFIERS CONTROLLER
SuperAdmin can access Create Amplifier
    [Tags]    AmplifiersController      CreateAmplifier
    create amplifier  201

SuperAdmin can access Get By SN
    [Tags]    AmplifiersController      GetBySn
    get by sn  200

SuperAdmin can access Get Amplifer Types
    [Tags]    AmplifiersController      GetAmplifierTypes
    get amplifier types  200

SuperAdmin can access Get Amplifer Modes
    [Tags]    AmplifiersController      GetModesForAmplifier
    get modes for amplifier  200

SuperAdmin can access Get Sample Rates for Amplifer
    [Tags]    AmplifiersController      GetSampleRatesForAmplifier
    get sample rates for amplifier  200

SuperAdmin can access Get Amplifers
    [Tags]    AmplifiersController      Get
    get amplifiers  200

SuperAdmin can access Get Amplifer with ID
    [Tags]    AmplifiersController      Get?id=
    get amplifier id  200

SuperAdmin can access Post Amplifer with ID
    [Tags]    AmplifiersController      Post
    post amplifiers  201

SuperAdmin can access Patch Amplifer with ID
    [Tags]    AmplifiersController      Patch
    patch amplifiers  500

SuperAdmin can access Put Amplifer with ID
    [Tags]    AmplifiersController      Put
    put amplifiers  200

SuperAdmin can access Delete Amplifer with ID
    [Tags]    AmplifiersController      Delete
    delete amplifier  200

