*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Production role

Library     ../../TestCaseMethods/UsersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  production    201
    ${token_for_version}=  get user token    production
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   production   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Production User
    ${token}=  login to ras    production
    set suite variable  ${token}
    set token of users class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of users class for super admin  ${super_admin_token}

# USERS CONTROLLER
Production cannot access Create User
    [Tags]    UserController        CreateUser
    create user     facility admin  403

Production cannot Change User Role
    [Tags]    UserController        ChangeUserRole
    change user role   review doctor  403

Production can Access Get My Role
    [Tags]    UserController        GetMyRoles
    get my roles  200

Production can Access Get My Theme
    [Tags]    UserController        GetMyTheme
    get my theme  200

Production can Access Set My Theme
    [Tags]    UserController        SetMyTheme
    set my theme  200

Production can Access Set Selected Facility
    [Tags]    UserController        SetSelectedFacility
    set selected facility  200

Production can Access Is User In Role
    [Tags]    UserController        IsUserInRole
    is user in role  200

Production cannot Access Get User Info
    [Tags]    UserController        GetUserInfo
    get user info  403

Production cannot Access Get Users
    [Tags]    UserController        GetUsers
    get users  403

Production cannot Access Get User with ID
    [Tags]    UserController        Get?id=
    get user id  403

Production cannot Access Patch User Data
    [Tags]    UserController        Patch
    patch user  403

Production cannot Access Put User Data
    [Tags]    UserController        Put
    put user  403

Production cannot Access Post User Data
    [Tags]    UserController        Post
    post user  403

Production cannot Access Delete User from Selected Facility
    [Tags]    UserController        DeleteFacilityUser
    delete facility user  403

Production cannot Access Delete User from RAS
    [Tags]    UserController        Delete
    delete user  403

