*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/UsersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${doctor_id}=  Create Test Users  review doctor    201
    set suite variable  ${doctor_id}
    ${token_for_version}=  get user token    super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   super admin   200
    Delete Created Users   review doctor   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=  login to ras    super admin
    set suite variable  ${token}
    set token of users class  ${token}
    set token of users class for super admin  ${token}

# USERS CONTROLLER
SuperAdmin can access Create User
    [Tags]    UserController        CreateUser
    create user     facility admin  201

SuperAdmin can Change User Role
    [Tags]    UserController        ChangeUserRole
    change user role   review doctor  200

SuperAdmin can Access Get My Role
    [Tags]    UserController        GetMyRoles
    get my roles  200

SuperAdmin can Access Get My Theme
    [Tags]    UserController        GetMyTheme
    get my theme  200

SuperAdmin can Access Set My Theme
    [Tags]    UserController        SetMyTheme
    set my theme  200

SuperAdmin can Access Set Selected Facility
    [Tags]    UserController        SetSelectedFacility
    set selected facility  200

SuperAdmin can Access Is User In Role
    [Tags]    UserController        IsUserInRole
    is user in role  200

SuperAdmin can Access Get User Info
    [Tags]    UserController        GetUserInfo
    get user info  200

SuperAdmin can Access Get Users
    [Tags]    UserController        GetUsers
    get users  200

SuperAdmin can Access Get User with ID
    [Tags]    UserController        Get?id=
    get user id  200

SuperAdmin can Access Patch User Data
    [Tags]    UserController        Patch
    patch user  204

SuperAdmin can Access Put User Data
    [Tags]    UserController        Put
    put user  500

SuperAdmin can Access Post User Data
    [Tags]    UserController        Post
    post user  500

SuperAdmin can Access Delete User from Selected Facility
    [Tags]    UserController        DeleteFacilityUser
    delete facility user with id  ${doctor_id}  200

SuperAdmin can Access Delete User from RAS
    [Tags]    UserController        Delete
    delete user  200

