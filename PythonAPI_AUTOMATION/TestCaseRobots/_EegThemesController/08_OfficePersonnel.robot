*** Settings ***
Documentation   Test cases to validate all EegThemes Controller endpoints authorization for OfficePersonnel role

Library     ../../TestCaseMethods/EegThemesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource    ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create test users   office personnel     201
    ${token_for_version}=  get user token   office personnel
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    office personnel     200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as OfficePersonnel
    ${token} =  login to ras    office personnel
    set suite variable  ${token}
    set token of eeg themes class  ${token}
    ${super_admin_token} =  login to ras with email password     ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of eeg themes class for super admin  ${super_admin_token}

# EEGTHEMES CONTROLLER
OfficePersonnel cannot access Post EegThemes
    [Tags]      EegThemesController     Post
    post eeg themes     403

OfficePersonnel cannot access Get EegThemes
    [Tags]      EegThemesController     Get
    get eeg themes      403

OfficePersonnel cannot access Get EegThemes with ID
    [Tags]      EegThemesController     Get?id=
    get eeg themes id   403

OfficePersonnel cannot access Patch EegThemes
    [Tags]      EegThemesController     Patch
    patch eeg themes    403

OfficePersonnel cannot access Put EegThemes
    [Tags]      EegThemesController     Put
    put eeg themes      403

OfficePersonnel cannot access Delete EegThemes
    [Tags]      EegThemesController     Delete
    delete eeg themes   403
