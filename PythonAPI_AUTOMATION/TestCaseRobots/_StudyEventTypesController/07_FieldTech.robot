*** Settings ***
Documentation   Test cases to validate all StudyEventTypes controller endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/StudyEventTypesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource    ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create test users   field tech     201
    ${token_for_version}=  get user token   field tech
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    field tech     200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as FieldTech
    ${token}=   login to ras    field tech
    set suite variable  ${token}
    set token of study event types class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of study event types class for super admin  ${super_admin_token}

# STUDY EVENT TYPES CONTROLLER
FieldTech cannot access Post StudyEventTypes
    [Tags]      StudyEventTypesController   Post
    post study event types      403

FieldTech can access Get StudyEventTypes
    [Tags]      StudyEventTypesController   Get
    get study event types       200

FieldTech cannot access Get StudyEventTypes with ID
    [Tags]      StudyEventTypesController   Get?id=
    get study event types id    403

FieldTech cannot access Patch StudyEventTypes
    [Tags]      StudyEventTypesController   Patch
    patch study event types     403

FieldTech cannot access Put StudyEventTypes
    [Tags]      StudyEventTypesController   Put
    put study event types       403

FieldTech cannot access Delete StudyEventTypes
    [Tags]      StudyEventTypesController   Delete
    delete study event types    403
