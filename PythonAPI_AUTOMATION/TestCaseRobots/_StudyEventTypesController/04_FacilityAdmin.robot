*** Settings ***
Documentation   Test cases to validate all StudyEventTypes controller endpoints authorization for FacilityAdmin role

Library     ../../TestCaseMethods/StudyEventTypesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource    ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create test users   facility admin     201
    ${token_for_version}=  get user token   facility admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    facility admin     200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as FacilityAdmin
    ${token}=   login to ras    facility admin
    set suite variable  ${token}
    set token of study event types class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of study event types class for super admin  ${super_admin_token}

# STUDY EVENT TYPES CONTROLLER
FacilityAdmin cannot access Post StudyEventTypes
    [Tags]      StudyEventTypesController   Post
    post study event types      403

FacilityAdmin can access Get StudyEventTypes
    [Tags]      StudyEventTypesController   Get
    get study event types       200

FacilityAdmin cannot access Get StudyEventTypes with ID
    [Tags]      StudyEventTypesController   Get?id=
    get study event types id    403

FacilityAdmin cannot access Patch StudyEventTypes
    [Tags]      StudyEventTypesController   Patch
    patch study event types     403

FacilityAdmin cannot access Put StudyEventTypes
    [Tags]      StudyEventTypesController   Put
    put study event types       403

FacilityAdmin cannot access Delete StudyEventTypes
    [Tags]      StudyEventTypesController   Delete
    delete study event types    403
