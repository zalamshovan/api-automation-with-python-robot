*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for LeadTech role

Library     ../../TestCaseMethods/DevicesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   lead tech    201
    ${token_for_version}=  get user token   lead tech
    set suite variable  ${token_for_version}
#    ${token_for_fac} =  login to ras    super admin
#    set suite variable  ${token_for_fac}
#    set token of facilities class  ${token_for_fac}
#    ${created_fac_id} =  create facility    200
#    set suite variable  ${created_fac_id}


Multiple Teardown Methods
#    Delete facility id      ${created_fac_id}   200
    Delete created users    lead tech    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as LeadTech
    ${token}=   login to ras    lead tech
    set suite variable  ${token}
    set token of devices class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of devices class for super admin  ${super_admin_token}


# DEVICES CONTROLLER
LeadTech cannot access Create Device
    [Tags]     DevicesController        CreateDevice
    create device  403

LeadTech cannot access Get By SN
    [Tags]      DevicesController       GetBySn
    get by sn  403

LeadTech cannot access Get Devices
    [Tags]      DevicesController       GetDevices
    get devices  403

LeadTech cannot access Get Devices with ID
    [Tags]      DevicesController       Get?id=
    get device id  403

LeadTech cannot access Post Device with ID
    [Tags]      DevicesController       Post
    post devices  403

LeadTech cannot access Patch Device with ID
    [Tags]      DevicesController       Patch
    patch devices  403

LeadTech cannot access Put Device with ID
    [Tags]      DevicesController       Put
    put devices  403

LeadTech cannot access ChangeDeviceFacility
    [Tags]      DevicesController       ChangeDeviceFacility
    change device facility  ${created_fac_id}   403

LeadTech cannot access Delete Device
    [Tags]     DevicesController        Delete
    delete devices  403