*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Production role

Library     ../../TestCaseMethods/FacilitiesTCs.py
Library     ../../TestCaseMethods/UsersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
#    Create test users   super admin     201
#    ${created_super_admin_token}=    login to ras   super admin
#    set suite variable  ${created_super_admin_token}
    Create Test Users  production    201
    ${token_for_version}=  login to ras    production
    set suite variable  ${token_for_version}
#    set token of facilities class  ${created_super_admin_token}
###    ${facility_id}=     create facility named enosis    200
###    set suite variable  ${facility_id}
#    set token of users class  ${created_super_admin_token}
#    change selected facility to enosis      ${created_fac_id}
#    ${production_email}=    get user email  production
#    set suite variable  ${production_email}
#    send invitation using email role    ${production_email}     ${field_tech_role}     200
#    change selected facility    mobilemedtek

Multiple Teardown Methods
#    Delete created users    super admin     200
    Delete Created Users   production   200

Delete Invited User After Accepting Invitation
    ${token_for_invited_user}=  login to ras with email password  ${invited_email}  Enosis123
    ${invited_user_id}=  get logged in user id  ${token_for_invited_user}
    delete user with id  ${invited_user_id}  200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Production User
    ${token}=  login to ras    production
    set suite variable  ${token}
    set token of facilities class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of facilities class for super admin  ${super_admin_token}

# FACILITIES CONTROLLER

Only for Now
    set_test_values_for_facilities_class

#Production cannot access Create Facility
#    [Tags]    FacilitiesController     Post
#    create facility     403
#
Production cannot access RotateToken
    [Tags]    FacilitiesController      RotateToken
    rotate token  403

Production can access GetMetrics
    [Tags]    FacilitiesController      GetMetrics
    get facility metrics  200

Production can access GetFacilities
    [Tags]    FacilitiesController      GetFacilities
    get facilities  200

Production cannot access Get Facility by ID
    [Tags]    FacilitiesController      Get?id=
    get facility  403

Production cannot access Patch Facility
    [Tags]    FacilitiesController      Patch
    patch facility  403

Production cannot access Put Facility
    [Tags]    FacilitiesController      Put
    put facility    403

#Production cannot access SendInvitation
#    [Tags]    FacilitiesController      SendInvitation
#    send invitation  403

#Production cannot access GetInvitations
    [Tags]    FacilitiesController      GetInvitations
#    ${invitation_id}=   get invitations  None   ${production_email} 403
#    set suite variable  ${invitation_id}

#Production can access GetInvitation
#    [Tags]    FacilitiesController      GetInvitation
#    get invitation id   ${invitation_id}    200

Production can access DoesInvitedUserExist
    [Tags]    FacilitiesController      DoesInvitedUserExist
    does invited user exist  200

Production can access IsInvitationExpired
    [Tags]    FacilitiesController      IsInvitationExpired
    is invitation expired  200

Production can access AcceptInvitation
    [Tags]    FacilitiesController      AcceptInvitation
    ${status} =   accept invitation  200
    Run Keyword If  '${status}' == 'success'  Delete Invited User After Accepting Invitation

Production cannot access Delete Facility
    [Tags]    FacilitiesController      Delete
    delete facility  403
