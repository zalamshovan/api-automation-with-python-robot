*** Settings ***
Documentation     Test Cases to validate all Amplifiers Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/FacilitiesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${token_for_version}=  get user token    super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   super admin   200

Delete Invited User After Accepting Invitation
    ${token_for_invited_user}=  login to ras with email password  ${invited_email}  Enosis123
    ${invited_user_id}=  get logged in user id  ${token_for_invited_user}
    delete user with id  ${invited_user_id}  200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=  login to ras    super admin
    set token of facilities class   ${token}
    set token of facilities class for super admin  ${token}

# FACILITIES CONTROLLER

Only for Now
    set_test_values_for_facilities_class

#SuperAdmin can access Create Facility
#    [Tags]    FacilitiesController     Post
#    create facility     200
#
SuperAdmin can access RotateToken
    [Tags]    FacilitiesController      RotateToken
    rotate token  200

SuperAdmin can access GetMetrics
    [Tags]    FacilitiesController      GetMetrics
    get facility metrics  200

SuperAdmin can access GetFacilities
    [Tags]    FacilitiesController      GetFacilities
    get facilities  200

SuperAdmin can access Get Facility by ID
    [Tags]    FacilitiesController      Get?id=
    get facility  200

SuperAdmin can access Patch Facility
    [Tags]    FacilitiesController      Patch
    patch facility  204

SuperAdmin can access Put Facility
    [Tags]    FacilitiesController      Put
    put facility    204

SuperAdmin can access SendInvitation
    [Tags]    FacilitiesController      SendInvitation
    send invitation  200

SuperAdmin can access GetInvitations
    [Tags]    FacilitiesController      GetInvitations
    get invitations     None    None    200

SuperAdmin can access GetInvitation
    [Tags]    FacilitiesController      GetInvitation
    get invitation  200

SuperAdmin can access DoesInvitedUserExist
    [Tags]    FacilitiesController      DoesInvitedUserExist
    does invited user exist  200

SuperAdmin can access IsInvitationExpired
    [Tags]    FacilitiesController      IsInvitationExpired
    is invitation expired  200

SuperAdmin can access AcceptInvitation
    [Tags]    FacilitiesController      AcceptInvitation
    ${status} =   accept invitation  200
    Run Keyword If  '${status}' == 'success'  Delete Invited User After Accepting Invitation

SuperAdmin can access Delete Facility
    [Tags]    FacilitiesController      Delete
    delete facility  200
