*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/FacilitiesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  lead tech    201
    ${token_for_version}=  get user token    lead tech
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   lead tech   200

Delete Invited User After Accepting Invitation
    ${token_for_invited_user}=  login to ras with email password  ${invited_email}  Enosis123
    ${invited_user_id}=  get logged in user id  ${token_for_invited_user}
    delete user with id  ${invited_user_id}  200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods


*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Lead Tech User
    ${token}=  login to ras    lead tech
    set suite variable  ${token}
    set token of facilities class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of facilities class for super admin  ${super_admin_token}

# FACILITIES CONTROLLER

Only for Now
    set_test_values_for_facilities_class

#LeadTech cannot access Create Facility
#    [Tags]    FacilitiesController     Post
#    create facility     403
#
LeadTech cannot access RotateToken
    [Tags]    FacilitiesController      RotateToken
    rotate token  403

LeadTech can access GetMetrics
    [Tags]    FacilitiesController      GetMetrics
    get facility metrics  200

LeadTech can access GetFacilities
    [Tags]    FacilitiesController      GetFacilities
    get facilities  200

LeadTech cannot access Get Facility by ID
    [Tags]    FacilitiesController      Get?id=
    get facility  403

LeadTech cannot access Patch Facility
    [Tags]    FacilitiesController      Patch
    patch facility  403

LeadTech cannot access Put Facility
    [Tags]    FacilitiesController      Put
    put facility    403

LeadTech cannot access SendInvitation
    [Tags]    FacilitiesController      SendInvitation
    send invitation  403

LeadTech cannot access GetInvitations
    [Tags]    FacilitiesController      GetInvitations
    get invitations  403

LeadTech can access GetInvitation
    [Tags]    FacilitiesController      GetInvitation
    get invitation  200

LeadTech can access DoesInvitedUserExist
    [Tags]    FacilitiesController      DoesInvitedUserExist
    does invited user exist  200

LeadTech can access IsInvitationExpired
    [Tags]    FacilitiesController      IsInvitationExpired
    is invitation expired  200

LeadTech can access AcceptInvitation
    [Tags]    FacilitiesController      AcceptInvitation
    ${status} =   accept invitation  200
    Run Keyword If  '${status}' == 'success'  Delete Invited User After Accepting Invitation

LeadTech cannot access Delete Facility
    [Tags]    FacilitiesController      Delete
    delete facility  403
