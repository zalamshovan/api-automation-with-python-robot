*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Office Personnel role

Library     ../../TestCaseMethods/FacilitiesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  office personnel    201
    ${token_for_version}=  get user token    office personnel
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   office personnel   200

Delete Invited User After Accepting Invitation
    ${token_for_invited_user}=  login to ras with email password  ${invited_email}  Enosis123
    ${invited_user_id}=  get logged in user id  ${token_for_invited_user}
    delete user with id  ${invited_user_id}  200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Office Personnel User
    ${token}=  login to ras    office personnel
    set suite variable  ${token}
    set token of facilities class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of facilities class for super admin  ${super_admin_token}

# FACILITIES CONTROLLER

Only for Now
    set_test_values_for_facilities_class

#OfficePersonnel cannot access Create Facility
#    [Tags]    FacilitiesController     Post
#    create facility     403
#
OfficePersonnel cannot access RotateToken
    [Tags]    FacilitiesController      RotateToken
    rotate token  403

OfficePersonnel can access GetMetrics
    [Tags]    FacilitiesController      GetMetrics
    get facility metrics  200

OfficePersonnel can access GetFacilities
    [Tags]    FacilitiesController      GetFacilities
    get facilities  200

OfficePersonnel cannot access Get Facility by ID
    [Tags]    FacilitiesController      Get?id=
    get facility  403

OfficePersonnel cannot access Patch Facility
    [Tags]    FacilitiesController      Patch
    patch facility  403

OfficePersonnel cannot access Put Facility
    [Tags]    FacilitiesController      Put
    put facility    403

OfficePersonnel cannot access SendInvitation
    [Tags]    FacilitiesController      SendInvitation
    send invitation  403

OfficePersonnel cannot access GetInvitations
    [Tags]    FacilitiesController      GetInvitations
    get invitations  403

OfficePersonnel can access GetInvitation
    [Tags]    FacilitiesController      GetInvitation
    get invitation  200

OfficePersonnel can access DoesInvitedUserExist
    [Tags]    FacilitiesController      DoesInvitedUserExist
    does invited user exist  200

OfficePersonnel can access IsInvitationExpired
    [Tags]    FacilitiesController      IsInvitationExpired
    is invitation expired  200

OfficePersonnel can access AcceptInvitation
    [Tags]    FacilitiesController      AcceptInvitation
    ${status} =   accept invitation  200
    Run Keyword If  '${status}' == 'success'  Delete Invited User After Accepting Invitation

OfficePersonnel cannot access Delete Facility
    [Tags]    FacilitiesController      Delete
    delete facility  403
