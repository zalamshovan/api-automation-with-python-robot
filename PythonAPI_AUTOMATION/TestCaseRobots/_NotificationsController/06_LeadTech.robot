*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for LeadTech role

Library     ../../TestCaseMethods/NotificationsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   lead tech    201
    Create Test Users   review doctor    201
    ${token_for_version}=  get user token    review doctor
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    lead tech    200
    Delete created users    review doctor    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as SuperAdmin user to share a study with the Lead Tech
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of notifications class  ${super_admin_token}
    set token of notifications class for super admin  ${super_admin_token}
    set super admin token for study events and studies  ${super_admin_token}
    ${study_id}=    get study id
    set suite variable  ${study_id}
    ${event_id}=    get event id
    set suite variable  ${event_id}
    ${lead_tech_token}=   login to ras    lead tech
    set suite variable  ${lead_tech_token}
    ${lead_tech_id}=  get logged in user id  ${lead_tech_token}
    set suite variable  ${lead_tech_id}
    share study  ${study_id}  ${lead_tech_id}  200

Initializing other user so that Lead Tech can share with him
    set token of notifications class  ${lead_tech_token}
    ${receiver_token}=  login to ras  review doctor
    set suite variable  ${receiver_token}
    ${receiver_email}=  get user email  review doctor
    set suite variable  ${receiver_email}
    ${receiver_user_id}=  get logged in user id  ${receiver_token}
    set suite variable  ${receiver_user_id}

# NOTIFICATIONS CONTROLLER
LeadTech can access Post ShareStudy
    [Tags]      NotificationsController   ShareStudy
    share study  ${study_id}  ${receiver_user_id}  200

LeadTech can access Post ShareStudyEvent
    [Tags]      NotificationsController   ShareStudyEvent
    share study event  ${study_id}  ${event_id}  ${receiver_user_id}  200

LeadTech can access Post NotifyStudySubmitted
    [Tags]      NotificationsController   NotifyStudySubmitted
    notify study submitted  ${study_id}  ${receiver_user_id}  ${receiver_email}  200

LeadTech can access Get GetShareableUsersAndGroups
    [Tags]      NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups  200

LeadTech can access Get GetSubmittableUsersAndGroups
    [Tags]      NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  200
