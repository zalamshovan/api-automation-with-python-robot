*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/NotificationsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   field tech    201
    Create Test Users   review doctor    201
    ${token_for_version}=  get user token    review doctor
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    field tech    200
    Delete created users    review doctor    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as SuperAdmin user to share a study with the Field Tech
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of notifications class  ${super_admin_token}
    set token of notifications class for super admin  ${super_admin_token}
    set super admin token for study events and studies  ${super_admin_token}
    ${study_id}=    get study id
    set suite variable  ${study_id}
    ${event_id}=    get event id
    set suite variable  ${event_id}
    ${field_tech_token}=   login to ras    field tech
    set suite variable  ${field_tech_token}
    ${field_tech_id}=  get logged in user id  ${field_tech_token}
    set suite variable  ${field_tech_id}
    share study  ${study_id}  ${field_tech_id}  200

Initializing other user so that Field Tech can share with him
    set token of notifications class  ${field_tech_token}
    ${receiver_token}=  login to ras  review doctor
    set suite variable  ${receiver_token}
    ${receiver_email}=  get user email  review doctor
    set suite variable  ${receiver_email}
    ${receiver_user_id}=  get logged in user id  ${receiver_token}
    set suite variable  ${receiver_user_id}

# NOTIFICATIONS CONTROLLER
FieldTech can access Post ShareStudy
    [Tags]      NotificationsController   ShareStudy
    share study  ${study_id}  ${receiver_user_id}  200

FieldTech can access Post ShareStudyEvent
    [Tags]      NotificationsController   ShareStudyEvent
    share study event  ${study_id}  ${event_id}  ${receiver_user_id}  200

FieldTech can access Post NotifyStudySubmitted
    [Tags]      NotificationsController   NotifyStudySubmitted
    notify study submitted  ${study_id}  ${receiver_user_id}  ${receiver_email}  200

FieldTech can access Get GetShareableUsersAndGroups
    [Tags]      NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups  200

FieldTech can access Get GetSubmittableUsersAndGroups
    [Tags]      NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  200
