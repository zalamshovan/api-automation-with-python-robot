*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for ReviewDoctor role

Library     ../../TestCaseMethods/NotificationsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   review doctor    201
    Create Test Users   lead tech    201
    ${token_for_version}=  get user token    review doctor
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    review doctor    200
    Delete created users    lead tech    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as SuperAdmin user to share a study with the Review Doctor
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of notifications class  ${super_admin_token}
    set token of notifications class for super admin  ${super_admin_token}
    set super admin token for study events and studies  ${super_admin_token}
    ${study_id}=    get study id
    set suite variable  ${study_id}
    ${event_id}=    get event id
    set suite variable  ${event_id}
    ${review_doctor_token}=   login to ras    review doctor
    set suite variable  ${review_doctor_token}
    ${review_doctor_id}=  get logged in user id  ${review_doctor_token}
    set suite variable  ${review_doctor_id}
    share study  ${study_id}  ${review_doctor_id}  200

Initializing other user so that Review Doctor can share with him
    set token of notifications class  ${review_doctor_token}
    ${receiver_token}=  login to ras  lead tech
    set suite variable  ${receiver_token}
    ${receiver_email}=  get user email  lead tech
    set suite variable  ${receiver_email}
    ${receiver_user_id}=  get logged in user id  ${receiver_token}
    set suite variable  ${receiver_user_id}


# NOTIFICATIONS CONTROLLER
ReviewDoctor can access Post ShareStudy
    [Tags]      NotificationsController   ShareStudy
    share study  ${study_id}  ${receiver_user_id}  200

ReviewDoctor can access Post ShareStudyEvent
    [Tags]      NotificationsController   ShareStudyEvent
    share study event  ${study_id}  ${event_id}  ${receiver_user_id}  200

ReviewDoctor can access Post NotifyStudySubmitted
    [Tags]      NotificationsController   NotifyStudySubmitted
    notify study submitted  ${study_id}  ${receiver_user_id}  ${receiver_email}  200

ReviewDoctor can access Get GetShareableUsersAndGroups
    [Tags]      NotificationsController   GetShareableUsersAndGroups
    get shareable users and groups  200

ReviewDoctor can access Get GetSubmittableUsersAndGroups
    [Tags]      NotificationsController   GetSubmittableUsersAndGroups
    get submittable users and groups  200
