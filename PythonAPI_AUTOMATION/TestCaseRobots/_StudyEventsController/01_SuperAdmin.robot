*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/StudyEventsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${token_for_version}=  get user token    super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   super admin   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=  login to ras    super admin
    set suite variable  ${token}
    set token of study events class  ${token}
    set token of study events class for super admin  ${token}

# STUDY EVENTS CONTROLLER
SuperAdmin can access Add Study Events
    [Tags]    StudyEventsController        AddStudyEvent
    add study events  201

SuperAdmin can access Get Study Events by Study ID
    [Tags]    StudyEventsController        GetStudyEventsByStudyID
    get study events by study id  200

SuperAdmin can access Get Study Events by Event ID
    [Tags]    StudyEventsController        Get?id=
    get study events by event id  200

SuperAdmin can access Get Study Events
    [Tags]    StudyEventsController        Get
    get study events  200

SuperAdmin can access Patch Study Event
    [Tags]    StudyEventsController        Patch
    patch study event  204

SuperAdmin can access Put Study Event
    [Tags]    StudyEventsController        Put
    put study event  200

SuperAdmin can access Post Study Event
    [Tags]    StudyEventsController        Post
    post study event  201

SuperAdmin can access Delete Study Event by ID
    [Tags]    StudyEventsController        Delete
    delete study event  200
