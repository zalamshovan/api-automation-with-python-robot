*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/StudyEventsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  facility admin    201
    ${token_for_version}=  get user token    facility admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   facility admin   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Facility Admin User
    ${token}=  login to ras    facility admin
    set suite variable  ${token}
    set token of study events class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of study events class for super admin  ${super_admin_token}

# STUDY EVENTS CONTROLLER
FacilityAdmin can access Add Study Events
    [Tags]    StudyEventsController        AddStudyEvent
    add study events  201

FacilityAdmin can access Get Study Events by Study ID
    [Tags]    StudyEventsController        GetStudyEventsByStudyID
    get study events by study id  200

FacilityAdmin cannot access Get Study Events by Event ID
    [Tags]    StudyEventsController        Get?id=
    get study events by event id  403

FacilityAdmin cannot access Get Study Events
    [Tags]    StudyEventsController        Get
    get study events  403

FacilityAdmin cannot access Patch Study Event
    [Tags]    StudyEventsController        Patch
    patch study event  403

FacilityAdmin can access Put Study Event
    [Tags]    StudyEventsController        Put
    put study event  200

FacilityAdmin cannot access Post Study Event
    [Tags]    StudyEventsController        Post
    post study event  403

FacilityAdmin can access Delete Study Event by ID
    [Tags]    StudyEventsController        Delete
    delete study event  200
