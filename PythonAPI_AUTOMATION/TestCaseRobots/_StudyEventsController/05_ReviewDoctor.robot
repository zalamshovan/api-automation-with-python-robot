*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/StudyEventsTCs.py
Library     ../../TestCaseMethods/Common.py
Library     ../../TestCaseMethods/NotificationsTCs.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  review doctor    201
    ${token_for_version}=  get user token    review doctor
    set suite variable  ${token_for_version}
    Create Test Users  super admin    201

Multiple Teardown Methods
    Delete Created Users   review doctor   200
    Delete Created Users   super admin   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin to get a Study ID
    ${super_admin_token}=  login to ras  super admin
    set suite variable  ${super_admin_token}
    set token of notifications class for super admin  ${super_admin_token}
    ${study_id}=  get study id
    set suite variable  ${study_id}

Login to RAS as Review Doctor User
    ${token}=  login to ras    review doctor
    set suite variable  ${token}
    set token of study events class  ${token}
    set token of study events class for super admin  ${super_admin_token}
    ${user_id}=  get logged in user id   ${token}
    set suite variable  ${user_id}

Sharing the found study with Review Doctor
    set token of notifications class  ${super_admin_token}
    share study  ${study_id}  ${user_id}  200

# STUDY EVENTS CONTROLLER
ReviewDoctor can access Add Study Events
    [Tags]    StudyEventsController        AddStudyEvent
    add study events  201

ReviewDoctor can access Get Study Events by Study ID
    [Tags]    StudyEventsController        GetStudyEventsByStudyID
    get study events by study id  200

ReviewDoctor cannot access Get Study Events by Event ID
    [Tags]    StudyEventsController        Get?id=
    get study events by event id  403

ReviewDoctor cannot access Get Study Events
    [Tags]    StudyEventsController        Get
    get study events  403

ReviewDoctor cannot access Patch Study Event
    [Tags]    StudyEventsController        Patch
    patch study event  403

ReviewDoctor can access Put Study Event
    [Tags]    StudyEventsController        Put
    put study event  200

ReviewDoctor cannot access Post Study Event
    [Tags]    StudyEventsController        Post
    post study event  403

ReviewDoctor can access Delete Study Event by ID
    [Tags]    StudyEventsController        Delete
    delete study event  200
