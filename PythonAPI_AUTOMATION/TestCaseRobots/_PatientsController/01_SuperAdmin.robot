*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/PatientsTCs.py
Library     ../../TestCaseMethods/Common.py

*** Keywords ***
Multiple Setup Methods
    Create Test Users   super admin    201
    ${token_for_version}=   get user token  super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    super admin    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=   login to ras    super admin
    set suite variable  ${token}
    set token of patients class  ${token}
    set token of patients class for super admin  ${token}


# PATIENTS CONTROLLER
SuperAdmin can access Create Patient
    [Tags]     PatientController    Post
    create patient  200

SuperAdmin can access Search Patient
    [Tags]     PatientController    Search
    search patient  200

SuperAdmin can access Get Patient for Editing
    [Tags]     PatientController    GetPatientForEditing
    get patient for editing  200

SuperAdmin can access Get Patient by ID
    [Tags]     PatientController    Get?id=
    get patient  200

SuperAdmin can access Get Patients
    [Tags]     PatientController    Get
    get patients  200

SuperAdmin can access Put Patient
    [Tags]     PatientController    Put
    put patients  200

SuperAdmin can access Patch Patient
    [Tags]     PatientController    Patch
    patch patients  204

SuperAdmin can access Delete Patient
    [Tags]     PatientController    Delete
    delete patient  200
