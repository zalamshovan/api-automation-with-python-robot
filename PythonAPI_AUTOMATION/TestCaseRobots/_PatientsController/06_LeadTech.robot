*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Lead Tech role

Library     ../../TestCaseMethods/PatientsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  lead tech    201
    ${token_for_version}=  get user token    lead tech
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   lead tech   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Lead Tech User
    ${token}=  login to ras    lead tech
    set suite variable  ${token}
    set token of patients class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of patients class for super admin  ${super_admin_token}

# PATIENTS CONTROLLER
LeadTech can access Create Patient
    [Tags]     PatientController    Post
    create patient  200

LeadTech can access Search Patient
    [Tags]     PatientController    Search
    search patient  200

LeadTech can access Get Patient for Editing
    [Tags]     PatientController    GetPatientForEditing
    get patient for editing  200

LeadTech can access Get Patient by ID
    [Tags]     PatientController    Get?id=
    get patient  200

LeadTech can access Get Patients
    [Tags]     PatientController    Get
    get patients  200

LeadTech can access Put Patient
    [Tags]     PatientController    Put
    put patients  200

LeadTech cannot access Patch Patient
    [Tags]     PatientController    Patch
    patch patients  403

LeadTech cannot access Delete Patient
    [Tags]     PatientController    Delete
    delete patient  403
