*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Field Tech role

Library     ../../TestCaseMethods/PatientsTCs.py
Library     ../../TestCaseMethods/Common.py
Library     ../../TestCaseMethods/NotificationsTCs.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  field tech    201
    ${token_for_version}=  get user token    field tech
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete Created Users   field tech   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Field Tech User
    ${token}=  login to ras    field tech
    set suite variable  ${token}
    ${user_id}=  get logged in user id   ${token}
    set suite variable  ${user_id}
    set token of patients class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of patients class for super admin  ${super_admin_token}
    set token of notifications class for super admin  ${super_admin_token}
    set token of notifications class  ${super_admin_token}
    ${study_id}=  get patient from studies list
    set suite variable  ${study_id}
    share study  ${study_id}  ${user_id}  200

# PATIENTS CONTROLLER
FieldTech can access Create Patient
    [Tags]     PatientController    Post
    create patient  200

FieldTech can access Search Patient
    [Tags]     PatientController    Search
    ${pat_id}=  search patient by name  ${None}  200
    set suite variable  ${pat_id}

FieldTech can access Get Patient for Editing
    [Tags]     PatientController    GetPatientForEditing
    get patient for editing  200

FieldTech can access Get Patient by ID
    [Tags]     PatientController    Get?id=
    get patient by id   ${pat_id}   200

FieldTech can access Get Patients
    [Tags]     PatientController    Get
    get patients  200

FieldTech can access Put Patient
    [Tags]     PatientController    Put
    put patients  200

FieldTech cannot access Patch Patient
    [Tags]     PatientController    Patch
    patch patients  403

FieldTech cannot access Delete Patient
    [Tags]     PatientController    Delete
    delete patient  403
