*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for ReviewDoctor role

Library     ../../TestCaseMethods/StudyRecordingsTCs.py
Library     ../../TestCaseMethods/NotificationsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   review doctor    201
    ${token_for_version}=  get user token   review doctor
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    review doctor    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as ReviewDoctor user
    ${token}=   login to ras    review doctor
    set suite variable  ${token}
    set token of study recordings class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of studies class  ${super_admin_token}
    ${study_id}=    get studies id      200
    set suite variable  ${study_id}
    ${user_id}=     get logged in user id   ${token}
    set suite variable  ${user_id}
    set token of notifications class  ${super_admin_token}
    share study  ${study_id}    ${user_id}  200


# STUDY RECORDINGS CONTROLLER
ReviewDoctor can access Get CalculateServerChecksum
    [Tags]      StudyRecordingsController   CalculateServerChecksum
    calculate server checksum       200

ReviewDoctor can access Get CalculateServerVideoChecksum
    [Tags]      StudyRecordingsController   CalculateServerVideoChecksum
    calculate server video checksum     200

ReviewDoctor can access Get GetLastSyncPacketIndex
    [Tags]      StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index     200

ReviewDoctor can access Get GetServerVideoSize
    [Tags]      StudyRecordingsController   GetServerVideoSize
    get server video size   200

ReviewDoctor can access Post UpdateStudyRecordingNotes
    [Tags]      StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    200

ReviewDoctor cannot access Get GetStudyRecording
    [Tags]      StudyRecordingsController   GetStudyRecording
    get study recording     403