*** Settings ***
Documentation    Test Cases to validate all StudyRecordings Controller endpoints authorization for SuperAdmin role

Library     ../../TestCaseMethods/StudyRecordingsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users   super admin    201
    ${token_for_version}=  get user token   super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    super admin    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as SuperAdmin user
    ${token}=   login to ras    super admin
    set suite variable  ${token}
    set token of study recordings class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of studies class  ${super_admin_token}
    get studies id      200

# STUDY RECORDINGS CONTROLLER
SuperAdmin can access Get CalculateServerChecksum
    [Tags]      StudyRecordingsController   CalculateServerChecksum
    calculate server checksum       200

SuperAdmin can access Get CalculateServerVideoChecksum
    [Tags]      StudyRecordingsController   CalculateServerVideoChecksum
    calculate server video checksum     200

SuperAdmin can access Get GetLastSyncPacketIndex
    [Tags]      StudyRecordingsController   GetLastSyncPacketIndex
    get last sync packet index     200

SuperAdmin can access Get GetServerVideoSize
    [Tags]      StudyRecordingsController   GetServerVideoSize
    get server video size   200

SuperAdmin can access Post UpdateStudyRecordingNotes
    [Tags]      StudyRecordingsController   UpdateStudyRecordingNotes
    update study recording notes    200

SuperAdmin can access Get GetStudyRecording
    [Tags]      StudyRecordingsController   GetStudyRecording
    get study recording     200