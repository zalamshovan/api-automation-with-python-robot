*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Facility Admin role

Library     ../../TestCaseMethods/FacilityUsersTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  facility admin    201
    ${user_id}=  create test users at enosis facility  office personnel  201
    set suite variable  ${user_id}
    ${email_to_create_fac_user}=  get user email  office personnel
    set suite variable  ${email_to_create_fac_user}
    ${token_for_version}=  get user token    facility admin
    set suite variable  ${token_for_version}
    change selected facility    mobilemedtek

Multiple Teardown Methods
    Delete Created Users   facility admin   200
    Delete Created Users   office personnel   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Facility Admin User
    ${token}=  login to ras    facility admin
    set suite variable  ${token}
    set token of facility users class  ${token}
    ${super_admin_token}=  login to ras with email password  ${email}    ${password}
    set suite variable  ${super_admin_token}
    set token of facility users class for super admin  ${super_admin_token}

# FACILITY USERS CONTROLLER
Facility Admin User can access Create Facility User
    [Tags]     FacilityUsersController  CreateFacilityUser
    create facility user with mail  ${email_to_create_fac_user}  201

Facility Admin User can access Get User
    [Tags]     FacilityUsersController  GetUser
    get user  200

Facility Admin User can access Get Users
    [Tags]     FacilityUsersController  GetUsers
    get users  200

Facility Admin User cannot access Post Facility Users
    [Tags]     FacilityUsersController  Post
    post facility user  ${user_id}  403

Facility Admin User cannot access Get Facility User
    [Tags]     FacilityUsersController  Get?id=
    get facility user  403

Facility Admin User cannot access Get Facility Users
    [Tags]     FacilityUsersController  Get
    get facility users  403

Facility Admin User cannot access Put Facility Users
    [Tags]     FacilityUsersController  Put
    put facility user   ${user_id}  403

Facility Admin User cannot access Patch Facility Users
    [Tags]     FacilityUsersController  Patch
    patch facility user   ${user_id}  403

Facility Admin User can access Delete Facility User
    [Tags]     FacilityUsersController  Delete
    delete facility user  500

