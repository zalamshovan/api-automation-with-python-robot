*** Settings ***
Documentation    Test Cases to validate all Devices Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/FacilityUsersTCs.py
Library     ../../TestCaseMethods/Common.py

*** Keywords ***
Multiple Setup Methods
    Create Test Users   super admin    201
    ${user_id}=  create test users at enosis facility  office personnel  201
    set suite variable  ${user_id}
    ${email_to_create_fac_user}=  get user email  office personnel
    set suite variable  ${email_to_create_fac_user}
    ${token_for_version}=   get user token  super admin
    set suite variable  ${token_for_version}

Multiple Teardown Methods
    Delete created users    super admin    200
    Delete created users    office personnel    200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Super Admin
    ${token}=   login to ras    super admin
    set suite variable  ${token}
    set token of facility users class  ${token}
    set token of facility users class for super admin  ${token}


# FACILITY USERS CONTROLLER
Super Admin can access Create Facility User
    [Tags]     FacilityUsersController    CreateFacilityUser
    create facility user with mail  ${email_to_create_fac_user}  201

Super Admin can access Get User
    [Tags]     FacilityUsersController  GetUser
    get user  200

Super Admin can access Get Users
    [Tags]     FacilityUsersController  GetUser
    get users  200

Super Admin can access Post Facility Users
    [Tags]     FacilityUsersController  Post
    post facility user  ${user_id}  500

Super Admin can access Get Facility User
    [Tags]     FacilityUsersController  Get?id=
    get facility user  200

Super Admin can access Get Facility Users
    [Tags]     FacilityUsersController  Get
    get facility users  200

Super Admin can access Put Facility Users
    [Tags]     FacilityUsersController  Put
    put facility user   ${user_id}  204

Super Admin can access Patch Facility Users
    [Tags]     FacilityUsersController  Patch
    patch facility user   ${user_id}  404

Super Admin can access Delete Facility User
    [Tags]     FacilityUsersController  Delete
    delete facility user  500
