*** Settings ***
Documentation   Test cases to validate all ControlSoftwareLogs controller authorization for FieldTech role

Library     ../../TestCaseMethods/ControlSoftwareLogsTCs.py
Library     ../../TestCaseMethods/DevicesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource    ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create test users   field tech     201
    ${token_for_version}=  get user token   field tech
    set suite variable  ${token_for_version}
    ${token_for_device} =   login to ras with email password  ${email}  ${password}
    set suite variable  ${token_for_device}
    set token of devices class  ${token_for_device}
    ${created_device_id} =  create device   201
    set suite variable  ${created_device_id}

Multiple Teardown Methods
    Delete devices      200
    Delete created users    field tech     200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as FieldTech
    ${token}=   login to ras    field tech
    set suite variable  ${token}
    set token of control software logs class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of control software logs class for super admin  ${super_admin_token}

# CONTROL SOFTWARE LOGS CONTROLLER
FieldTech cannot access Post ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Post
    post control software logs      ${created_device_id}    403

FieldTech cannot access Get ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Get
    get control software logs   403

FieldTech cannot access Get ControlSoftwareLogs with ID
    [Tags]      ControlSoftwareLogsController   Get?id=
    get control software logs id    403

FieldTech cannot access Put ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Put
    put control software logs      ${created_device_id}    403

FieldTech cannot access Patch ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Patch
    patch control software logs      ${created_device_id}    403

FieldTech cannot access Get Device Logs
    [Tags]      ControlSoftwareLogsController   GetDeviceLogs
    get device logs     ${created_device_id}    403

FieldTech cannot access Get Facility Logs
    [Tags]      ControlSoftwareLogsController   GetFacilityLogs
    get facility logs   403

FieldTech can access Post Synchronize Log Entry
    [Tags]      ControlSoftwareLogsController   SynchronizeLogEntry
    post synchronize log entry  ${created_device_id}    200

FieldTech cannot access Delete ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Delete
    delete control software log     403
