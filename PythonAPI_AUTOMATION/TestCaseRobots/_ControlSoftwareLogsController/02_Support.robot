*** Settings ***
Documentation   Test cases to validate all ControlSoftwareLogs controller authorization for Support role

Library     ../../TestCaseMethods/ControlSoftwareLogsTCs.py
Library     ../../TestCaseMethods/DevicesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource    ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create test users   support     201
    ${token_for_version}=   get user token      support
    set suite variable  ${token_for_version}
    ${token_for_device} =   login to ras with email password  ${email}  ${password}
    set suite variable  ${token_for_device}
    set token of devices class  ${token_for_device}
    ${created_device_id} =  create device   201
    set suite variable  ${created_device_id}

Multiple Teardown Methods
    Delete devices      200
    Delete created users    support     200

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as Support
    ${token}=   login to ras    support
    set suite variable  ${token}
    set token of control software logs class  ${token}
    ${super_admin_token}=   login to ras with email password  ${email}  ${password}
    set suite variable  ${super_admin_token}
    set token of control software logs class for super admin  ${super_admin_token}

# CONTROL SOFTWARE LOGS CONTROLLER
Support cannot access Post ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Post
    post control software logs      ${created_device_id}    403

Support cannot access Get ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Get
    get control software logs   403

Support cannot access Get ControlSoftwareLogs with ID
    [Tags]      ControlSoftwareLogsController   Get?id=
    get control software logs id    403

Support can access Put ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Put
    put control software logs      ${created_device_id}    204

Support can access Patch ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Patch
    patch control software logs      ${created_device_id}    204

Support can access Get Device Logs
    [Tags]      ControlSoftwareLogsController   GetDeviceLogs
    get device logs     ${created_device_id}    200

Support can access Get Facility Logs
    [Tags]      ControlSoftwareLogsController   GetFacilityLogs
    get facility logs   200

Support can access Post Synchronize Log Entry
    [Tags]      ControlSoftwareLogsController   SynchronizeLogEntry
    post synchronize log entry  ${created_device_id}    200

Support can access Delete ControlSoftwareLogs
    [Tags]      ControlSoftwareLogsController   Delete
    delete control software log     200
