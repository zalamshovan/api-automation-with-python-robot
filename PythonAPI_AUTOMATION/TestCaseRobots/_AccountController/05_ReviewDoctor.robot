*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for Review Doctor role

Library     ../../TestCaseMethods/AccountTCs.py
Library     ../../TestCaseMethods/DevicesTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${super_admin_token}=  get user token    super admin
    set suite variable  ${super_admin_token}
    set token of devices class  ${super_admin_token}
    ${device_id}=  create device    201
    set suite variable  ${device_id}
    Create Test Users  lead tech    201
    ${lead_tech_token}=  get user token    lead tech
    ${lead_tech_id}=  get logged in user id  ${lead_tech_token}
    set suite variable  ${lead_tech_id}
    ${lead_tech_email}=  get user email  lead tech
    set suite variable  ${lead_tech_email}
    Create Test Users  review doctor    201

Multiple Teardown Methods
    Delete Created Users   lead tech  200
    delete device by id  ${device_id}   200
    Delete Created Users   super admin   200
    Delete Created Users   review doctor   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***

Set User Role
    initialize access for user  Review Doctor

API Version
    get_version_number  ${super_admin_token}

Login to RAS as Review Doctor User
    ${token}=  login to ras    review doctor
    set suite variable  ${token}
    set token of account class  ${token}

# ACCOUNT CONTROLLER
ReviewDoctor User can access Authorized
    [Tags]    AccountController      Authorized
    authorized

ReviewDoctor User cannot access Reset Password
    [Tags]    AccountController      ResetUserPassword
    reset user password  ${lead_tech_id}

ReviewDoctor User can access Forget Password
    [Tags]    AccountController      ForgotPassword
    forget password  ${lead_tech_email}

ReviewDoctor User can access Account Recovery
    [Tags]    AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access User Info
    [Tags]    AccountController      UserInfo
    user info

ReviewDoctor User can access Is Authorized For Device Use
    [Tags]    AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${device_id}

ReviewDoctor User can access Manage Info
    [Tags]    AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access Change Password
    [Tags]    AccountController      ChangePassword
    change password

ReviewDoctor User can access Set Password
    [Tags]    AccountController      SetPassword
    set password

ReviewDoctor User can access Add External Login
    [Tags]    AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access Remove Login
    [Tags]    AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access External Login
    [Tags]    AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access External Logins
    [Tags]    AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

ReviewDoctor User can access Log Out
    [Tags]    AccountController      Logout
    log out

