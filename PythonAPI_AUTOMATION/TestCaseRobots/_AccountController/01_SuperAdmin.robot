*** Settings ***
Documentation     Test Cases to validate all Account Controller endpoints authorization for Super Admin role

Library     ../../TestCaseMethods/AccountTCs.py
Library     ../../TestCaseMethods/Common.py
Library     ../../TestCaseMethods/DevicesTCs.py

Resource  ../global_robot_variables.robot


*** Keywords ***
Multiple Setup Methods
    Create Test Users  super admin    201
    ${super_admin_token}=  get user token    super admin
    set suite variable  ${super_admin_token}
    set token of devices class  ${super_admin_token}
    ${device_id}=  create device    201
    set suite variable  ${device_id}
    Create Test Users  review doctor    201
    ${review_doctor_token}=  get user token    review doctor
    ${review_doctor_id}=  get logged in user id  ${review_doctor_token}
    set suite variable  ${review_doctor_id}
    ${review_doctor_email}=  get user email  review doctor
    set suite variable  ${review_doctor_email}


Multiple Teardown Methods
    delete device by id  ${device_id}   200
    Delete Created Users   super admin   200
    Delete Created Users   review doctor   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***

Set User Role
    initialize access for user  Super Admin

API Version
    get_version_number  ${super_admin_token}

Login to RAS as Super Admin
    ${token}=  login to ras    super admin
    set suite variable  ${token}
    set token of account class  ${token}

# ACCOUNT CONTROLLER
SuperAdmin can access Authorized
    [Tags]    AccountController      Authorized
    authorized

SuperAdmin can access Reset Password
    [Tags]    AccountController      ResetUserPassword
    ${new_password}=  reset user password  ${review_doctor_id}
    set suite variable  ${new_password}

Check if Review Doctor can login to RAS with New Password
    [Tags]    AccountController      NewPassword
    login to ras with email password  ${review_doctor_email}  ${new_password}

SuperAdmin can access Forget Password
    [Tags]    AccountController      ForgotPassword
    forget password  ${review_doctor_email}

SuperAdmin can access Account Recovery
    [Tags]    AccountController      AccountRecovery
    log  UNABLE TO TEST ACCOUNT RECOVERY. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access User Info
    [Tags]    AccountController      UserInfo
    user info

SuperAdmin can access Is Authorized For Device Use
    [Tags]    AccountController      IsAuthorizedForDeviceUse
    is authorized for device use  ${device_id}

SuperAdmin can access Manage Info
    [Tags]    AccountController      GetManageInfo
    log  UNABLE TO TEST MANAGE INFO. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access Change Password
    [Tags]    AccountController      ChangePassword
    change password

SuperAdmin can access Set Password
    [Tags]    AccountController      SetPassword
    set password

SuperAdmin can access Add External Login
    [Tags]    AccountController      AddExternalLogin
    log  UNABLE TO TEST ADD EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access Remove Login
    [Tags]    AccountController      RemoveLogin
    log  UNABLE TO TEST ADD REMOVE LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access External Login
    [Tags]    AccountController      GetExternalLogin
    log  UNABLE TO TEST EXTERNAL LOGIN. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access External Logins
    [Tags]    AccountController      GetExternalLogins
    log  UNABLE TO TEST EXTERNAL LOGINS. REQUIRED PARAMS ARE UNKNOWN!   WARN

SuperAdmin can access Log Out
    [Tags]    AccountController      Logout
    log out

