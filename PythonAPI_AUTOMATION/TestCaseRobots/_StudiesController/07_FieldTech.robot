*** Settings ***
Documentation     Test Cases to validate all endpoints authorization for FieldTech role

Library     ../../TestCaseMethods/StudiesTCs.py
Library     ../../TestCaseMethods/PatientsTCs.py
Library     ../../TestCaseMethods/Common.py

Resource  ../global_robot_variables.robot

*** Keywords ***
Multiple Setup Methods
    Create Test Users  field tech    201
    ${token_for_version}=  get user token    field tech
    set suite variable  ${token_for_version}
    ${token_for_super_admin}=   login to ras with email password    ${email}    ${password}
    set suite variable  ${token_for_super_admin}
    set token of patients class  ${token_for_super_admin}
    set token of patients class for super admin  ${token_for_super_admin}
    ${patient_id}=  create patient      200
    set suite variable  ${patient_id}

Multiple Teardown Methods
    delete patient id  ${patient_id}    200
    Delete Created Users   field tech   200

*** Settings ***
Suite Setup       run keyword  Multiple Setup Methods
Suite Teardown    run keyword  Multiple Teardown Methods

*** Test Cases ***
API Version
    get_version_number  ${token_for_version}

Login to RAS as FieldTech
    ${token}=  login to ras    field tech
    set suite variable  ${token}
    set token of studies class  ${token}
    set token of studies class for super admin  ${token_for_super_admin}

# STUDIES CONTROLLER
FieldTech cannot access Get Studies
    [Tags]    StudiesController        Get
    get studies  403

FieldTech can access StudyUploadSetup
    [Tags]      StudiesController       StudyUploadSetup
    ${study_id}=    study upload setup  ${patient_id}   200
    set suite variable  ${study_id}

FieldTech can access AwsBucketForStudy
    [Tags]      StudiesController       AwsBucketForStudy
    aws bucket for study    ${study_id}     200

FieldTech cannot access Delete Study
    [Tags]      StudiesController       Delete
    delete study  ${study_id}   403


