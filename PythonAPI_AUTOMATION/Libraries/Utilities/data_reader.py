import csv
import os
from collections import defaultdict


def read_data_by_row(path_to_file, name_of_file_with_extension, column_header, row_header):
    """
    This method will read a file that has Comma Separated Values(.txt/.csv), and return the row you are looking for
    :param path_to_file: Path to the .csv/.txt file
    :param name_of_file_with_extension: Name of the Input file
    :param column_header: Name of the column from where the row name will be searched
    :param row_header: Name of the row that you want to get from the Input file
    :return: The values of the row in a dictionary.
             E.g. [('User_Role', 'Review Doctor'), ('Dashboard', 'Yes'), ('Info_Box', 'Yes')]
    """
    input_file = os.path.abspath(os.path.join(path_to_file, name_of_file_with_extension))
    try:
        with open(input_file, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                if row[column_header].lower() == row_header.lower():
                    return row

    except IOError:
        raise AssertionError("Error: cannot find file or read data")


def read_data_by_column(path_to_file, name_of_file_with_extension):
    """
    This method will read a file that has Comma Separated Values(.txt/.csv), and return the values of columns
     in lists
    :param path_to_file: Path to the .csv/.txt file
    :param name_of_file_with_extension: Name of the Input file
    :return: All the columns as a dictionary
             E.g. {'User_Role': ['Super Admin', 'Support', 'Production', 'Facility Admin', 'Review Doctor',
            'Lead Tech', 'Field Tech', 'Office Personnel'],
            'Dashboard': ['Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes']

    """
    input_file = os.path.abspath(os.path.join(path_to_file, name_of_file_with_extension))
    try:
        columns = defaultdict(list)  # each value in each column is appended to a list
        with open(input_file) as csv_file:
            csv_reader = csv.DictReader(csv_file)   # read rows into a dictionary format
            for row in csv_reader:                  # read a row as {column1: value1, column2: value2,...}
                for (k, v) in row.items():          # go over each column name and value
                    columns[k].append(v)            # append the value into the appropriate list based on column name k

        return columns
    except IOError:
        raise AssertionError("Error: cannot find file or read data")
