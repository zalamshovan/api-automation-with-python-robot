import smtplib
import os
import sys
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders

lib_path = os.path.abspath(os.path.join('..', 'Libraries'))
sys.path.insert(0, lib_path)
import conf as init

gmail_user = 'enosis.mmt@gmail.com'
gmail_password = 'Admin1@3'

sent_from = gmail_user
recipients = init.recipients
subject = init.subject_for_email
body = '***THIS IS AN AUTOMATED EMAIL. Please do NOT reply to this message.***\n\n' \
       'The attached file contains the test reports of all the controllers.\n' \
       'If you have any queries, please feel free to contact sheikh.raiyan@enosisbd.com'

msg = MIMEMultipart()
msg['To'] = ", ".join(recipients)
msg['From'] = sent_from
msg['Subject'] = subject
zipFilePath = os.path.abspath(os.path.join('..', 'TestReportsOfAllControllers.zip'))
zf = open(zipFilePath, 'rb')
part = MIMEBase('application', 'zip')
part.set_payload(zf.read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', 'attachment; filename=TestReportsOfAllControllers.zip')
msg.attach(part)
msg.attach(MIMEText(body, 'plain'))

email_text = msg.as_string()


server = smtplib.SMTP('smtp.gmail.com', 587)
server.ehlo()
server.starttls()
server.login(gmail_user, gmail_password)
server.sendmail(sent_from, recipients, email_text)
server.close()

print('Email sent!')
