import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)

import requests
from functools import wraps
from logger_robot import logger as logger_extended
import time

status = logger_extended().get_logger('API_Automation')


def my_timer(orig_func):
    """
    @desc - This is a python decorator to calculate the time taken to perform a method
    :param - orig_func: Name of the method
    :rtype : wrapper
    """
    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('{} ran in: {} sec'.format(orig_func.__name__, t2))
        status.info('{} executed in: {} sec'.format(orig_func.__name__, t2))
        return result
    return wrapper


@my_timer
def request(method, url, data, headers, params=None):
    """
    @desc - This is a custom request field to track the time required to perform the request.
    :param - method: Type of Request
    :param - url: URL to the Request
    :param - data: Payload
    :param - headers: Header elements
    :param - params: Query string (Optional)
    :rtype : response
    """
    status.info('METHOD: {}; URL : {}'.format(method, url))
    if params is not None:
        return requests.request(method, url, data=data, headers=headers, params=params)
    else:
        return requests.request(method, url, data=data, headers=headers)


def code_match(found_code, target_code):
    """
    @desc - The method verifies if the found code matches with the target code.
    :param found_code: Code that was returned from server
    :param target_code: Yes/No or pass the explicit code
    :return:
    """
    if target_code in ('Yes', 'No', 'YES', 'NO', 'UNRESTRICTED', 'Unrestricted'):
        if target_code in ('Yes', 'YES', 'UNRESTRICTED', 'Unrestricted'):
            if found_code in ('200', '201', '204'):
                print("Returned status code: " + found_code + ". And the user has access to the Endpoint.")
                status.info('Returned Code:{} - PASSED'.format(found_code))
                return "success"
            elif found_code == "404":
                print('404: Not Found.')
                status.warn('Returned Code:{} - NOT FOUND'.format(found_code))
        elif target_code in ('No', 'NO'):
            if found_code != "403":
                status.error('Returned Code:{} - FAILED'.format(found_code))
                raise AssertionError("User does not have access to the Endpoint. But " + found_code + " was returned.")
    else:
        if found_code == target_code:
            print("Returned status code: " + found_code + " matched with the expected: " + target_code)
            status.info('Returned Code:{} - PASSED'.format(target_code))
            return "success"
        elif found_code == "404":
            print('404: Not Found.')
            status.warn('Returned Code:{} - NOT FOUND'.format(found_code))
            raise AssertionError("Status code expected " + target_code + " but found " + found_code)
        else:
            status.error('Returned Code:{} - FAILED'.format(found_code))
            raise AssertionError("Status code expected " + target_code + " but found " + found_code)


def code_match_old(found_code, target_code):
    """
    @desc - This method is for deciding pass/fail
    :param - found_code: Status code returned by the response
    :param - target_code: Expected code to be returned
    :rtype : pass/fail
    """
    if found_code == target_code:
        print("Returned status code: "+found_code+" matched with the expected: "+target_code)
        status.info('Returned Code:{} - PASSED'.format(target_code))
        return "success"
    elif found_code == "404":
        print('404: Not Found.')
        status.warn('Returned Code:{} - NOT FOUND'.format(found_code))
        raise AssertionError("Status code expected " + target_code + " but found " + found_code)
    else:
        status.error('Returned Code:{} - FAILED'.format(found_code))
        raise AssertionError("Status code expected "+target_code+" but found "+found_code)


def add_version_number_in_report(path, html_name, version_number):
    """
    @desc - rewrite html file to embed Version Number In Log file
    :param path:
    :param html_name:
    :param version_number:
    :return:
    """
    with open(path + html_name, 'r+') as f:
        data = f.read()
        f.seek(0)
        # f.write(re.sub("'<div id=\"generated\">' +",
        #                "'<h2>" + version_number + "</h2>' +  '<h1> </h1>' + '<div id=\"generated\">' ", data))
        f.truncate()
        f.close()
