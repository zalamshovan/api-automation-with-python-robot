# API SERVER URL
# url = "https://napi-stagingn.rendrneuro.com/api/"
url = "https://napi-stagingn.rendrneuro.com"

# # EXISTING USERS' EMAILS
# super_admin = "rendrsuper@yahoo.com"
# support = "rendrsupp.enosis@yahoo.com"
# production = "rendrprouser@yahoo.com"
# facility_admin = "rendr_facilityadm@yahoo.com"
# review_doctor = "rendrreviewdoctor@yahoo.com"
# lead_tech = "rendrleadtech@yahoo.com"
# field_tech = "rendrfieldtech@yahoo.com"
# office_personnel = "rendrofficepersonnel@yahoo.com"

# COMMON PASSWORD
password = "Enosis123"

# ID OF RENDR USER ROLES
super_admin_role = "907198ED-EFBD-4B48-ABE9-1C62C129D947"
support_role = "1139367A-07B8-46D3-A949-20DFE2B7E9AC"
production_role = "A247B200-ED53-4F9A-8449-4E56043A00C1"
facility_admin_role = "C94FD9B6-0C93-4901-91F7-764FEEE1FA21"
review_doctor_role = "289C85AC-F6CB-4DA5-9BA1-06A8E467821E"
lead_tech_role = "17950AA1-181D-4BA2-8FA5-CB8A91283B4B"
field_tech_role = "F6A4A81B-4BB0-452D-908C-5563D4A77CCE"
office_personnel_role = "EECD0CA1-5EAF-46CC-A3F1-7B90248F6CEF"

# # TEST USERS' EMAILS
# api_super_admin = "test.enosisbd@gmail.com"
# api_support = "enosisqa.2@gmail.com"
# # api_production = "testenosisbd02@gmail.com"  ## Cannot be used anymore
# api_production = "enosisqa_6@hotmail.com"
# api_facility_admin = "enosis.test2@gmail.com"
# # api_review_doctor = "enosistestbd@gmail.com"  ## STH WIERD HAPPENED WITH THIS ONE
# api_review_doctor = "naeer.amin@enosisbd.com"
# # api_lead_tech = "enosistestbd02@gmail.com"
# api_lead_tech = "enosisqa.3@gmail.com"
# # api_field_tech = "enosis.mmt@gmail.com"
# api_field_tech = "enosisqa_5@hotmail.com"
# # api_office_personnel = "zalamshovan@gmail.com"
# api_office_personnel = "enosis.mmt@gmail.com"

# IDs OF MOBILEMEDTEK SUPER ADMIN USER
default_super_admin_id = "2E449249-26B7-4DDB-8ED2-92E1655EA217"

# MOBILEMEDTEK FACILITY ID
mobile_med_tek_facility_id = "e3dd1742-335e-438e-8d8d-65df10447ea3"

# ENOSIS FACILITY ID
enosis_facility_id = "64dd56bc-85e6-4010-9c4a-b110b6800ad1"

# DARK THEME ID
dark_theme_id = "595ee95c-815d-4d13-bc3b-8d255373a6cc"

# EMAIL BEING USED TO SEND FACILITY INVITATION
email_address_for_invitation = "shovan.ahmedzia@ymail.com"

# REPORT EMAIL WILL BE SENT WITH FOLLOWING SUBJECT
subject_for_email = 'Test Reports of All the Controllers'

# LIST OF THE RECIPIENTS WHO WILL GET REPORT EMAIL
recipients = [
                'sheikh.raiyan@enosisbd.com',
                'naeer.amin@enosisbd.com',
                'nafis.kamal@enosisbd.com',
                'zubair.alam@enosisbd.com'
            ]

# INFO RELATED TO CSVs
api_access_csv_source = "../../DataSource/AccessToAPIs/"
user_credentials = "../../DataSource/UserCredentials/"
