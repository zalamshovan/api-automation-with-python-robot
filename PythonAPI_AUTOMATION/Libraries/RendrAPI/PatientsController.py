import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class PatientsController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "Patients/"
    user_token = ""
    super_admin_token = ""

    created_patient_id = ""
    facility_id = ""
    birth_date = ""
    user_defined_pat_id = ""

    def __init__(self):
        print("PatientsController class initialized.")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.super_admin_token = token

    def get_search(self, super_admin, search_string, expected_code):
        """
        @desc - method to check search endpoint. Gets a List of Patients by the search criteria.
        :param super_admin: if set to True, token of Super Admin will be used instead of logged in user. This is needed
        to ensure patient can be created even if logged in user does not have that role.
        :param search_string:
        :param expected_code:
        :return:
        """
        url = self.url + "Search"

        querystring = {'searchString': search_string}

        payload = ""

        if super_admin is False:
            token = self.user_token
        else:
            token = self.super_admin_token

        headers = {
            'Authorization': "Bearer " + token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == "200":
            json_response = response.json()
            pat = json_response[0]['ID']
            return pat

    def post_patient(self, f_name, l_name, fac_id, expected_code):
        """
        @desc - Create a patient. If logged in user cannot create, patient will be created by super admin token
        :param f_name: first name
        :param l_name: last name
        :param fac_id: facility id
        :param expected_code:
        :return:
        """
        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
        st3 = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S.%f')[:-3]

        date_of_birth = st1 + "T" + st3 + "Z"  # To get the format-> 2019-05-26T05:58:24.566Z

        pat_id = "PAT_" + date_of_birth

        url = self.url

        self.facility_id = fac_id
        self.birth_date = date_of_birth
        self.user_defined_pat_id = pat_id

        payload = "{\r\n  " \
                  "\"FacilityID\": \"" + fac_id + "\",\r\n  " \
                  "\"FirstName\": \"" + f_name + "\",\r\n  " \
                  "\"LastName\": \"" + l_name + "\",\r\n  " \
                  "\"DateOfBirth\": \"" + date_of_birth + "\",\r\n  " \
                  "\"PatientID\": \"" + pat_id + "\",\r\n  " \
                  "\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        if str(response.status_code) == "200":
            self.created_patient_id = self.get_search(super_admin=True, search_string=pat_id, expected_code="200")
            print("Patient ID of the newly created Patient : " + self.created_patient_id)

        elif str(response.status_code) == "403":
            print("Patient could not be created with logged in user")
            print("Patient is now being created using SuperAdmin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers)
                print(new_response.text)
                if str(new_response.status_code) == "200":
                    print("Patient has been created using SuperAdmin token")
                    self.created_patient_id = self.get_search(super_admin=True, search_string=pat_id,
                                                              expected_code="200")
                    print("Patient ID of the newly created Patient : " + self.created_patient_id)
                else:
                    raise AssertionError("Something went wrong. Patient could not be created")

        utils.code_match(str(response.status_code), expected_code)
        return self.created_patient_id

    def get_get_patient_for_editing(self, pat_id, expected_code):
        """
        @desc - get patient info with patient id. Gets a specific patient for editing
        :param pat_id: id of the patient
        :param expected_code:
        :return:
        """
        url = self.url + "GetPatientForEditing"

        if pat_id is None:
            pat_id = self.created_patient_id

        querystring = {'id': pat_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_patients(self, pat_id, expected_code):
        """
        @desc - Delete a patient with id
        :param pat_id: id of the patient
        :param expected_code:
        :return:
        """
        if pat_id is None:
            url = self.url + self.created_patient_id
        else:
            url = self.url + pat_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "403":
            print("Logged in user could not delete the patient")
            print("Patient will be deleted by super admin token")
            new_headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }

            new_response = utils.request("DELETE", url, data=payload, headers=new_headers)
            if str(new_response.status_code) == "200":
                print("Patient has been deleted")
            else:
                print("Something went wrong. Patient could not be deleted")

        utils.code_match(str(response.status_code), expected_code)

    def get_patient_id(self, pat_id, expected_code):
        """
        @desc - Get patient info with id
        :param pat_id:
        :param expected_code:
        :return:
        """
        if pat_id is None:
            url = self.url + self.created_patient_id
        else:
            url = self.url + pat_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_patients(self, expected_code):
        """
        @desc - Get all patients' info. Gets a list of all the patients at a given facility
        :param expected_code:
        :return:
        """
        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_patient(self, expected_code):
        """
        @desc - Updates a patient by ID.
        :param expected_code:
        :return:
        """
        url = self.url + self.created_patient_id

        payload = "{\r\n  " \
                  "\"FirstName\": \"" + "EDITED_F" + "\",\r\n  " \
                  "\"LastName\": \"" + "EDITED_L" + "\",\r\n  " \
                  "\r\n}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def patch_patients(self, expected_code):
        """
        @desc - Patch patient.
        :param expected_code:
        :return:
        """
        url = self.url + self.created_patient_id

        payload = "{" \
                  "\"ID\":\"" + self.created_patient_id + "\",\n" \
                  "\"FirstName\":\"EDITED\",\n" \
                  "\"MiddleName\": \"\",\n  " \
                  "\"LastName\":\"EDITED\",\n" \
                  "\"DateOfBirth\":\"" + self.birth_date + "\",\n" \
                  "\"Sex\": \"\",\n  " \
                  "\"SSN\": \"\",\n  " \
                  "\"PatientID\": \"" + self.user_defined_pat_id + "\",\n  " \
                  "\"FacilityID\":\"" + self.facility_id + "\"" \
                  "}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

