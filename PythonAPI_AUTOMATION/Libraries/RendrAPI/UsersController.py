import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'rendrapi'))
sys.path.insert(0, lib_path)
import rendr_util


class UsersController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "/api/Users/"
    super_admin_token = ""
    user_token = ""
    user_id = ""
    facility_id = ""
    first_name = ""
    last_name = ""
    email = ""
    role_id = ""
    user_name = ""
    password = ""

    def __init__(self):
        print("UsersController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.super_admin_token = token

    def post_create_user(self, first_name, last_name, email, password, role, delete_if_conflicts, expected_code):
        """
        @desc - Creates a new MMT user and adds a facility user for the logged in user's facility
        :param - first_name: user's first name
        :param - last_name: user's last name
        :param - email: user's email address
        :param - password: user's password
        :param - role: user's role ID
        :param - delete_if_conflicts: boolean. Set to true if you want to delete the existing user if conflicts
        :param - expected_code: expected status code
        :rtype : created user's id
        """
        url = self.url + "CreateUser"

        payload = "{\n  " \
                  "\"Password\": \"" + password + "\",\n  " \
                  "\"ConfirmPassword\": \"" + password + "\",\n  " \
                  "\"RoleId\": \"" + role + "\",\n  " \
                  "\"FirstName\": \"" + first_name + "\",\n  " \
                  "\"LastName\": \"" + last_name + "\",\n  " \
                  "\"Email\": \"" + email + "\"\r\n}"

        headers = {
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'Authorization': "Bearer " + self.user_token,
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == "409":
            print("Conflict found. User with " + email + " already exits")
            if delete_if_conflicts is True:
                json_response = response.json()
                print("Deleting conflicted user")
                delete_id = json_response['ID']
                delete_url = self.url + delete_id

                headers_for_delete = {
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'Authorization': "Bearer " + self.super_admin_token,
                    'cache-control': "no-cache"
                }
                response_delete = utils.request("DELETE", delete_url, data="", headers=headers_for_delete)
                if str(response_delete.status_code) == "200":
                    print("Previous user has been deleted.")
                    print("Creating new user with the same email.")
                    response_new = utils.request("POST", url, data=payload, headers=headers)
                    print("NEW : "+response_new.text)
                    print("STATUS : "+str(response_new.status_code))
                    utils.code_match(str(response_new.status_code), expected_code)
                    if str(response_new.status_code) == "201":
                        json_response_new = response_new.json()
                        print("ID of the newly created User : " + json_response_new['ID'])
                        self.user_id = json_response_new['ID']
                        self.facility_id = json_response_new['SelectedFacilityID']
                        self.first_name = json_response_new['FirstName']
                        self.last_name = json_response_new['LastName']
                        self.email = json_response_new['Email']
                        self.user_name = json_response_new['Username']
                        self.role_id = json_response_new['Roles'][0]
                        self.password = password
                        return self.user_id
                else:
                    raise AssertionError("Could not delete created user")
        elif str(response.status_code) == "201":
            json_response = response.json()
            print("ID of the newly created User : " + json_response['ID'])
            self.user_id = json_response['ID']
            self.facility_id = json_response['SelectedFacilityID']
            self.first_name = json_response['FirstName']
            self.last_name = json_response['LastName']
            self.email = json_response['Email']
            self.user_name = json_response['Username']
            self.role_id = json_response['Roles'][0]
            self.password = password
            utils.code_match(str(response.status_code), expected_code)
            return self.user_id
        elif str(response.status_code) == "403":
            print("Logged in user cannot create a user.")
            self.user_id = rendr_util.get_logged_in_user_id(self.user_token)
            self.facility_id = rendr_util.get_logged_in_user_facility_id(self.user_token)
            self.first_name = "Zombie"
            self.last_name = "Zombie"
            self.email = "zombie@zombie.com"
            self.user_name = "zombie@zombie.com"
            self.role_id = init.office_personnel_role
            self.password = init.password
            utils.code_match(str(response.status_code), expected_code)
            return self.user_id

    def delete_delete_facility_user(self, user_id, expected_code):
        """
        @desc - This method deletes a facility user with the given MMT user ID at the logged in user's selected facility
        :param - user_id: user's id. Do not user the logged in user's ID
        :param - expected_code: expected status code
        """
        url = self.url + "DeleteFacilityUser"

        if user_id is None:
            user_id = self.user_id

        querystring = {'id': user_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_change_user_role(self, user_id, role_id, expected_code):
        """
        @desc - Changes the given user's role at the logged in user's current facility
        :param - user_id: user's ID. Whose role needs to be changed
        :param - role_id: target role ID
        :param - expected_code: expected status code
        """
        url = self.url + "ChangeUserRole"

        querystring = {'userId': user_id,
                       'newRoleId': role_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_my_roles(self, expected_code):
        """
        @desc - Returns callers current MMT and Facility role names.
        :param - expected_code: expected status code
        """
        url = self.url + "GetMyRoles"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_my_theme(self, expected_code):
        """
        @desc - Returns the users default theme or 404 Not Found
        :param - expected_code: expected status code
        """
        url = self.url + "GetMyTheme"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

        if response.text == "null":
            print("User does not have any theme as selected")
            return None
        else:
            return response.json()

    def post_set_my_theme(self, theme_id, expected_code):
        """
        @desc - Sets the users default theme
        :param - theme_id: ID of the theme. If found null then theme is set to Dark theme ID.
        :param - expected_code: expected status code
        """
        url = self.url + "SetMyTheme"

        querystring = {'id': theme_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_user_id(self, user_id, expected_code):
        """
        @desc - Gets an MMT user given the user's ID
        :param - user_id: ID of the user.
        :param - expected_code: expected status code
        """
        if user_id is None:
            user_id = self.user_id
        url = self.url + user_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_set_selected_facility(self, facility_id, expected_code):
        """
        @desc - Change user's selected facility
        :param - facility_id: ID of the facility.
        :param - expected_code: expected status code
        """
        url = self.url + "SetSelectedFacility"

        querystring = {'id': facility_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        return utils.code_match(str(response.status_code), expected_code)

    def post_is_user_in_role(self, role_name, expected_code):
        """
        @desc - Checks if the logged in user has the given MMT role or Facility role at the logged in user's selected
        facility
        :param - role_name: NAME of the role.
        :param - expected_code: expected status code
        """
        url = self.url + "IsUserInRole"

        querystring = {'roleName': role_name}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        return response.text

    def get_get_user_info(self, user_id, expected_code):
        """
        @desc - Gets various user information for a given user id
        :param - user_id: ID of the user
        :param - expected_code: expected status code
        """
        url = self.url + "GetUserInfo"
        if user_id is None:
            user_id = self.user_id

        querystring = {'id': user_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) != "403":
            return response.json()

    def get_get_users(self, expected_code):
        """
        @desc - Gets a list of facility users from the logged in user's selected facility
        :param - expected_code: expected status code
        """
        url = self.url + "GetUsers"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) != "403":
            return response.json()

    def patch_user_id(self, expected_code):
        """
        @desc - Updates the user's information
        :param - expected_code: expected status code
        """

        url = self.url + self.user_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        st2 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%S%d')

        payload = "{\n  " \
                  "\"ID\": \"" + self.user_id + "\",\n  " \
                  "\"Title\": \"\",\n  " \
                  "\"FirstName\": \"" + self.first_name + st1 + st2 + "\",\n  " \
                  "\"MiddleName\": \"\",\n  " \
                  "\"LastName\": \"" + self.last_name + st1 + st2 + "\",\n  " \
                  "\"Suffix\": \"\",\n  " \
                  "\"Email\": \"" + self.email + "\",\n  " \
                  "\"TwoFactorEnabled\": false,\n  " \
                  "\"FacilityID\": \"\",\n  " \
                  "\"Password\": \"\",\n  " \
                  "\"ConfirmPassword\": \"\",\n  " \
                  "\"RoleId\": \"" + self.role_id + "\",\n  " \
                  "\"Role\": \"" + self.role_id + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_users(self, expected_code):
        """
        @desc - Really don't know what it does :/
        :param - expected_code: expected status code
        """
        url = self.url + self.user_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        st2 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%S%d')

        payload = "{\n  " \
                  "\"ID\": \"" + self.user_id + "\",\n  " \
                  "\"Title\": \"\",\n  " \
                  "\"FirstName\": \"" + self.first_name + st1 + st2 + "\",\n  " \
                  "\"MiddleName\": \"\",\n  " \
                  "\"LastName\": \"" + self.last_name + st1 + st2 + "\",\n  " \
                  "\"Suffix\": \"\",\n  " \
                  "\"Email\": \"" + self.email + "\",\n  " \
                  "\"TwoFactorEnabled\": false,\n  " \
                  "\"FacilityID\": \"\",\n  " \
                  "\"Password\": \"\",\n  " \
                  "\"ConfirmPassword\": \"\",\n  " \
                  "\"RoleId\": \"" + self.role_id + "\",\n  " \
                  "\"Role\": \"" + self.role_id + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_users(self, expected_code):
        """
        @desc - Why is it still here?
        :param - expected_code: expected status code
        """
        url = self.url

        payload = "{\n  " \
                  "\"Password\": \"" + self.password + "\",\n  " \
                  "\"ConfirmPassword\": \"" + self.password + "\",\n  " \
                  "\"RoleId\": \"" + self.role_id + "\",\n  " \
                  "\"FirstName\": \"" + self.first_name + "\",\n  " \
                  "\"LastName\": \"" + self.last_name + "\",\n  " \
                  "\"Email\": \"" + self.email + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_delete_user_id(self, user_id, expected_code):
        """
        @desc - Deletes an MMT user and all the facility users associated with this user
        :param - user_id: ID of the user to be deleted. Do not user logged in user's id
        :param - expected_code: expected status code
        """
        if user_id == init.default_super_admin_id or self.user_id == init.default_super_admin_id:
            raise AssertionError("You cannot delete Default Admin user (admin@mobilemedtek.com)")

        if user_id is None:
            user_id = self.user_id

        url = self.url + user_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache",
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
