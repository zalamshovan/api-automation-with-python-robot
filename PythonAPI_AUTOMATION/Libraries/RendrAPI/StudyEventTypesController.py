import os
import sys
import datetime
import time

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path1)
import conf as init

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path2)
import utils
from logger_robot import logger as logger_extended


class StudyEventTypesController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + 'StudyEventTypes/'

    user_token = ""

    super_admin_token = ""

    study_event_type_id = ""
    study_event_type_name = ""
    study_event_type_description = ""

    def __int__(self):
        print("StudyEventTypesController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token:
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin user's token
        :return:
        """
        self.super_admin_token = token

    def post_study_event_types(self, study_event_type_name, description, expected_code):
        """
        @desc - method to create a new study event type
        :param study_event_type_name: name of the study event
        :param description: Description of the study event type
        :param expected_code: expected status code
        :return:
        """
        url = self.url

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M$Sd')

        self.study_event_type_name = study_event_type_name + st1
        self.study_event_type_description = description + st1

        payload = "{\n\t\"" \
                  "Name\": \"" + self.study_event_type_name + "\",\n\t\"" \
                  "Description\": \"" + self.study_event_type_description + "\"\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '201':
            json_response = response.json()
            self.study_event_type_id = str(json_response['ID'])
            print("ID of the newly created study event type: {}".format(self.study_event_type_id))
            self.study_event_type_name = json_response['Name']
            print("Name of the newly created study event type: {}".format(self.study_event_type_name))
            self.study_event_type_description = json_response['Description']
            print("Description of the newly created study event type: {}".format(self.study_event_type_description))
        elif str(response.status_code) == '403':
            print("Study Event Type could not be created using logged in user")
            print("Study Event Type now being created using Super Admin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache"
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers)
                print(new_response.text)
                if str(new_response.status_code) == '201':
                    json_response = new_response.json()
                    self.study_event_type_id = str(json_response['ID'])
                    print("ID of the newly created study event type: {}".format(self.study_event_type_id))
                    self.study_event_type_name = json_response['Name']
                    print("Name of the newly created study event type: {}".format(self.study_event_type_name))
                    self.study_event_type_description = json_response['Description']
                    print("Description of the newly created study event type: {}".format(
                        self.study_event_type_description))
                else:
                    raise AssertionError("Something went wrong. StudyEventType could not be created")
        utils.code_match(str(response.status_code), expected_code)

    def get_study_event_types(self, expected_code):
        """
        @desc - method to get a list of study event types
        :param expected_code: expected status code
        :return:
        """
        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_study_event_types_id(self, study_event_type_id, expected_code):
        """
        @desc - method to get study event types by id
        :param study_event_type_id: ID of the study event type
        :param expected_code: expected status code
        :return:
        """
        if study_event_type_id is None:
            url = self.url + self.study_event_type_id
        else:
            url = self.url + study_event_type_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def patch_study_event_types(self, type_id, name, description, expected_code):
        """
        @desc - method to edit study event type using patch endpoint
        :param type_id: ID of the study event type
        :param name: name of the study event type
        :param description: description of the study event type
        :param expected_code: expected status code
        :return:
        """
        if type_id is None:
            url = self.url + self.study_event_type_id
        else:
            url = self.url + type_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M$Sd')

        self.study_event_type_name = name + st1
        self.study_event_type_description = description + st1

        payload = "{\r\n  \"" \
                  "Name\": \"" + self.study_event_type_name + "\",\r\n  \"" \
                  "Description\": \"" + self.study_event_type_description + "\"\r\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("PATCH", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_study_event_types(self, type_id, name, description, expected_code):
        """
        @desc - method to edit study event type using put endpoint
        :param type_id: ID of the study event type
        :param name: Name of the study event type
        :param description: Description of the study eve
        :param expected_code: expected status code
        :return:
        """
        if type_id is None:
            url = self.url + self.study_event_type_id
        else:
            url = self.url + type_id
            self.study_event_type_id = type_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M$Sd')

        self.study_event_type_name = name + st1
        self.study_event_type_description = description + st1

        payload = "{\r\n  \"" \
                  "ID\": \"" + self.study_event_type_id + "\",\r\n  \"" \
                  "Name\": \"" + self.study_event_type_name + "\",\r\n  \"" \
                  "Description\": \"" + self.study_event_type_description + "\" \r\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("PUT", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_study_event_types(self, type_id, expected_code):
        """
        @desc - method to delete a study event types
        :param type_id: ID of the study event type
        :param expected_code: expected status code
        :return:
        """
        if type_id is None:
            url = self.url + self.study_event_type_id
        else:
            url = self.url + type_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("DELETE", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '403':
            print("Logged in user could not delete study event type")
            print("Study Event type will now be deleted using SuperAdmin token")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == '200':
                print("Study Event Type will now be deleted using SuperAdmin token")
            else:
                print("Something went wrong! Study Event Type was not deleted")
        utils.code_match(str(response.status_code), expected_code)
