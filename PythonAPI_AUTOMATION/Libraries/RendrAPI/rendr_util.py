import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
from logger_robot import logger as logger_extended
import utils
from TestDataProvider import TestDataProvider

status = logger_extended().get_logger('API_Automation')
data = TestDataProvider()


def login_to_rendr(email, password):
    """
    @desc - This method is for login into RAS
    :param - email: Email address of the user
    :param - password: Password of the user
    :rtype : access_token (if logged in properly)
    """
    login_url = init.url + data.get_endpoint_url(controller_name="AccountController", endpoint_name="Login")

    print("LOGIN - " + login_url)

    payload = "username="+email+"&password="+password+"&grant_type=password"

    headers = {
        'cache-control': "no-cache"
     }

    print(email)
    print(password)

    response = utils.request("POST", login_url, data=payload, headers=headers)

    print(response.text)

    if response.status_code == 200:
        status.info("User ("+email+") logged in successfully")
        json_response = response.json()
        global access_token
        access_token = json_response['access_token']
        return access_token
    else:
        raise AssertionError("Could not login using Email:" + email + " and Password: " + password)


def get_logged_in_user_id(token):
    """
    @desc - This method returns user id based on user token
    :param - token
    :rtype : user id
    """
    user_info_url = init.url + data.get_endpoint_url(controller_name="AccountController", endpoint_name="UserInfo")
    payload = ""
    headers = {
        'Authorization': "Bearer "+token,
        'cache-control': "no-cache"
    }

    response = utils.request("GET", user_info_url, data=payload, headers=headers)
    json_response = response.json()

    if response.status_code == 200:
        status.info("Logged in user's ID: "+json_response['ID'])
        return json_response['ID']


def get_logged_in_user_facility_id(token):
    """
    @desc - This method returns facility id based on user token
    :param - token
    :rtype : facility id
    """

    # user_info_url = init.url + "Account/UserInfo"
    user_info_url = init.url + data.get_endpoint_url(controller_name="AccountController", endpoint_name="UserInfo")
    payload = ""
    headers = {
        'Authorization': "Bearer "+token,
        'cache-control': "no-cache"
    }

    response = utils.request("GET", user_info_url, data=payload, headers=headers)
    json_response = response.json()

    if response.status_code == 200:
        status.info("Logged in user's Facility ID: "+json_response['FacilityID'])
        return json_response['FacilityID']


def get_logged_in_user_facility_name(token):
    """
    @desc - This method returns facility name based on user token
    :param - token
    :rtype : facility name
    """
    # user_info_url = init.url + "Account/UserInfo"
    user_info_url = init.url + data.get_endpoint_url(controller_name="AccountController", endpoint_name="UserInfo")

    payload = ""
    headers = {
        'Authorization': "Bearer "+token,
        'cache-control': "no-cache"
    }

    response = utils.request("GET", user_info_url, data=payload, headers=headers)
    json_response = response.json()

    if response.status_code == 200:
        status.info("Logged in user's Facility Name: "+json_response['FacilityName'])
        return json_response['FacilityName']


def get_api_version(token):
    """
    @desc - This method returns API version
    :param token: user's token
    :return: API version
    """
    url = init.url + "/api/Meta/ApiVersion"

    payload = ""
    headers = {
        'Authorization': "Bearer "+token,
        'Accept': "application/json, text/json",
        'cache-control': "no-cache"
    }

    response = utils.request("GET", url, data=payload, headers=headers)

    print(response.text)
    if response.status_code == 200:
        status.info("API Version: Staging " + response.text)
        return response.text