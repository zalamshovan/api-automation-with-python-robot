from collections import defaultdict
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
from data_reader import read_data_by_row as read_row, read_data_by_column as read_col


class TestDataProvider(object):
    row_records = []
    col_records = []

    def get_row_data(self, path, csv_name, column_header, row_header):
        """
        This method reads a CSV row wise, and initializes row_records for future access
        :param path: path to the CSV
        :param csv_name: name of the CSV (do not add the extension)
        :param column_header: Name of the column header
        :param row_header: Name of the Row header
        """
        self.row_records = read_row(path_to_file=path,
                                    name_of_file_with_extension=csv_name+".csv",
                                    column_header=column_header,
                                    row_header=row_header)

    def get_column_data(self, path, csv_name):
        """
        This method reads a CSV column wise, and initializes col_records for future access
        :param path: path to the CSV
        :param csv_name: name of the CSV (do not add the extension)
        """
        self.col_records = read_col(path_to_file=path, name_of_file_with_extension=csv_name+".csv")

    def get_two_merged_lists_as_dictionary(self, column_1, column_2, key_of_dict_to_get):
        """
        This method merges two lists and create a dictionary. Element of the first list item works as the Key of
        dictionary. Length of both lists should be the same
        :param column_1:
        :param column_2:
        :param key_of_dict_to_get:
        :return:
        """
        result = list(zip(self.col_records[column_1], self.col_records[column_2]))
        temp_dict = defaultdict(list)
        for i, j in result:
            temp_dict[i].append(j)

        return temp_dict[key_of_dict_to_get]

    def get_test_user_email(self, user_role):
        """
        This method returns test user email.
        :param user_role: Role name
        :return: Email address of the user
        """
        input_file = os.path.abspath(os.path.join(init.user_credentials))
        if user_role in ('Super Admin',
                         'Support',
                         'Production',
                         'Facility Admin',
                         'Review Doctor',
                         'Lead Tech',
                         'Field Tech',
                         'Office Personnel'):
            self.get_row_data(path=input_file, csv_name="TestUserCredentials", column_header="Role Name",
                              row_header=user_role)
            print(self.row_records['Email'])
            return self.row_records['Email']
        else:
            raise AssertionError("Check the User Role name.")

    def get_rendr_user_email(self, user_role):
        """
        This method returns Rendr user email.
        :param user_role: Role name
        :return: Email address of the user
        """
        input_file = os.path.abspath(os.path.join(init.user_credentials))
        if user_role in ('Super Admin',
                         'Support',
                         'Production',
                         'Facility Admin',
                         'Review Doctor',
                         'Lead Tech',
                         'Field Tech',
                         'Office Personnel'):
            self.get_row_data(path=input_file, csv_name="UserCredentials", column_header="Role Name",
                              row_header=user_role)
            print(self.row_records['Email'])
            return self.row_records['Email']
        else:
            raise AssertionError("Check the User Role name.")

    @staticmethod
    def get_common_password():
        """
        Returns common password from the conf.py file
        :return:
        """
        return init.password

    def get_endpoint_url(self, controller_name, endpoint_name):
        """
        This method returns URL to the endpoint of controllers
        :param controller_name: Name of the Controller
        :param endpoint_name: Name of the endpoint
        :return:
        """
        input_file = os.path.abspath(os.path.join(init.api_access_csv_source))

        if controller_name in ('AccountController',
                               'AmplifiersController',
                               'ControlSoftwareController',
                               'DevicesController',
                               'EegThemesController',
                               'FacilitiesController',
                               'FacilityUsersController',
                               'NotificationsController',
                               'PatientsController',
                               'StudiesController',
                               'StudyEventsController',
                               'StudyEventTypesController',
                               'StudyRecordingsController',
                               'UsersController'):
            self.get_row_data(path=input_file, csv_name=controller_name, column_header="Endpoint",
                              row_header=endpoint_name)
        else:
            raise AssertionError("Select a proper Controller name")
        print(self.row_records['Super Admin'])
        return self.row_records['URL']

    def get_access_status(self, controller_name, endpoint_name, user_role):
        """
        This method returns YES/NO/UNRESTRICTED based of users authorization to access an endpoint
        :param controller_name:
        :param endpoint_name:
        :param user_role:
        :return:
        """
        input_file = os.path.abspath(os.path.join(init.api_access_csv_source))

        if controller_name in ('AccountController',
                               'AmplifiersController',
                               'ControlSoftwareController',
                               'DevicesController',
                               'EegThemesController',
                               'FacilitiesController',
                               'FacilityUsersController',
                               'NotificationsController',
                               'PatientsController',
                               'StudiesController',
                               'StudyEventsController',
                               'StudyEventTypesController',
                               'StudyRecordingsController',
                               'UsersController') and \
                user_role in ('Super Admin',
                              'Support',
                              'Production',
                              'Facility Admin',
                              'Review Doctor',
                              'Lead Tech',
                              'Field Tech',
                              'Office Personnel'):
            self.get_row_data(path=input_file, csv_name=controller_name, column_header="Endpoint",
                              row_header=endpoint_name)
            print(self.row_records[user_role])
            return self.row_records[user_role]
        else:
            raise AssertionError("Check the User Role name and Controller name")
