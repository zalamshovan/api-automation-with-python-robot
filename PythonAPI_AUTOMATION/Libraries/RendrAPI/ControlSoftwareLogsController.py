import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class ControlSoftwareLogsController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + 'ControlSoftwareLogs/'

    user_token = ""
    super_admin_token = ""

    control_software_log_id = ""

    def __int__(self):
        print("ControlSoftwareLogs class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin user's token
        :return:
        """
        self.super_admin_token = token

    def post_control_software_logs(self, device_id, user_name, facility_id, expected_code):
        """
        @desc - method to create a new control software log
        :param device_id: ID of the device for which log files are created
        :param user_name: Name of the user
        :param facility_id:
        :param expected_code:
        :return:
        """

        url = self.url

        payload = "{\n\t\"" \
                  "DeviceID\": \"" + device_id + "\",\n\t\"" \
                  "UserName\": \"" + user_name + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + "\"\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '201':
            print("ControlSoftwareLog has been created using POST method")
            json_response = response.json()
            self.control_software_log_id = json_response['ID']
            print("ID of the newly created ControlSoftwareLog : {}".format(self.control_software_log_id))
        elif str(response.status_code) == '403':
            print("ControlSoftwareLog could not be created using Logged in user")
            print("ControlSoftwareLog is now being created using SuperAdmin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin user")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache"
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers)
                if str(new_response.status_code) == '201':
                    print("ControlSoftwareLog has been created using SuperAdmin token")
                    json_response = new_response.json()
                    self.control_software_log_id = json_response['ID']
                    print("ID of the newly created ControlSoftwareLog : {}".format(self.control_software_log_id))
                else:
                    raise AssertionError("Something went wrong!")

        utils.code_match(str(response.status_code), expected_code)

    def get_control_software_logs(self, expected_code):
        """
        @desc - method to get all the control software logs
        :param expected_code: expected status code
        :return:
        """

        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.text)

        utils.code_match(str(response.status_code), expected_code)

    def get_control_software_logs_id(self, control_software_log_id, expected_code):
        """
        @desc - method to get control software log by id
        :param control_software_log_id: ID of the control software log
        :param expected_code: expected status code
        :return:
        """
        if control_software_log_id is None:
            url = self.url + self.control_software_log_id
        else:
            url = self.url + control_software_log_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.text)

        utils.code_match(str(response.status_code), expected_code)

    def put_control_software_logs(self, control_software_log_id, device_id, user_name, facility_id, expected_code):
        """
        @desc - method to edit control software log using PUT method
        :param control_software_log_id: ID of the control software log
        :param device_id: ID of the device
        :param user_name: user's name
        :param facility_id: ID of the facility
        :param expected_code: expected status code
        :return:
        """
        if control_software_log_id is None:
            url = self.url + self.control_software_log_id
        else:
            url = self.url + control_software_log_id
            self.control_software_log_id = control_software_log_id

        payload = "{\n\t\"" \
                  "ID\": \"" + self.control_software_log_id + "\",\n\t\"" \
                  "DeviceID\": \"" + device_id + "\",\n\t\"" \
                  "UserName\": \"" + user_name + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + \
                  "\"\n}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def patch_control_software_logs(self, control_software_log_id, device_id, user_name, facility_id, expected_code):
        """
        @desc - method to edit control software log using POST method
        :param control_software_log_id: ID of the control software log
        :param device_id: ID of the device
        :param user_name: user's name
        :param facility_id: ID of the facility
        :param expected_code: expected status code
        :return:
        """
        if control_software_log_id is None:
            url = self.url + self.control_software_log_id
        else:
            url = self.url + control_software_log_id
            self.control_software_log_id = control_software_log_id

        payload = "{\n\t\"" \
                  "ID\": \"" + self.control_software_log_id + "\",\n\t\"" \
                  "DeviceID\": \"" + device_id + "\",\n\t\"" \
                  "UserName\": \"" + user_name + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + \
                  "\"\n}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("PATCH", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_device_logs(self, device_id, max_search_results, expected_code):
        """
        @desc method to get a list of logs for a particular device
        :param device_id: ID of the device
        :param max_search_results: Maximum search results
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "GetDeviceLogs"

        querystring = {"deviceId": device_id, "maxSearchResults": max_search_results}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_facility_logs(self, facility_id, expected_code):
        """
        @desc - method to get logs related to a particular facility
        :param facility_id: ID of the facility
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "GetFacilityLogs"

        querystring = {"facilityId": facility_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_synchronize_log_entry(self, device_id, user_name, facility_id, expected_code):
        """
        @desc - method to synchronize log entry
        :param device_id: ID of the device
        :param user_name: user's name
        :param facility_id: ID of the facility
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "SynchronizeLogEntry"

        payload = "{\n\t\"" \
                  "DeviceID\": \"" + device_id + "\",\n\t\"" \
                  "UserName\": \"" + user_name + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + "\"\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_control_software_log(self, control_software_log_id, expected_code):
        """
        @desc - method to delete control software log by id
        :param control_software_log_id: ID of the control software log
        :param expected_code: expected status code
        :return:
        """
        if control_software_log_id is None:
            url = self.url + self.control_software_log_id
        else:
            url = self.url + control_software_log_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("DELETE", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '403':
            print("Control Software Log could not be deleted using logged in user")
            print("Control Software Log now being deleted using SuperAdmin token")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == '200':
                print("Control Software Log deleted using SuperAdmin token")
            else:
                print("Something went wrong!")

        utils.code_match(str(response.status_code), expected_code)

