import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class StudyRecordingsController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "StudyRecordings/"
    user_token = ""
    super_admin_token = ""

    def __int__(self):
        print("StudyRecordingsController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    # def set_super_admin_token(self, token):
    #     """
    #     @desc - method to set token of the super admin user
    #     :param token: super admin user's token
    #     :return:
    #     """
    #     self.super_admin_token = token

    def get_get_study_recording(self, study_id, recording_index, expected_code):
        """
        @desc = method to get study recording according to ID
        :param study_id: ID of the study
        :param recording_index: Recording index of the study
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "GetStudyRecording"

        querystring = {"id": study_id, "index": recording_index}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_update_study_recording_notes(self, study_id, index, notes, expected_code):
        """
        @desc - method to update study recording notes
        :param study_id: ID of the study to be updated
        :param index: Recording index
        :param notes: Notes to be updated
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "UpdateStudyRecordingNotes"

        payload = "{\r\n  \"" \
                  "StudyID\": \"" + study_id + "\",\r\n  \"" \
                  "Index\": \"" + index + "\",\r\n  \"" \
                  "Notes\": \"" + notes + "\"\r\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_last_sync_packet_index(self, study_id, index, expected_code):
        """
        @desc - method to get last sync packet index
        :param study_id: ID of the study
        :param index: Recording index
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "GetLastSyncPacketIndex"

        querystring = {"id": study_id, "recordingIndex": index}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_calculate_server_checksum(self, study_id, index, expected_code):
        """
        @desc - method to get CalculateServerChecksum
        :param study_id: ID of the study
        :param index: Recording index
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "CalculateServerChecksum"

        querystring = {"id": study_id, "recordingIndex": index}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_calculate_server_video_checksum(self, study_id, recording_index, camera_index, video_index, expected_code):
        """
        @desc - method to get calculate server video checksum
        :param study_id: ID of the study
        :param recording_index: Recording index
        :param camera_index: Camera index
        :param video_index: Video index
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "CalculateServerVideoChecksum"

        querystring = {"id": study_id, "recordingIndex": recording_index, "cameraIndex": camera_index,
                       "videoIndex": video_index}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_server_video_size(self, study_id, recording_index, camera_index, video_index, expected_code):
        """
        @desc - method to get server video size
        :param study_id: ID of the study
        :param recording_index: Recording index
        :param camera_index: Camera index
        :param video_index: Video index
        :param expected_code: expected status code
        :return:
        """
        url = self.url + "GetServerVideoSize"

        querystring = {"id": study_id, "recordingIndex": recording_index, "cameraIndex": camera_index,
                       "videoIndex": video_index}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)