import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class FacilitiesController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "Facilities/"
    user_token = ""
    super_admin_token = ""

    facility_id = ""
    mmt_facility_name = ""
    mmt_facility_domain = ""
    mmt_local_storage_days = ""
    mmt_date_created = ""

    facility_id = ""
    facility_name = ""
    facility_domain = ""
    archive_location = ""
    local_storage_days = 0
    uses_cloud_storage = ""
    disable_auto_archive = ""
    study_note_template = ""
    date_created = ""

    invitation_id = ""
    invited_user_role_id = ""

    email_address_for_invitation = ""

    mmt_facility_id = init.mobile_med_tek_facility_id

    def __init__(self):
        print("FacilitiesController class initialized.")

    def set_test_values(self, fac_name, fac_dom):
        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d_%H_%M_%S')

        self.facility_name = fac_name + st1
        self.facility_domain = fac_dom
        self.local_storage_days = 30
        self.date_created = "2019-05-23T04:16:55.083"

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.super_admin_token = token

    def post_rotate_token(self, fac_id, expected_code):
        """
        @desc - Applies a new token for a facility
        :param - fac_id: ID of the faciltiy
        :param - expected_code: expected status code
        """
        url = self.url + "RotateToken"

        if fac_id is None:
            fac_id = self.mmt_facility_id

        print(fac_id)

        querystring = {'id': fac_id}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_metrics(self, fac_id, expected_code):
        """
        @desc - Returns dashboard metrics for facility
        :param - fac_id: ID of the faciltiy
        :param - expected_code: expected status code
        """
        url = self.url + "Metrics"

        if fac_id is None:
            fac_id = self.mmt_facility_id

        querystring = {'id': fac_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_facilities(self, expected_code):
        """
        @desc - Gets a list of all facilities
        :param - expected_code: expected status code
        """
        url = self.url + "GetFacilities"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_create_facility(self, fac_name, fac_domain, expected_code):
        """
        @desc - Creates a new facility
        :param - fac_name: Name of the facility
        :param - fac_domain: domain name of the facility
        :param - expected_code: expected status code
        """
        url = self.url + "CreateFacility"

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d_%H_%M_%S')

#        fac_name = fac_name + st1

        payload = "{\n  " \
                  "\"Name\": \"" + fac_name + "\",\n" \
                  "\"Domain\":\"" + fac_domain + "\"\r\n}"

        self.facility_name = fac_name
        self.facility_domain = fac_domain

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "200":
            json_response = response.json()
            print("ID of the newly created Facility : "+json_response['ID'])
            self.facility_id = json_response['ID']
            self.date_created = json_response['DateCreated']
            self.local_storage_days = json_response['LocalStorageDays']
        elif str(response.status_code) == "403":
            print("Facility could not be created with logged in user")
            print("Facility is now being created using SuperAdmin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers)
                print(new_response.text)
                if str(new_response.status_code) == "200":
                    print("Facility has been created using SuperAdmin token")
                    new_json_response = new_response.json()
                    print("ID of the newly created Facility : " + new_json_response['ID'])
                    self.facility_id = new_json_response['ID']
                    self.date_created = new_json_response['DateCreated']
                    self.local_storage_days = new_json_response['LocalStorageDays']
                else:
                    raise AssertionError("Something went wrong. Facility could not be created")
        elif str(response.status_code) == '409':
            print("Facility could not be created properly as a facility with the name, " + fac_name + ", already exists"
                                                                                                      "")
            print("Returning the ID of the existing facility named:" + fac_name)
            print("ID : " + init.enosis_facility_id)
            self.facility_id = init.enosis_facility_id
        utils.code_match(str(response.status_code), expected_code)
        return self.facility_id

    def get_send_invitation(self, email, role_id, expected_code):
        """
        @desc - Creates and sends an invite to an email with the intent of adding a user (new or existing) to a facility
        :param - email: Email of the user you are sending invitation. (MUST BE VALID EMAIL)
        :param - role_id: Id of the role
        :param - expected_code: expected status code
        """
        url = self.url + "SendInvitation"

        if email is None:
            email = init.email_address_for_invitation

        if role_id is None:
            role_id = init.facility_admin_role

        self.invited_user_role_id = role_id

        querystring = {'email': email, 'roleid': role_id}

        print("querystring -" + querystring['email'])
        print("querystring -" + querystring['roleid'])

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        if str(response.status_code) == "403":
            print("Logged in user could not send Invitation")
            print("Invitation will now be sent with Super Admin token")
            new_headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            new_response = utils.request("GET", url, data=payload, headers=new_headers, params=querystring)
            if str(new_response.status_code) == "200":
                print(new_response.text)
                print("Super Admin sent the invitation")
            else:
                print("Something went wrong. Super Admin could not send invitation")

        utils.code_match(str(response.status_code), expected_code)

    def post_accept_invitation(self, invitation_id, expected_code):
        """
        @desc - Allows user to accept a facility invitation
        :param - invitation_id: Id of the invitation
        :param - expected_code: expected status code
        """
        url = self.url + "AcceptInvitation"

        if invitation_id is None:
            invitation_id = self.invitation_id

        querystring = {'invitationId': invitation_id}

        payload = "{\n  " \
                  "\"FirstName\": \"" + "Invited" + "\",\n  " \
                  "\"LastName\": \"" + "User" + "\",\n  " \
                  "\"Password\": \"" + "Enosis123" + "\",\n  " \
                  "\"ConfirmPassword\": \"" + "Enosis123" + "\",\n  " \
                  "\"RoleId\":\"0\"\r\n}"

        print("querystring :" + querystring['invitationId'])
        print("PAYLOAD :" + payload)

        headers = {
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        return utils.code_match(str(response.status_code), expected_code)

    def get_get_invitation(self, invitation_id, expected_code):
        """
        @desc - Get invitation by id
        :param - invitation_id: Id of the invitation
        :param - expected_code: expected status code
        """
        url = self.url + "GetInvitation"

        if invitation_id is None:
            invitation_id = self.invitation_id

        querystring = {'id': invitation_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_invitations(self, active_only, is_super_admin, email_address, expected_code):
        """
        @desc - Get list of invitations. Returns all invitations for a facility
        :param - active_only: Optional. True if only active invitation are needed false otherwise
        :param - is_super_admin: If the user is super admin or not
        :param - email_address: Invited email address
        :param - expected_code: expected status code
        """
        url = self.url + "GetInvitations"

        querystring = ""
        if active_only is not None:
            if active_only is True:
                querystring = {'activeOnly': "true"}
            elif active_only is False:
                querystring = {'activeOnly': "false"}
            else:
                raise AssertionError("activeOnly value has to be either true or false")

        payload = ""
        if is_super_admin is None:
            headers = {
                'Authorization': "Bearer " + self.user_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
        else:
            headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
        if email_address is None:
            self.email_address_for_invitation = init.email_address_for_invitation
        else:
            self.email_address_for_invitation = email_address

        if querystring is None:
            response = utils.request("GET", url, data=payload, headers=headers)
        else:
            response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        if str(response.status_code) == "200":
            json_response = response.json()
            for each in range(len(json_response)):
                if json_response[each]['InvitedEmail'] == self.email_address_for_invitation:
                    self.invitation_id = json_response[each]['ID']
                    print("INVITATION EMAIL = " + json_response[each]['InvitedEmail'])
                    print("INVITATION ID = " + self.invitation_id)
                    break
        elif str(response.status_code) == "403":
            print("Logged in user could not access GetInvitations endpoint")
            print("SuperAdmin will now access GetInvitations endpoint")
            new_headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            if querystring is None:
                new_response = utils.request("GET", url, data=payload, headers=new_headers)
            else:
                new_response = utils.request("GET", url, data=payload, headers=new_headers, params=querystring)
            if str(new_response.status_code) == "200":
                new_json_response = new_response.json()
                for each in range(len(new_json_response)):
                    if new_json_response[each]['InvitedEmail'] == init.email_address_for_invitation:
                        self.invitation_id = new_json_response[each]['ID']
                        print("INVITATION EMAIL = " + new_json_response[each]['InvitedEmail'])
                        print("INVITATION ID = " + self.invitation_id)
                        break
            else:
                print("Something went wrong. SuperAdmin could not access GetInvitations endpoint")

        utils.code_match(str(response.status_code), expected_code)
        return self.invitation_id

    def get_does_invited_user_exist(self, inv_id, expected_code):
        """
        @desc - Indicates if the invitation is for an existing user
        :param - inv_id: id of the invitation
        :param - expected_code: expected status code
        """
        url = self.url + "DoesInvitedUserExist"

        if inv_id is None:
            querystring = {'id': self.invitation_id}
        else:
            querystring = {'id': inv_id}

        payload = ""
        headers = {
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_is_invitation_expired(self, invitation_id, expected_code):
        """
        @desc - Indicates if the invitation is expired or not
        :param - invitation_id: id of the invitation
        :param - expected_code: expected status code
        """
        url = self.url + "IsInvitationExpired"

        if invitation_id is None:
            invitation_id = self.invitation_id

        querystring = {'id': invitation_id}

        payload = ""
        headers = {
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_facility(self, fac_id, expected_code):
        """
        @desc - Delete facility by id
        :param - fac_id: id of facility
        :param - expected_code: expected status code
        """

        if fac_id.lower() == init.mobile_med_tek_facility_id.lower():
            raise AssertionError("Cannot delete MobileMedTek facility")

        if self.facility_id.lower() == init.mobile_med_tek_facility_id.lower():
            raise AssertionError("Facility ID is set to MMT fac ID. Cannot delete this facility")

        if fac_id is None:
            fac_id = self.facility_id

        url = self.url + fac_id

        print(url)

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "403":
            print("Test user could not delete the Facility.")
            print("Deleting the Facility with super admin token.")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == "200":
                print("The Facility has been deleted by super admin token")
            else:
                print("Something went wrong. Could not delete the Facility")

        utils.code_match(str(response.status_code), expected_code)

    def get_facility_id(self, fac_id, expected_code):
        """
        @desc - Gets a facility by ID. Facility admins are only allowed to get facilities that they are an admin of
        :param - fac_id: id of facility
        :param - expected_code: expected status code
        """
        if fac_id is None:
            fac_id = self.facility_id

        url = self.url + fac_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "200" and fac_id == init.mobile_med_tek_facility_id:
            json_response = response.json()
            self.mmt_facility_name = json_response['Name']
            self.mmt_facility_domain = json_response['Domain']
            self.mmt_local_storage_days = json_response['LocalStorageDays']
            self.mmt_date_created = json_response['DateCreated']

        utils.code_match(str(response.status_code), expected_code)

    def patch_facility_id(self, fac_id, expected_code):
        """
        @desc - Updates facility info
        :param - fac_id: id of the facility
        :param - expected_code: expected status code
        """
        if fac_id is None:
            fac_id = self.facility_id

        url = self.url + fac_id

        if fac_id == init.mobile_med_tek_facility_id:
            fac_name = self.mmt_facility_name
            fac_domain = self.mmt_facility_domain
            storage_days = str(self.mmt_local_storage_days)
            date_created = self.mmt_date_created
        else:
            fac_name = self.facility_name + "_EDIT_PATCH"
            fac_domain = self.facility_domain
            storage_days = str(self.local_storage_days)
            date_created = self.date_created

        payload = "{\n  " \
                  "\"ID\": \"" + fac_id + "\",\n  " \
                  "\"Name\": \"" + fac_name + "\",\n  " \
                  "\"Domain\": \"" + fac_domain + "\",\n  " \
                  "\"ArchiveLocation\": \"\",\n  " \
                  "\"LocalStorageDays\":" + storage_days + ",\n  " \
                  "\"UsesCloudStorage\": true,\n  " \
                  "\"DisableAutoArchive\": false,\n  " \
                  "\"StudyNotesTemplate\": \"\",\n  " \
                  "\"VideoFileLengthOptionID\": 1,\n  " \
                  "\"DateCreated\":\"" + date_created + "\"\r\n}"

        # payload = "{\n  " \
        #           "\"Name\": \"" + fac_name + "_EDIT_PUT" + "\",\n  " \
        #           "\"Domain\": \"" + fac_domain + "\",\n  " \
        #           "\"ArchiveLocation\": \"\",\n  " \
        #           "\"LocalStorageDays\":\"15\",\n  " \
        #           "\"UsesCloudStorage\": true,\n  " \
        #           "\"DisableAutoArchive\": false,\n  " \
        #           "\"StudyNotesTemplate\": \"\",\n  " \
        #           "\"VideoFileLengthOptionID\": 1\n}"

        print(payload)

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_facility_id(self, fac_id, expected_code):
        """
        @desc - Adds or updates a facility. The facility admin can only update their own facility
        :param - fac_id: id of the facility
        :param - expected_code: expected status code
        """
        if fac_id is None:
            fac_id = self.facility_id

        url = self.url + fac_id

        if fac_id == init.mobile_med_tek_facility_id:
            fac_name = self.mmt_facility_name
            fac_domain = self.mmt_facility_domain
            storage_days = str(self.mmt_local_storage_days)
            date_created = self.mmt_date_created
        else:
            fac_name = self.facility_name + "_EDIT_PUT"
            fac_domain = self.facility_domain
            storage_days = str(self.local_storage_days)
            date_created = self.date_created

        payload = "{\n  " \
                  "\"ID\": \"" + fac_id + "\",\n  " \
                  "\"Name\": \"" + fac_name + "\",\n  " \
                  "\"Domain\": \"" + fac_domain + "\",\n  " \
                  "\"ArchiveLocation\": \"\",\n  " \
                  "\"LocalStorageDays\":" + storage_days + ",\n  " \
                  "\"UsesCloudStorage\": true,\n  " \
                  "\"DisableAutoArchive\": false,\n  " \
                  "\"StudyNotesTemplate\": \"\",\n  " \
                  "\"VideoFileLengthOptionID\": 1,\n  " \
                  "\"DateCreated\":\"" + date_created + "\"\r\n}"

        print(payload)

        # payload = "{\n  " \
        #           "\"ID\": \"" + fac_id + "\",\n  " \
        #           "\"Name\": \"" + self.facility_name + "_EDIT_PUT" + "\",\n  " \
        #           "\"Domain\": \"" + self.facility_domain + "\",\n  " \
        #           "\"ArchiveLocation\": \"\",\n  " \
        #           "\"LocalStorageDays\":" + str(self.local_storage_days) + ",\n  " \
        #           "\"UsesCloudStorage\": true,\n  " \
        #           "\"DisableAutoArchive\": false,\n  " \
        #           "\"StudyNotesTemplate\": \"\",\n  " \
        #           "\"VideoFileLengthOptionID\": 1,\n  " \
        #           "\"DateCreated\":\"" + self.date_created + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

