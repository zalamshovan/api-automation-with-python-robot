import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class StudiesController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "Studies/"
    user_token = ""
    super_admin_token = ""
    study_id = ""
    uploaded_study_id = ""

    created_patient_id = ""
    facility_id = ""
    birth_date = ""
    user_defined_pat_id = ""

    def __init__(self):
        print("StudiesController class initialized.")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: user's token
        :return:
        """
        self.super_admin_token = token

    def get_studies(self, is_super_admin, expected_code):
        """
        @desc - method to get all studies in the current facility
        :param is_super_admin: set to True if you want to access with super admin token
        :param expected_code:
        :return: response
        """
        url = self.url
        self.log.info('METHOD: get_studies; url : ' + url)

        if is_super_admin is True:
            if self.super_admin_token == "":
                raise AssertionError("Super Admin token of Studies Controller is NULL")
            token = self.super_admin_token
        else:
            token = self.user_token

        payload = ""
        headers = {
            'Authorization': "Bearer " + token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

#        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == "200":
            return response

    def get_studies_id(self, study_id, expected_code):
        """
        @desc - method to get a study by ID
        :param study_id: ID of the study
        :param expected_code: expected status code
        :return: Response
        """
        if study_id is None:
            url = self.url + self.study_id
        else:
            url = self.url + study_id

        self.log.info('METHOD: get_studies_id; url : ' + url)

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.json())
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == '200':
            return response

    def post_study_upload_study_setup(self, patient_id, study_sync_state, scale, use_video, sample_rate, harness_id,
                                      expected_code):
        """
        @desc - Method to create a study and an associated Amazon s3 bucket for uploading study data
        :param patient_id: ID of the patient
        :param study_sync_state: Study sync state
        :param scale: scale
        :param use_video: Boolean value to determine if video to be used or not
        :param sample_rate: Sample rate of the amplifier
        :param harness_id: Harness ID
        :param expected_code: status code
        :return: STUDY ID
        """
        url = self.url + "StudyUploadSetup"
        self.log.info('METHOD: post_study_upload_study_setup; url : ' + url)
        payload = "{\r\n  \"" \
                  "PatientID\":\"" + patient_id + "\",\r\n  \"" \
                  "StudySyncState\":\"" + study_sync_state + "\",\r\n  \"" \
                  "Scale\":\"" + scale + "\",\r\n  \"" \
                  "UseVideo\":\"" + use_video + "\",\r\n  \"" \
                  "SampleRate\":\"" + sample_rate + "\",\r\n  \"" \
                  "CustomElectrodes\":[],\r\n  \"" \
                  "HarnessID\":\"" + harness_id + "\"\r\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)
        if str(response.status_code) == '200':
            json_response = response.json()
            self.uploaded_study_id = json_response['study']['ID']
            return self.uploaded_study_id
        elif str(response.status_code) == '403':
            print('Logged in user could not access StudyUploadSetup endpoint')
            print('SuperAdmin user now trying to access StudyUploadSetup endpoint')
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'Content-Type': "application/json",
                'cache-control': "no-cache"
            }
            response_super = utils.request("POST", url, data=payload, headers=headers_super_admin)
            print(response_super.text)
            json_response_super = response_super.json()
            if str(response_super.status_code) == '200':
                self.uploaded_study_id = json_response_super['study']['ID']
                return self.uploaded_study_id
        utils.code_match(str(response.status_code), expected_code)

    def post_aws_bucket_for_study(self, study_id, expected_code):
        """
        @desc - method to create an amazon s3 bucket for uploading study data
        :return:
        """
        url = self.url + "AwsBucketForStudy"
        self.log.info('METHOD: post_aws_bucket_for_study; url : ' + url)

        if study_id is None:
            study_id = self.uploaded_study_id

        querystring = {"studyId": study_id}
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_study_id(self, study_id, expected_code):
        """
        @desc - Deletes a study by ID
        :param study_id: ID of the study
        :param expected_code: expected status code
        :return:
        """
        if study_id is None:
            url = self.url + self.study_id
        else:
            url = self.url + study_id
        self.log.info('METHOD: delete_study_id; url : ' + url)

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("DELETE", url, data=payload, headers=headers)
        print(response.text)
        if str(response.status_code) == '403':
            print('Study could not deleted using logged in user')
            print('Study now being deleted using SuperAdmin token')
            headers_for_delete = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            delete_response = utils.request("DELETE", url, data=payload, headers=headers_for_delete)
            print(delete_response.text)
        utils.code_match(str(response.status_code), expected_code)
