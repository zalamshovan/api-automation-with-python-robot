import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class StudyEventsController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "StudyEvents/"
    user_token = ""
    super_admin_token = ""

    note_type_event_id = "14"

    payload_of_adding_event = ""
    response_body_of_created_study_event = ""
    created_event_id = ""
    study_id = ""

    def __init__(self):
        print("PatientsController class initialized.")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: user's token
        :return:
        """
        self.super_admin_token = token

    def post_add_study_events(self, study_id, recording_index, start_packet_index, end_packet_index, comment,
                              expected_code):
        """
        @desc - Post method to add a study event
        :param study_id:
        :param recording_index:
        :param start_packet_index:
        :param end_packet_index:
        :param comment:
        :param expected_code:
        :return:
        """
        url = self.url + "AddStudyEvent"

        self.study_id = study_id

        payload = "{\r\n" \
                  "\"StartPacketIndex\":" + start_packet_index + ",\r\n" \
                  "\"EndPacketIndex\":" + end_packet_index + ",\r\n" \
                  "\"RecordingIndex\":" + recording_index + ",\r\n" \
                  "\"EventTypeID\":" + self.note_type_event_id + ",\r\n" \
                  "\"StudyID\":\"" + study_id + "\",\r\n" \
                  "\"Comment\":\"" + comment + "\"\r\n}"

        self.payload_of_adding_event = payload

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print("RESPONSE BODY -" + response.text)
        print("STATUS CODE -" + str(response.status_code))

        if str(response.status_code) == "201":
            self.response_body_of_created_study_event = response.text
            json_response = response.json()
            self.created_event_id = json_response['ID']
            print(self.created_event_id)
            return self.created_event_id
        elif str(response.status_code) == "403":
            print("Logged in user could not create study event")
            print("Study event will now be created by super admin token")
            if self.super_admin_token is None:
                raise AssertionError("Super admin token has not been initialized")
            else:
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache"
                }
                new_response = utils.request("POST", url, data=payload, headers=new_headers)

                print(new_response.text)

                if str(new_response.status_code) == "201":
                    print("Study event has been created by super admin token.")
                    self.response_body_of_created_study_event = new_response.text
                    json_response = new_response.json()
                    self.created_event_id = json_response['ID']
                    return self.created_event_id
                else:
                    raise AssertionError("Something went wrong. Event could not be created.")

        utils.code_match(str(response.status_code), expected_code)

    def get_get_study_events_by_study_id(self, is_super_admin, study_id, expected_code):
        """
        @desc - Shows a list of Study Events by Study ID
        :param is_super_admin:
        :param study_id:
        :param expected_code:
        :return:
        """
        url = self.url + "GetStudyEventsByStudyID"

        if is_super_admin is True:
            token = self.super_admin_token
        else:
            token = self.user_token

        print("get_get_study_events_by_study_id TOKEN:"+token)

        querystring = {'id': study_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        print("Querystring: " + querystring['id'])

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        return response

    def get_study_events_id(self, event_id, expected_code):
        """
        @desc - Get study events by id
        :param event_id:
        :param expected_code:
        :return:
        """
        if event_id is None:
            event_id = self.created_event_id
        url = self.url + event_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def patch_study_event(self, event_id, expected_code):
        """
        @desc - Patch a study event (not sure about the params)
        :param event_id:
        :param expected_code:
        :return:
        """
        if event_id is None:
            event_id = self.created_event_id
        url = self.url + event_id

        payload = "{\r\n  \"StudyID\": \"" + self.study_id + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_study_events(self, event_id, expected_code):
        """
        @desc - Put study event.
        :param event_id:
        :param expected_code:
        :return:
        """
        if event_id is None:
            event_id = self.created_event_id
        url = self.url + event_id

        payload = self.response_body_of_created_study_event
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_study_events(self, expected_code):
        """
        @desc - Get all study events
        :param expected_code:
        :return:
        """
        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_study_event(self, expected_code):
        """
        @desc - Method to check post study event
        :param expected_code:
        :return:
        """
        url = self.url

        payload = self.payload_of_adding_event

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_study_event(self, event_id, expected_code):
        """
        @desc - To delete a study event by ID
        :param event_id:
        :param expected_code:
        :return:
        """
        if event_id is None:
            event_id = self.created_event_id

        url = self.url + event_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)
        if str(response.status_code) == "403":
            print("Logged in user could not delete the event id")
            print("Study event will now be deleted by super admin token")
            new_headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            new_response = utils.request("DELETE", url, data=payload, headers=new_headers)

            if str(new_response.status_code) == "200":
                print("Study event has been deleted by the super admin token")
            else:
                raise AssertionError("Something went wrong. Event could not be deleted.")

        utils.code_match(str(response.status_code), expected_code)
