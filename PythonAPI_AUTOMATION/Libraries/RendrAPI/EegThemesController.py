import os
import sys
import time
import datetime

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class EegThemesController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + 'EegThemes/'

    user_token = ""

    eeg_theme_id = ""
    eeg_date_created = ""
    eeg_theme_name = ""

    super_admin_token = ""

    def __int__(self):
        print("EegThemesController class initialized")

    def set_token(self, token):
        """
        @desc - method to set the token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: user's token
        :return:
        """
        self.super_admin_token = token

    def post_eeg_themes(self, eeg_theme_name, background_color, label_color, facility_id, expected_code):
        """
        @desc - Creates a new eeg theme
        :param eeg_theme_name: name of the theme
        :param facility_id: ID of the facility for which the theme will be saved
        :param background_color: Background color of the new theme
        :param label_color: Label color of the new theme
        :param expected_code: expected status code
        :return:
        """

        url = self.url

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')

        self.eeg_theme_name = eeg_theme_name + st1

        payload = "{\n\t\"" \
                  "Name\": \"" + eeg_theme_name + "\",\n\t\"" \
                  "BackgroundColor\": \"" + background_color + "\",\n\t\"" \
                  "LabelColor\": \"" + label_color + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + \
                  "\"\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '201':
            print("EegTheme has been created using POST method")
            json_response = response.json()
            self.eeg_theme_id = json_response['ID']
            print("ID of the newly created EegTheme: {}".format(self.eeg_theme_id))
        elif str(response.status_code) == '403':
            print("Eeg Theme could not be created using logged in user")
            print("Eeg Theme is now being created using super admin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin user")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache"
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers)
                if str(new_response.status_code) == '201':
                    print("Eeg Theme has been created using SuperAdmin token")
                    json_response = new_response.json()
                    self.eeg_theme_id = json_response['ID']
                    print("ID of the newly created EegTheme: {}".format(self.eeg_theme_id))
                else:
                    raise AssertionError("Something went wrong!")

        utils.code_match(str(response.status_code), expected_code)

    def get_eeg_themes(self, expected_code):
        """
        @desc - method to get all eeg themes
        :param expected_code: expected status code
        :return:
        """

        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)
        print(response.text)

        utils.code_match(str(response.status_code), expected_code)

    def get_eeg_themes_id(self, eeg_themes_id, expected_code):
        """
        @desc - method to get eeg theme using ID
        :param eeg_themes_id: ID of the eeg theme
        :param expected_code: expected status code
        :return:
        """

        if eeg_themes_id is None:
            url = self.url + self.eeg_theme_id
        else:
            url = self.url + eeg_themes_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

        if str(response.status_code) == '200':
            print("Got Eeg Themes by ID")
            json_response = response.json()
            self.eeg_date_created = json_response['DateCreated']

    def patch_eeg_themes_id(self, eeg_themes_id, eeg_theme_name, background_color, label_color, facility_id,
                            expected_code):
        """
        @desc - method to edit eeg theme by patch method
        :param eeg_themes_id: ID of the theme
        :param eeg_theme_name: Theme's name
        :param background_color: Theme's background color
        :param label_color: Theme's label color
        :param facility_id: Facility ID of the theme
        :param expected_code: expected status code
        :return:
        """

        if eeg_themes_id is None:
            url = self.url + self.eeg_theme_id
        else:
            url = self.url + eeg_themes_id
            self.eeg_theme_id = eeg_themes_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')
        self.eeg_theme_name = eeg_theme_name + st1

        payload = "{\n\t\"" \
                  "ID\": \"" + self.eeg_theme_id + "\",\n\t\"" \
                  "Name\": \"" + self.eeg_theme_name + "\",\n\t\"" \
                  "BackgroundColor\": \"" + background_color + "\",\n\t\"" \
                  "LabelColor\": \"" + label_color + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + \
                  "\"\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_eeg_themes_id(self, eeg_themes_id, eeg_themes_name, background_color, label_color, facility_id,
                          expected_code):
        """
        @desc - method to edit theme by id using put method
        :param eeg_themes_id: ID of the theme
        :param eeg_themes_name: Theme's name
        :param background_color: Background color of the theme
        :param label_color: Label color of the theme
        :param facility_id: Facility ID of the theme to which it belongs
        :param expected_code: expected status code
        :return:
        """

        if eeg_themes_id is None:
            url = self.url + self.eeg_theme_id
        else:
            url = self.url + eeg_themes_id
            self.eeg_theme_id = eeg_themes_id

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')
        self.eeg_theme_name = eeg_themes_name + st1

        payload = "{\n\t\"" \
                  "ID\": \"" + self.eeg_theme_id + "\",\n\t\"" \
                  "Name\": \"" + self.eeg_theme_name + "\",\n\t\"" \
                  "BackgroundColor\": \"" + background_color + "\",\n\t\"" \
                  "LabelColor\": \"" + label_color + "\",\n\t\"" \
                  "FacilityID\": \"" + facility_id + \
                  "\"\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_eeg_themes_id(self, eeg_themes_id, expected_code):
        """
        @desc - method to delete a theme by id
        :param eeg_themes_id: ID of the theme to be deleted
        :param expected_code: expected status code
        :return:
        """

        if eeg_themes_id is None:
            url = self.url + self.eeg_theme_id
        else:
            url = self.url + eeg_themes_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }
        response = utils.request("DELETE", url, data=payload, headers=headers)
        print(response.text)

        if str(response.status_code) == '403':
            print("Logged in user could not delete Eeg Theme")
            print("Eeg Theme will now be deleted using SuperAdmin token")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == '200':
                print("Eeg Theme deleted using SuperAdmin token")
            else:
                print("Something went wrong")

        utils.code_match(str(response.status_code), expected_code)
