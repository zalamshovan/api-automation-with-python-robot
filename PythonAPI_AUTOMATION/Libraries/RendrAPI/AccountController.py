import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries', 'RendrAPI'))
sys.path.insert(0, lib_path2)
from TestDataProvider import TestDataProvider

class AccountController:
    log = logger_extended().get_logger('API_Automation')
    # url = init.url + "Account/"
    url = init.url
    data = TestDataProvider()

    user_token = ""
    super_admin_token = ""

    def __init__(self):
        print("AccountController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.super_admin_token = token

    def get_authorized(self, expected_code):
        """
        @desc - Check if user is authorized or not
        :param expected_code:
        :return:
        """
        # url = self.url + "Authorized"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="Authorized")

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_reset_user_password(self, user_id, expected_code):
        """
        @desc - Allows admins to reset a users password, new password is returned
        :param user_id: ID of the user whose password will be changed
        :param expected_code:
        :return:
        """
        # url = self.url + "ResetUserPassword"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="ResetUserPassword")

        querystring = {'ID': user_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

        changed_password = response.text

        if str(response.status_code) == "200":
            return changed_password.replace('\"', '')

    def post_forget_password(self, email, expected_code):
        """
        @desc - Forger password endpoint
        :param email:
        :param expected_code:
        :return:
        """
        # url = self.url + "ForgotPassword"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="ForgotPassword")

        querystring = {'email': email}

        payload = ""
        headers = {
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_account_recovery(self, user_id, password, code, expected_code):
        """
        @desc - Account recovery endpoint.
        :param user_id:
        :param password:
        :param code:
        :param expected_code:
        :return:
        """
        # url = self.url + "AccountRecovery"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="AccountRecovery")

        payload = "{\r\n" \
                  "\"userId\":\"" + user_id + "\",\r\n" \
                  "\"password\":\"" + password + "\",\r\n" \
                  "\"confirmPassword\":\"" + password + "\",\r\n" \
                  "\"code\":\"" + code + "\"\r\n}\r\n"

        headers = {
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_user_info(self, expected_code):
        """
        @desc - Method to get user information
        :param expected_code:
        :return:
        """
        # url = self.url + "UserInfo"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="UserInfo")

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        return response

    def get_is_authorized_for_device_use(self, device_id, expected_code):
        # url = self.url + "IsAuthorizedForDeviceUse"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="IsAuthorizedForDeviceUse")

        querystring = {'deviceId': device_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_manage_info(self, return_url, general_state, expected_code):
        """
        @desc - No Idea.
        :param return_url:
        :param general_state:
        :param expected_code:
        :return:
        """
        # url = self.url + "ManageInfo"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="GetManageInfo")

        querystring = {'returnUrl': return_url, 'generateState': general_state}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_change_password(self, old_password, new_password, expected_code):
        """
        @desc - Endpoint to change logged in user's password
        :param old_password:
        :param new_password:
        :param expected_code:
        :return:
        """
        # url = self.url + "ChangePassword"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="ChangePassword")

        payload = "{\r\n" \
                  "\"OldPassword\": \"" + old_password + "\",\r\n" \
                  "\"NewPassword\": \"" + new_password + "\",\r\n" \
                  "\"ConfirmPassword\": \"" + new_password + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_set_password(self, new_password, expected_code):
        """
        @desc - Set password of the logged in user
        :param new_password:
        :param expected_code:
        :return:
        """
        # url = self.url + "SetPassword"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="SetPassword")

        payload = "{\r\n" \
                  "\"NewPassword\": \"" + new_password + "\",\r\n" \
                  "\"ConfirmPassword\": \"" + new_password+ "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_add_external_login(self, token, expected_code):
        """
        @desc - No idea!
        :param token:
        :param expected_code:
        :return:
        """
        # url = self.url + "AddExternalLogin"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="AddExternalLogin")

        payload = "{\r\n  \"ExternalAccessToken\": \"" + token + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = url.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_remove_login(self, login_provider, provider_key, expected_code):
        """
        @desc - No Idea!
        :param login_provider:
        :param provider_key:
        :param expected_code:
        :return:
        """
        # url = self.url + "RemoveLogin"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="RemoveLogin")

        payload = "{\r\n  \"LoginProvider\": \"" + login_provider + "\",\r\n  \"ProviderKey\": \"" + provider_key + "\"\r\n}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_external_login(self, provider, error, expected_code):
        """
        @desc - No Idea!
        :param provider:
        :param error:
        :param expected_code:
        :return:
        """
        # url = self.url + "ExternalLogin"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="GetExternalLogin")

        querystring = {'provider': provider, 'error': error}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache",
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_external_logins(self, return_url, generate_state, expected_code):
        """
        @desc - No Idea!
        :param return_url:
        :param generate_state:
        :param expected_code:
        :return:
        """
        # url = self.url + "ExternalLogins"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController",
                                                    endpoint_name="GetExternalLogins")

        querystring = {'returnUrl': return_url, 'generateState': generate_state}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = url.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_log_out(self, expected_code):
        """
        @desc - method to log out
        :param expected_code:
        :return:
        """
        # url = self.url + "Logout"
        url = self.url + self.data.get_endpoint_url(controller_name="AccountController", endpoint_name="Logout")

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
