import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init


lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class DevicesController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + '/api/Devices/'

    user_token = ""
    device_name = ""
    device_sn = ""
    device_config = ""
    device_part_number = ""
    facility_id = ""
    device_id = ""

    device_date_created = ""

    super_admin_token = ""

    def __init__(self):
        print("DevicesController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token: user's token
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user.
        :param token: user's token
        :return:
        """
        self.super_admin_token = token

    def post_create_device(self, device_name, device_sn, device_config, device_partnumber, facility_id, expected_code):
        """
        @desc - Creates a new Device using Post method
        :param device_name: Name of the device
        :param device_sn: Serial Number of the device
        :param device_config: Configuration of the device
        :param device_partnumber: Part Number of the device
        :param facility_id: ID of the facility to which the device will belong
        :param expected_code: Expected Status code
        :return: Newly created device ID
        """

        url = self.url + "CreateDevice"
        querystring = {'facilityId': facility_id}

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        st2 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')
        st3 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')
        st4 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%Sd')

        self.device_name = device_name + st1
        self.device_sn = device_sn + st2
        self.device_config = device_config + st3
        self.device_part_number = device_partnumber + st4
        self.facility_id = facility_id

        payload = "{\r\n  \"" \
                  "Name\": \"" + self.device_name + "\",\r\n  \"" \
                  "SerialNumber\": \"" + self.device_sn + "\",\r\n  \"" \
                  "Configuration\": \"" + self.device_config + "\",\r\n  \"" \
                  "PartNumber\": \"" + self.device_part_number + "\",\r\n  \"" \
                  "FacilityID\": \"" + self.facility_id + "\"\r\n" \
                  "}"
        print("User Token = " + self.user_token)

        print(payload)

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        if str(response.status_code) == '201':
            json_response = response.json()
            self.device_id = json_response['ID']
            print("ID of the newly created Device: {}".format(self.device_id))
            self.device_name = json_response['Name']
            print("Name of the newly created Device: {}".format(self.device_name))
            self.device_sn = json_response['SerialNumber']
            print("Serial Number of the newly created Device: {}".format(self.device_sn))
            self.device_config = json_response['Configuration']
            print("Configuration of the newly created Device: {}".format(self.device_config))
            self.device_part_number = json_response['PartNumber']
            print("Part Number of the newly created Device: {}".format(self.device_part_number))
        elif str(response.status_code) == '403':
            print("Device could not be created with logged in user")
            print("Device is now being created using SuperAdmin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache"
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers, params=querystring)
                print(new_response.text)
                if str(new_response.status_code) == '201':
                    print("Device has been created using SuperAdmin token")
                    json_response = new_response.json()
                    self.device_id = json_response['ID']
                    print("ID of the newly created Device: {}".format(self.device_id))
                    self.device_name = json_response['Name']
                    print("Name of the newly created Device: {}".format(self.device_name))
                    self.device_sn = json_response['SerialNumber']
                    print("Serial Number of the newly created Device: {}".format(self.device_sn))
                    self.device_config = json_response['Configuration']
                    print("Configuration of the newly created Device: {}".format(self.device_config))
                    self.device_part_number = json_response['PartNumber']
                    print("Part Number of the newly created Device: {}".format(self.device_part_number))
                else:
                    raise AssertionError("Something went wrong. Device could not be created")

        utils.code_match(str(response.status_code), expected_code)
        return self.device_id

    def get_get_by_sn(self, sn_device, expected_code):
        """
        @desc - Get a device by serial number
        :param sn_device: Serial Number of the device. If set to None, the created device's sn will be used
        :param expected_code: expected status code
        :return:
        """

        url = self.url + "GetBySn"

        if sn_device is None:
            sn_device = self.device_sn

        querystring = {'sn': sn_device}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_devices(self, expected_code):
        """
        @desc - method to Get all Devices
        :param expected_code: expected status code
        :return:
        """

        url = self.url + "GetDevices"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_devices_id(self, id_devices, expected_code):
        """
        @desc - Get a Device's data using ID
        :param id_devices: ID of the device
        :param expected_code: expected status code
        :return:
        """

        if id_devices is None:
            url = self.url + self.device_id
        else:
            url = self.url + id_devices

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

        if str(response.status_code) == '200':
            print('Device has been created using SuperAdmin token')
            json_response = response.json()
            self.device_date_created = json_response['DateCreated']

    def post_devices(self, expected_code):
        """
        @desc - Post method of Devices controller.
        :param expected_code: expected status code
        :return:
        """

        url = self.url

        payload = "{\r\n  \"" \
                  "Name\": \"" + self.device_name + "\",\r\n  \"" \
                  "SerialNumber\": \"" + self.device_sn + "\",\r\n  \"" \
                  "Configuration\": \"" + self.device_config + "\",\r\n  \"" \
                  "PartNumber\": \"" + self.device_part_number + "\",\r\n  \"" \
                  "FacilityID\": \"" + self.facility_id + "\"\r\n" \
                  "}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == '201':
            print("Device has been created using POST method")
            json_response = response.json()
            print("Deleting the created Device using SuperAdmin token")
            delete_url = self.url + json_response['ID']

            payload = ""
            headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }

            delete_response = utils.request("DELETE", delete_url, data=payload, headers=headers)
            if str(delete_response.status_code) == '200':
                print("The device has been deleted properly")
            else:
                print("Something went wrong!")

    def patch_devices_id(self, expected_code):
        """
        @desc - Update a device by id
        :param expected_code: expected status code
        :return:
        """

        url = self.url + self.device_id

        serial_no = self.device_sn + "Edited"
        name = self.device_name + "Edited"

        payload = "{\r\n  \"" \
                  "Name\": \"" + name + "\",\r\n  \"" \
                  "SerialNumber\": \"" + serial_no + "\",\r\n  \"" \
                  "Configuration\": \"" + self.device_config + "\",\r\n  \"" \
                  "PartNumber\": \"" + self.device_part_number + "\",\r\n  \"" \
                  "FacilityID\": \"" + self.facility_id + "\"\r\n" \
                  "}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_devices_id(self, expected_code):
        """
        @desc - Updates a device by passing full entity
        :param expected_code: expected status code
        :return:
        """

        url = self.url + self.device_id

        device_name = self.device_name + "Editedbyput"

        payload = "{\r\n  \"" \
                  "ID\": \"" + self.device_id + "\", \r\n  \"" \
                  "Name\": \"" + device_name + "\",\r\n  \"" \
                  "SerialNumber\": \"" + self.device_sn + "\",\r\n  \"" \
                  "Configuration\": \"" + self.device_config + "\",\r\n  \"" \
                  "PartNumber\": \"" + self.device_part_number + "\",\r\n  \"" \
                  "FacilityID\": \"" + self.facility_id + "\"\r\n" \
                  "}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_change_device_facility(self, device_id, facility_id, expected_code):
        """
        @desc- Changes the facility of a device.
        :param device_id: ID of the device
        :param facility_id: ID of the facility to which the device will be changed to
        :param expected_code: expected status code
        :return:
        """
        if device_id is None:
            device_id = self.device_id
        url = self.url + "ChangeDeviceFacility"
        querystring = {"deviceId": device_id, "facilityId": facility_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_delete_devices(self, id_device, expected_code):
        """
        @desc Deletes a device. If logged in user cannot delete, the device will be deleted using SuperAdmin token
        :param id_device: ID of the device to be deleted
        :param expected_code: expected status code
        :return:
        """
        if id_device is None:
            url = self.url + self.device_id
        else:
            url = self.url + id_device

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == '403':
            print("Test user could not delete device")
            print("Device will now be deleted using SuperAdmin token")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }

            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == "200":
                print("Device has been deleted using SuperAdmin token")
            else:
                print("Something went wrong. Device was not deleted")

        utils.code_match(str(response.status_code), expected_code)

