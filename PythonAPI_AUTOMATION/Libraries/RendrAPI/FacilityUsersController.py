import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class FacilityUsersController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "FacilityUsers/"
    user_token = ""
    super_admin_token = ""

    mmt_facility_id = init.mobile_med_tek_facility_id

    random_user_id_taken_from_users_list = ""
    default_email_address = init.api_office_personnel
    facility_user_id = ""

    def __init__(self):
        print("FacilityUsers class initialized.")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.super_admin_token = token

    def get_get_users(self, expected_code):
        """
        @desc - Gets all the facility users from the facility that the app user has selected
        :param expected_code:
        :return:
        """
        url = self.url + "GetUsers"

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "200":
            json_response = response.json()
            print(json_response[0]['ID'] + " has been assigned to random_user_id_taken_from_users_list variable")
            self.random_user_id_taken_from_users_list = json_response[0]['ID']

        utils.code_match(str(response.status_code), expected_code)

    def get_get_user(self, fac_user_id, expected_code):
        """
        @desc - Gets a particular facility user by ID
        :param fac_user_id:
        :param expected_code:
        :return:
        """
        url = self.url + "GetUser"

        if fac_user_id is None:
            fac_user_id = self.facility_user_id

        querystring = {'facilityUserId': fac_user_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def post_create_facility_user(self, user_email, expected_code):
        """
        @desc - Creates the facility user as long as an MMT user exists with the given email address
        :param user_email:
        :param expected_code:
        :return:
        """
        url = self.url + "CreateFacilityUser"
        print("User email "+user_email)

        querystring = {'emailAddress': user_email}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        if str(response.status_code) == "201":
            print("Facility user with " + self.default_email_address + " email was created")
            json_response = response.json()
            self.facility_user_id = json_response['ID']
        elif str(response.status_code) == "403":
            print("Facility user could not be created with logged in user")
            print("Facility user will now be created with super admin token")
            new_headers = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            new_response = utils.request("POST", url, data=payload, headers=new_headers, params=querystring)
            print(new_response.text)
            if str(new_response.status_code) == "201":
                print("Facility user with " + self.default_email_address + " email was created with super admin token")
                json_response = new_response.json()
                self.facility_user_id = json_response['ID']
            else:
                print(str(new_response.status_code))
                raise AssertionError("Something went wrong. Super admin could not create facility user")
        utils.code_match(str(response.status_code), expected_code)

    def post_facility_user(self, fac_id, user_id, expected_code):
        """
        @des- Create a facility user
        :param fac_id:
        :param user_id:
        :param expected_code:
        :return:
        """
        url = self.url

        if fac_id is None:
            fac_id = self.mmt_facility_id

        payload = "{\r\n  " \
                  "\"FacilityID\": \"" + fac_id + "\",\r\n  " \
                  "\"UserID\": \"" + user_id + "\"\r\n}"
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "500":
            json_response = response.json()
            self.facility_user_id = json_response['ID']

        utils.code_match(str(response.status_code), expected_code)

    def delete_facility_user(self, fac_user_id, expected_code):
        """
        @desc - Deletes the Facility User, but not the MMT user account
        :param fac_user_id:
        :param expected_code:
        :return:
        """
        if fac_user_id is None:
            fac_user_id = self.facility_user_id

        url = self.url + fac_user_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_facility_user(self, fac_user_id, expected_code):
        """
        @desc - Get a facility user based of ID
        :param fac_user_id:
        :param expected_code:
        :return:
        """
        if fac_user_id is None:
            fac_user_id = self.facility_user_id

        url = self.url + fac_user_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_facility_users(self, expected_code):
        """
        @desc- Gets a queryable collection
        :param expected_code:
        :return:
        """
        url = self.url

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_facility_user(self, fac_id, user_id, expected_code):
        """
        @desc - Update facility user info
        :param fac_id:
        :param user_id:
        :param expected_code:
        :return:
        """
        if fac_id is None:
            fac_id = self.mmt_facility_id

        url = self.url + fac_id

        payload = "{\r\n  " \
                  "\"FacilityID\": \"" + fac_id + "\",\r\n  " \
                  "\"UserID\": \"" + user_id + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def patch_facility_user(self, fac_id, user_id, expected_code):
        """
        @desc - Update facility user info
        :param fac_id:
        :param user_id:
        :param expected_code:
        :return:
        """
        if fac_id is None:
            fac_id = self.mmt_facility_id
        if user_id is None:
            user_id = self.facility_user_id

        url = self.url + fac_id

        payload = "{\r\n  " \
                  "\"FacilityID\": \"" + fac_id + "\",\r\n  " \
                  "\"UserID\": \"" + user_id + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
