import os
import sys
import datetime
import time

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path1)
import conf as init

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path2)
import utils
from logger_robot import logger as logger_extended


class NotificationsController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + 'Notifications/'

    user_token = ""

    super_admin_token = ""

    study_event_type_id = ""
    study_event_type_name = ""
    study_event_type_description = ""

    def __int__(self):
        print("NotificationsController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param token:
        :return:
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param token: super admin user's token
        :return:
        """
        self.super_admin_token = token

    def post_share_study(self, study_id, user_id, expected_code):
        """
        @desc - Post share study endpoint
        :param study_id: id of the study to share
        :param user_id: receiver user id
        :param expected_code:
        :return:
        """
        url = self.url + "ShareStudy"

        payload = "{" \
                  "\"StudyId\":\"" + study_id + "\"," \
                  "\"UserIds\":[\"" + user_id + "\"]}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)

        utils.code_match(str(response.status_code), expected_code)

    def post_share_study_event(self, study_id, study_event_id, user_id, expected_code):
        """
        @desc - Post share study event endpoint
        :param study_id: id of the study to share
        :param study_event_id: id of an event of the study to share
        :param user_id: receiver user id
        :param expected_code:
        :return:
        """
        url = self.url + "ShareStudyEvent"

        payload = "{" \
                  "\"StudyId\":\"" + study_id + "\"," \
                  "\"StudyEventId\":\"" + study_event_id + "\"," \
                  "\"UserIds\":[\"" + user_id + "\"]}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)

        utils.code_match(str(response.status_code), expected_code)

    def post_notify_study_submitted(self, study_id, user_id, user_email, expected_code):
        """
        @desc - Post notify study submitted endpoint
        :param study_id: id of the study to share
        :param user_id: receiver user id
        :param user_email: receiver user email
        :param expected_code:
        :return:
        """
        url = self.url + "NotifyStudySubmitted"

        payload = "{\r\n  " \
                  "\"StudyId\": \"" + study_id + "\",\r\n  " \
                  "\"UserIds\": [\r\n    \"" + user_id + "\"\r\n  ],\r\n  " \
                  "\"Emails\": [\r\n    \"" + user_email + "\"\r\n  ]\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_shareable_users_and_groups(self, facility_id, expected_code):
        """
        @desc - Get sharable users and groups endpoint
        :param facility_id:
        :param expected_code:
        :return:
        """
        url = self.url + "GetShareableUsersAndGroups"

        querystring = {'facilityId': facility_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_submittable_users_and_groups(self, facility_id, expected_code):
        """
        @desc - get submittable user and groups endpoint
        :param facility_id:
        :param expected_code:
        :return:
        """
        url = self.url + "GetSubmittableUsersAndGroups"

        querystring = {'facilityId': facility_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

