import datetime
import time
import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'libraries'))
sys.path.insert(0, lib_path2)
import conf as init

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'utilities'))
sys.path.insert(0, lib_path1)
import utils
from logger_robot import logger as logger_extended


class AmplifiersController:
    log = logger_extended().get_logger('API_Automation')
    url = init.url + "Amplifiers/"

    user_token = ""
    amp_name = ""
    amp_id = ""
    amp_sn = ""
    amp_type_id = []
    facility_id = ""

    amp_firmware_version = ""
    amp_bootloader_version = ""
    amp_sample_rate_id = ""
    amp_type_mode_id = ""
    amp_date_created = ""

    super_admin_token = ""

    def __init__(self):
        print("AmplifiersController class initialized")

    def set_token(self, token):
        """
        @desc - method to set token of the user
        :param - token: user's token
        """
        self.user_token = token

    def set_super_admin_token(self, token):
        """
        @desc - method to set token of the super admin user
        :param - token: user's token
        """
        self.super_admin_token = token

    def post_create_amplifier(self, amp_name, amp_sn, facility_id, expected_code):
        """
        @desc - Create an Amplifier using Post method.
        :param - amp_name: Name of the Amp.
        :param - amp_sn: Serial number of the Amp
        :param - facility_id: Id of the facility to which the Amp will belong
        :param - expected_code: expected status code
        """
        url = self.url + "CreateAmplifier"
        querystring = {'facilityId': facility_id}

        ts = time.time()
        st1 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        st2 = datetime.datetime.fromtimestamp(ts).strftime('%H%M%S%d')

        self.amp_name = amp_name + st1
        self.amp_sn = amp_sn + st2
        self.facility_id = facility_id
        payload = "{\n  " \
                  "\"Name\": \"" + self.amp_name + "\",\n  " \
                  "\"SerialNumber\": \"" + self.amp_sn + "\",\n  " \
                  "\"FacilityID\": \"" + facility_id + "\",\n  " \
                  "\"AmplifierTypeID\": 1\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }

        response = utils.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)

        if str(response.status_code) == "201":
            json_response = response.json()
            print("ID of the newly created Amplifier : "+json_response['ID'])
            self.amp_id = json_response['ID']
            print("Created Amp's ID :"+self.amp_id)
            self.amp_sn = json_response['SerialNumber']
            print("Created Amp's SN :"+self.amp_sn)
        elif str(response.status_code) == "403":
            print("Amplifier could not be created with logged in user")
            print("Amplifier is now being created using SuperAdmin token")
            if self.super_admin_token is None:
                raise AssertionError("Please initialize token for SuperAdmin")
            else:
                new_payload = payload
                new_headers = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }
                new_response = utils.request("POST", url, data=new_payload, headers=new_headers, params=querystring)
                print(new_response.text)
                if str(new_response.status_code) == "201":
                    print("Amplifier has been created using SuperAdmin token")
                    json_response = new_response.json()
                    print("ID of the newly created Amplifier : " + json_response['ID'])
                    self.amp_id = json_response['ID']
                    print("Created Amp's ID :" + self.amp_id)
                    self.amp_sn = json_response['SerialNumber']
                    print("Created Amp's SN :" + self.amp_sn)
                else:
                    raise AssertionError("Something went wrong. Amplifier could not be created")

        utils.code_match(str(response.status_code), expected_code)

    def get_get_by_sn(self, sn_amp, expected_code):
        """
        @desc - Get an Amp by serial number
        :param - sn_amp: Serial number of amp. If set to None, the created amp's sn will be userd
        :param - expected_code: expected status code
        """
        url = self.url + "GetBySn"

        if sn_amp is None:
            sn_amp = self.amp_sn

        querystring = {'sn': sn_amp}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache",
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_amplifier_types(self, expected_code):
        """
        @desc - Get all types of amplifiers
        :param - expected_code: expected status code
        """
        url = self.url + "GetAmplifierTypes"
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == "200":
            json_response = response.json()
            for index in range(len(json_response)):
                for key in json_response[index]:
                    if key == "ID":
                        self.amp_type_id.append(json_response[index][key])
                        print(json_response[index][key])

    def get_get_modes_for_amplifier(self, amplifier_type_id, expected_code):
        """
        @desc - Get an Amp's mode using ID
        :param - amplifier_type_id: ID of the amp type.
        :param - expected_code: expected status code
        """
        url = self.url + "GetModesForAmplifier"
        if amplifier_type_id is None:
            if self.amp_type_id[0] is not None:
                print("Amp type ID found from get_get_amplifier_types has been set")
                amplifier_type_id = str(self.amp_type_id[0])
            else:
                print("get_get_amplifier_types did not return Amp Type ID. Setting it to 1.")
                amplifier_type_id = "1"

        querystring = {'amplifierTypeId': amplifier_type_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_sample_rates_for_amplifier(self, amplifier_type_id, expected_code):
        """
        @desc - Get an Amp's sample rate using ID
        :param - amplifier_type_id: ID of the amp type.
        :param - expected_code: expected status code
        """
        url = self.url + "GetSampleRatesForAmplifier"
        if amplifier_type_id is None:
            if self.amp_type_id[0] is not None:
                print("Amp type ID found from get_get_amplifier_types has been set")
                amplifier_type_id = str(self.amp_type_id[0])
            else:
                print("get_get_amplifier_types did not return Amp Type ID. Setting it to 1.")
                amplifier_type_id = "1"

        querystring = {'amplifierTypeId': amplifier_type_id}

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers, params=querystring)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_amplifiers(self, expected_code):
        """
        @desc - Get all Amps' data
        :param - expected_code: expected status code
        """
        url = self.url + "GetAmplifiers"
        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache",
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def get_get_amplifier_id(self, amp_id, expected_code):
        """
        @desc - Get an Amp's data using ID
        :param - amp_id: ID of the amp.
        :param - expected_code: expected status code
        """
        if amp_id is None:
            url = self.url + self.amp_id
        else:
            url = self.url + amp_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("GET", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

        if str(response.status_code) == "200":
            print("Amplifier has been created using SuperAdmin token")
            json_response = response.json()
            self.amp_date_created = json_response['DateCreated']

    def post_amplifiers(self, expected_code):
        """
        @desc - Post method of Amplifier controller. Not sure the goal of this API
        :param - expected_code: expected status code
        """
        url = self.url

        payload = "{\n  " \
                  "\"Name\": \"" + self.amp_name + "\",\n  " \
                  "\"SerialNumber\": \"" + self.amp_sn + "\",\n  " \
                  "\"FacilityID\": \"" + self.facility_id + "\",\n  " \
                  "\"AmplifierTypeID\": 1\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("POST", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)
        if str(response.status_code) == "201":
            print("Amplifier has been created using POST method")
            json_response = response.json()
            print("Newly created Amplifier's ID : " + json_response['ID'])
            print("Deleting the created AMP with SuperAdmin token")
            delete_url = self.url + json_response['ID']
            print("Delete URL : " + delete_url)
            payload = ""

            if self.super_admin_token == "":
                print("SuperAdmin token has not been initialized")
            else:
                headers_for_delete = {
                    'Authorization': "Bearer " + self.super_admin_token,
                    'Accept': "application/json, text/json",
                    'cache-control': "no-cache"
                }
                delete_response = utils.request("DELETE", delete_url, data=payload, headers=headers_for_delete)
                if str(delete_response.status_code) == "200":
                    print("The amp has been deleted properly")
                else:
                    print("Something went wrong. Could not delete the Amp created by POST. Status Code found " +
                          str(delete_response.status_code))

    def patch_amplifiers_id(self, expected_code):
        """
        @desc - Update an amp's values
        :param - expected_code: expected status code
        """
        url = self.url + self.amp_id

        serial_no = self.amp_sn + "Edited"

        payload = "{\n  " \
                  "\"ID\": \"" + self.amp_id + "\",\n  " \
                  "\"SerialNumber\": \"" + serial_no + "\",\n  " \
                  "\"SerialNumber\": \"" + self.amp_name + "\",\n  " \
                  "\"FirmwareVersion\": \"\",\n  " \
                  "\"BootloaderVersion\": \"\",\n  " \
                  "\"AmplifierTypeID\": \"" + "1" + "\",\n  " \
                  "\"AmplifierTypeModeID\": \"\",\n  " \
                  "\"AmplifierSampleRateID\": \"\",\n  " \
                  "\"FacilityID\": \"" + self.facility_id + "\",\n  " \
                  "\"DateCreated\": \"" + self.amp_date_created + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PATCH", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def put_amplifiers(self, expected_code):
        """
        @desc - Really don't know what it does :/
        :param - expected_code: expected status code
        """
        url = self.url + self.amp_id

        payload = "{\n  " \
                  "\"ID\": \"" + self.amp_id + "\",\n  " \
                  "\"SerialNumber\": \"" + self.amp_sn + "\",\n  " \
                  "\"Name\": \"" + self.amp_name + "_PUT" + "\",\n  " \
                  "\"Name\": \"" + self.amp_name + "_PUT" + "\",\n  " \
                  "\"FirmwareVersion\": \"\",\n  " \
                  "\"BootloaderVersion\": \"\",\n  " \
                  "\"AmplifierTypeID\": \"" + "1" + "\",\n  " \
                  "\"AmplifierTypeModeID\": \"\",\n  " \
                  "\"AmplifierSampleRateID\": \"\",\n  " \
                  "\"FacilityID\": \"" + self.facility_id + "\",\n  " \
                  "\"DateCreated\": \"" + self.amp_date_created + "\"\r\n}"

        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = utils.request("PUT", url, data=payload, headers=headers)

        print(response.text)
        utils.code_match(str(response.status_code), expected_code)

    def delete_delete_amplifier(self, amp_id, expected_code):
        """
        @desc - Deletes an amp. If logged in user cannot delete, the amp will be deleted using super admin's token
        :param - amp_id: ID of the amp to be deleted.
        :param - expected_code: expected status code
        """

        if amp_id is None:
            url = self.url + self.amp_id
        else:
            url = self.url + amp_id

        payload = ""
        headers = {
            'Authorization': "Bearer " + self.user_token,
            'Accept': "application/json, text/json",
            'cache-control': "no-cache"
        }

        response = utils.request("DELETE", url, data=payload, headers=headers)

        print(response.text)

        if str(response.status_code) == "403":
            print("Test user could not delete the Amp.")
            print("Deleting the AMP with super admin token.")
            headers_super_admin = {
                'Authorization': "Bearer " + self.super_admin_token,
                'Accept': "application/json, text/json",
                'cache-control': "no-cache"
            }
            response_delete = utils.request("DELETE", url, data=payload, headers=headers_super_admin)
            if str(response_delete.status_code) == "200":

                print("The AMP " + self.amp_name + " has been deleted by super admin token")
            else:
                print("Something went wrong. Could not delete the amp")

        utils.code_match(str(response.status_code), expected_code)

