@echo off

call _AccountController.bat
call _AmplifiersController.bat
call _ControlSoftwareLogsController.bat
call _DevicesController.bat
call _EegThemesController.bat
call _FacilitiesController.bat
call _FacilityUsersController.bat
call _NotificationsController.bat
call _PatientsController.bat
call _StudyEventsController.bat
call _StudyEventTypesController.bat
call _StudyRecordingsController.bat
call _UsersController.bat

pause