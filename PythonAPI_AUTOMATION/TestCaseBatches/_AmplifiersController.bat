@echo off

set module=_AmplifiersController
set folder_name=Reports%module%

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
echo fullstamp: "%fullstamp%"

cd ..\TestReports

mkdir %folder_name%\01_SuperAdmin_Reports\%fullstamp%
mkdir %folder_name%\02_Support_Reports\%fullstamp%
mkdir %folder_name%\03_Production_Reports\%fullstamp%
mkdir %folder_name%\04_FacilityAdmin_Reports\%fullstamp%
mkdir %folder_name%\05_ReviewDoctor_Reports\%fullstamp%
mkdir %folder_name%\06_LeadTech_Reports\%fullstamp%
mkdir %folder_name%\07_FieldTech_Reports\%fullstamp%
mkdir %folder_name%\08_OfficePersonnel_Reports\%fullstamp%

cd ..\TestCaseRobots\%module%

call robot --outputdir ..\..\TestReports\%folder_name%\01_SuperAdmin_Reports\%fullstamp%\ -o 01_SuperAdmin.xml -l 01_SuperAdmin_Log.html -r 01_SuperAdmin_Report.html .\01_SuperAdmin.robot
call robot --outputdir ..\..\TestReports\%folder_name%\02_Support_Reports\%fullstamp%\ -o 02_Support.xml -l 02_Support_Log.html -r 02_Support_Report.html .\02_Support.robot
call robot --outputdir ..\..\TestReports\%folder_name%\03_Production_Reports\%fullstamp%\ -o 03_Production.xml -l 03_Production_Log.html -r 03_Production_Report.html .\03_Production.robot
call robot --outputdir ..\..\TestReports\%folder_name%\04_FacilityAdmin_Reports\%fullstamp%\ -o 04_FacilityAdmin.xml -l 04_FacilityAdmin_Log.html -r 04_FacilityAdmin_Report.html .\04_FacilityAdmin.robot
call robot --outputdir ..\..\TestReports\%folder_name%\05_ReviewDoctor_Reports\%fullstamp%\ -o 05_ReviewDoctor.xml -l 05_ReviewDoctor_Log.html -r 05_ReviewDoctor_Report.html .\05_ReviewDoctor.robot
call robot --outputdir ..\..\TestReports\%folder_name%\06_LeadTech_Reports\%fullstamp%\ -o 06_LeadTech.xml -l 06_LeadTech_Log.html -r 06_LeadTech_Report.html .\06_LeadTech.robot
call robot --outputdir ..\..\TestReports\%folder_name%\07_FieldTech_Reports\%fullstamp%\ -o 07_FieldTech.xml -l 07_FieldTech_Log.html -r 07_FieldTech_Report.html .\07_FieldTech.robot
call robot --outputdir ..\..\TestReports\%folder_name%\08_OfficePersonnel_Reports\%fullstamp%\ -o 08_OfficePersonnel.xml -l 08_OfficePersonnel_Log.html -r 08_OfficePersonnel_Report.html .\08_OfficePersonnel.robot

set super=%folder_name%\01_SuperAdmin_Reports\%fullstamp%\01_SuperAdmin.xml
set support=%folder_name%\02_Support_Reports\%fullstamp%\02_Support.xml
set production=%folder_name%\03_Production_Reports\%fullstamp%\03_Production.xml
set facility=%folder_name%\04_FacilityAdmin_Reports\%fullstamp%\04_FacilityAdmin.xml
set review=%folder_name%\05_ReviewDoctor_Reports\%fullstamp%\05_ReviewDoctor.xml
set lead=%folder_name%\06_LeadTech_Reports\%fullstamp%\06_LeadTech.xml
set field=%folder_name%\07_FieldTech_Reports\%fullstamp%\07_FieldTech.xml
set office=%folder_name%\08_OfficePersonnel_Reports\%fullstamp%\08_OfficePersonnel.xml	

cd ..\..\TestReports

set output_dir=Reports%module%\_All_Merged\%fullstamp%

mkdir %output_dir%
rebot --outputdir %output_dir%\ --timestampoutputs -l Merged_log.html -r Merged_log_Report.html %super% %support% %production% %facility% %review% %lead% %field% %office%

cd ..\TestCaseBatches