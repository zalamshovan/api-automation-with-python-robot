# API Automation with Python-Robot

This framework was designed to automate service layer testing. Using Python's Requests and Robot Framework. There are other paid/free tools to perform API Endpoint testing, but this framework gives the user a lot to play with the data.